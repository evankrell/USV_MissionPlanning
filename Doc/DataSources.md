# Data Sources


## Online NetCDF Ocean Forecast Sources


### Implemented

**The Northeast Coastal Ocean Forecast System (NECOFS)**

http://fvcom.smast.umassd.edu/necofs/

		sourceURL = 'http://www.smast.umassd.edu:8080/thredds/dodsC/FVCOM/NECOFS/Forecasts/NECOFS_FVCOM_OCEAN_MASSBAY_FORECAST.nc'
		sourceNC = netCDF4.Dataset (sourceURL)

"NECOFS provides three days of forecast fields of surface winds, air pressure, air temperature, air humidity, sea surface heat flux, evaporation minus precipitation, sea level, water temperature, salinity, currents, wave heights, wave directions and wave frequencies as well as wintertime icing rates and storm-induced inundation areas."


### Considering

**Physical Oceanography Numerical Group's Texas-Louisiana Shelf Region Forecast**

http://pong.tamu.edu/research.html

		sourceURL = 'http://barataria.tamu.edu:8080/thredds/dodsC/NcML/oof_latest_agg/roms_his_f_latest.nc'
		sourceNC = netCDF4.Dataset (sourceURL)

Made a single attempt to use by simply changing the NetCDF source URL. Gave the following error: 

		File "NetCDFtest.py", line 54, in getNcByTime
		    itime = netCDF4.date2index (time, nc.variables['time'], select = 'nearest')
		KeyError: 'time'

**Naval Oceanographic Office Global Hybrid Coordinate Ocean Model (HYCOM)**

https://ecowatch.ncddc.noaa.gov/erddap/griddap/documentation.html

		sourceURL = 'https://ecowatch.ncddc.noaa.gov/erddap/griddap/HYCOM_reg1_latest3d'
		sourceNC = netCDF4.Dataset (sourceURL)

Selecting by time seemed to work fine. Would need to change variable names to actuallyt use it in the code. 
For example, 'lat' to 'latitude'. 

**Naval Oceanographic Office Regional Navy Coastal Ocean Model (NCOM)**

https://www.ncdc.noaa.gov/data-access/model-data/model-datasets/navoceano-ncom-reg

		sourceURL = 'https://ecowatch.ncddc.noaa.gov/erddap/griddap/HYCOM_reg1_latest3d'
		sourceNC = netCDF4.Dataset (sourceURL)

Same data format as HYCOM, but fewer regions. The available regions seem to be at higher resolution. Texas coast is in U.S. East region. 

**Gulf Research Initiative's**

https://data.gulfresearchinitiative.org/

Seems as though they have data, but data portal was done when I checked. 


