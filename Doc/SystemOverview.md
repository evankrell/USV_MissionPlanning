# System Overview





### High-Level Algorithm Description

**INIT SYSTEM**

*Parameters*
- System Time
- USV Information
- Available Power
- Solar Power Charge Rate (as a function of available sunlight)
- Region Raster Map

*Get Water Currents*
- Use the extent of the Region raster to get Water Current forecast points for the next N hours
- Will get at X intervals. For example, will get forecasts for next 48 hours, one set per hour. 
- Will rasterize each set of points and store as bands of a raster.
- Will have a raster of the current magnitude and of the current direction. 

*Get Target Substrate Map*
- Use the extent of the Region raster to get the target substrate "aerial view". 
- Will initiale a rate of change map as 0's since no history
- OR will initialize with an existing rate of change map. 

**END INIT SYSTEM**

**START LOOP**

*Determine Targets of Interest*
- Assign "interestingness" to a raster based on target and target rate of change. 

*Path Planning*
- Use Metaheuristic Search to optimize path.
- Keep track of the time that it takes to reach each waypoint.
- Use that time to ensure using the nearest time grid for evaluating the water currents between each waypoint pair. 
- Keep track of energy expendature as well as energy charge
- Time of day --> Available sunlight --> Charge rate. 
- Path duration should not exceed available forecast. (right?)
- For now, use single criteria optimization (combined fitness function)

*Operate for one time interval*
- If working with currents spaced hourly, increment time by 1 hour
- Set current position to where it would be given that hour has passed based on the selected path. 

*Get next time interval*
- Since an hour has passed (supposedly), can get more data from online forecast. 
- So, should not start with all the available forecast when in simulation. Need to have future available. 

**RESTART LOOP**

