#!/usr/bin/python

def parseOption ():
	from optparse import OptionParser
	parser = OptionParser ()
	parser.add_option ('-f', "--file", dest='file',
		help = "Image file to analyze", metavar='FILE')
	parser.add_option ('-o', "--outfile", dest='outfile',
		help = "Image file to write to", metavar='OUT')
	parser.add_option ('-d', "--directory", dest='directory',
		help = "Directory of image iterations", metavar='DIR')
	return parser.parse_args ()

def initMapByImage (filename):
	from PIL import Image
	im = Image.open (filename)
	im = im.convert ('RGB')
	grid = [[0 for x in range (im.size[0])] for y in range (im.size[1])]

	points = [[], [], [], []]


	for row in range (im.size[0]):
		for col in range (im.size[1]):
			p = im.getpixel ( (row, col) )
		
			if (p == (0, 139, 0)): # green
				grid[col][row] = 1
				points[0].append ( [col, row] )
			elif (p == (224, 102, 255)): # purple
				grid[col][row] = 2
				points[1].append ( [col, row] )
			elif (p == (65, 105, 225)): # blue
				grid[col][row] = 3
				points[2].append ( [col, row] )
			elif (p == (225, 106, 106)): # red
				grid[col][row] = 4
				points[3].append ( [col, row] )
	return grid, points

def nn (entity_points):
	from sklearn.neighbors import NearestNeighbors

	nbrs = NearestNeighbors(algorithm='ball_tree').fit(entity_points)
	distances, indices = nbrs.radius_neighbors(entity_points, 25)

	return indices

def kmeans (X):
	from sklearn.cluster import KMeans

	if (X.shape[0] == 0):
		return None

	kmeans = KMeans (n_clusters = 50, random_state = 0).fit (X)
	return kmeans

def filterByNeighbors (indices, points, minNeighbors):
	dropIndices = []
	for i in indices:
		l = len (i)
		if (l < minNeighbors):
			dropIndices.append (i)
	points.drop (points.index[i], inplace = True)
			


def clusterBoundingBox (clusters):
	from scipy.spatial import ConvexHull

	# Init convex hull storage
	clusterSet = [] 

	# Get number of clusters
	nClusters = len (list(set(clusters['cluster'])))
	# Get a convex hull for each cluster
	for i in range (1, nClusters):
		c = clusters.loc[clusters['cluster'] == i]
		cp = c[[0,1]].as_matrix ()
		cSet = {'hull' : ConvexHull (cp), 
			'points' : cp}
		clusterSet.append (cSet)
	return clusterSet

def drawHulls (clusterSet, outfile):
	import matplotlib.pyplot as plt

	for cluster in clusterSet:
		p = cluster['points']
		for simplex in cluster['hull'].simplices:
			plt.plot(p[simplex, 0], p[simplex, 1], 'k-', linewidth = 10)
		plt.plot(p[:,0], p[:,1], '.')

	plt.savefig (outfile)
	plt.clf ()


def iteration (filename, outTag):
	(grid, points) = initMapByImage (filename)
	kmeans_directory = []
	nn_directory = []

	count = 0 
	for p in points:

		pdf = pd.DataFrame (data = p)

		# Use kNN to remove outliers
		nn_directory.append (nn (pdf))
		filterByNeighbors (nn_directory [count], pdf, 100)

		# Use kMeans to form clusters
		kmeans_directory.append (kmeans (pdf))
		pdf['cluster'] = kmeans_directory [count].labels_

		# Get convex hull around clusters
		clusterSet = clusterBoundingBox (pdf)

		# Draw convex hull around clusters
		drawHulls (clusterSet, str (outTag) + "_hulls_" + str (count) + ".png")

		count = count + 1

# MAIN
import numpy as np
import pandas as pd
from path import Path
import re

(options, args) = parseOption ()

p = Path (options.directory)
images = []
for f in p.files (pattern='iter_*.png'):
	images.append (f)

for i in images:
	print (i)
	m = re.search ('[0-9]*.png', i)
	m = re.search ('[0-9]*', m.group ())
	idx = int (m.group ())
	iteration (i, idx)

