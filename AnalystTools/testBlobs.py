#!/usr/bin/python

def parseOption ():
	from optparse import OptionParser
	parser = OptionParser ()
	parser.add_option ('-f', "--file", dest='file',
		help = "Image file to analyze", metavar='FILE')
	parser.add_option ('-o', "--outfile", dest='outfile',
		help = "Image file to write to", metavar='OUT')
	return parser.parse_args ()




import cv2
import numpy as np

(options, args) = parseOption ()

# Read image
img = cv2.imread (options.file, cv2.IMREAD_GRAYSCALE)

# Init blob detector
params = cv2.SimpleBlobDetector_Params()
# Change thresholds
params.minThreshold = 200
params.maxThreshold = 255
# Filter by Area.
params.filterByArea = True
params.minArea = 1
params.maxArea = 3000
# Filter by Circularity
params.filterByCircularity = False
params.minCircularity = 0.1
# Filter by Inertia
params.filterByInertia = False
params.minInertiaRatio = 0.01 
# Filter by Convexity
params.filterByConvexity = False
params.minConvexity = 0.87


bd = cv2.SimpleBlobDetector (params)

# Detect blobs
blobs = bd.detect (img)

# Draw blobs
img_blobs = cv2.drawKeypoints(img, blobs, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)


# Show 
cv2.imwrite (options.outfile, img_blobs)

