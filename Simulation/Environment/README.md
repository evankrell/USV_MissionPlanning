## USV Gazebo Environment for Mission Planning Evaluation

### Quick Start

**Gazebo environment:** 
USV_MissionPlanning/Simulation/Environment

**Launch**

		source /usr/share/gazebo/setup.sh
		GAZEBO_RESOURCE_PATH="$GAZEBO_RESOURCE_PATH:<YOUR_PATH>/USV_MissionPlanning/Simulation/Environment/catkin_ws/" \
			gazebo <YOUR_PATH>/USV_MissionPlanning/Simulation/Environment/catkin_ws/waterworld.sdf



Rather than implement a fully-featured marine environment,
this project essentially "hacks" a simple UGV environment
for testing the USV path planning.

Eventually, it would be ideal to have a more sophisticated system
for simulating surface vehicles, but I want something that can be
produced quickly given that I have no computer graphics experience.

### Environment assumptions & goals

Such a simple environment should be sufficient because of the 
high-level nature of the mission planning that is under evaluation.
Small water perturbations and such are assumed to be under control.
The USV is an airboat using differential drive by select choice of
motor control. 
The USV is basically similar to a differential drive UGV 
that is affected by water currents. 
Energy usage to overcome or to take advantage of currents is of major interest.

**Water currents**: There is no need to support 3D visualization of currents.
Instead, just overlay an image of the vector field.
Sufficient to visually see how the USV is working with the currents.
ROS supports applying arbitrary forces to objects. 
Thus, as any point, 
the current force interpolated to that point will be applied.

**Water surface**: Make the ground floor blue. 
Since the research is concerned with higher-level mission
plannining, don't need to worry about bouyancy, pitch, roll, etc.
Assume that the robot's onboard controller can handle these things.
Only specific forces, such as water currents, 
require explicit programming to overcome.

**Targets**: Make a floor below the ground floor that shows the
map of targets, color coded. A 2D map of the seafloor. 

**Terrain**: Elevated obstacle shape. 
The USV (an airboat) sits almost completely on the surface
and is intended to handle small bumps.
Thus, underwater terrain is negligable, and not of interest
for testing the mission planning. 

### How to Create Environment





