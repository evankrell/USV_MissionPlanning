TargetImgDir=$1
ModelDir=$2

# !NOTE that there must exist a valid 'sample_targets' model in $ModelDir
# The others will be copied from that model template.

numFiles=$(ls $TargetImgDir | grep 'TargetsIdx[0-9]*.png' | wc -l)
echo $numFiles

for N in $(seq 1 1 $numFiles); do

	# Delete new directory first, if already exists
	rm -rf $ModelDir""/targets_idx$N

	# Replicate idx 1 as template
	cp -r $ModelDir""/sample_targets $ModelDir""/targets_idx$N

	# Edit 'model.config'
	sed -i -e "s/SampleTargets/TargetsIdx$N/" \
		$ModelDir""/targets_idx$N""/model.config

	# Edit 'model.sdf'
	sed -i -e "s/sample_targets/targets_idx$N/g" \
		-e "s/SampleTargets/TargetsIdx$N/" \
		-e "s/<size>.*<\/size>/<size>570 570<\/size>/g" \
		$ModelDir""/targets_idx$N""/model.sdf

	# Edit 'materials/scripts/water_currents.material'
	sed -i -e "s/SampleTargets/TargetsIdx$N/" \
		$ModelDir""/targets_idx$N""/materials/scripts/sample_targets.material

	# Copy water currents image to 'materials/scripts/textures'
	cp $TargetImgDir""/TargetsIdx$N"".png $ModelDir""/targets_idx$N""/materials/textures/

done
