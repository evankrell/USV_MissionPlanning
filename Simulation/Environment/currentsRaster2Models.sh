CurrentImgDir=$1
ModelDir=$2

# !NOTE that there must exist a valid 'water_currents' model in $ModelDir
# The others will be copied from that model template.

numFiles=$(ls $CurrentImgDir | grep 'WaterCurrentsIdx[0-9]*.png' | wc -l)
echo $numFiles

for N in $(seq 1 1 $numFiles); do

	# Delete new directory first, if already exists
	rm -rf $ModelDir""/water_currents_idx$N

	# Replicate idx 1 as template
	cp -r $ModelDir""/water_currents_template $ModelDir""/water_currents_idx$N

	# Edit 'model.config'
	sed -i -e "s/WaterCurrentsIdx1/WaterCurrentsIdx$N/" \
		$ModelDir""/water_currents_idx$N""/model.config

	# Edit 'model.sdf'
	sed -i -e "s/water_currents_idx1/water_currents_idx$N/g" \
		-e "s/WaterCurrentsIdx1/WaterCurrentsIdx$N/" \
		-e "s/<size>.*<\/size>/<size>570 570<\/size>/g" \
		$ModelDir""/water_currents_idx$N""/model.sdf

	# Edit 'materials/scripts/water_currents.material'
	sed -i -e "s/WaterCurrentsIdx1/WaterCurrentsIdx$N/" \
		$ModelDir""/water_currents_idx$N""/materials/scripts/water_currents.material

	# Copy water currents image to 'materials/scripts/textures'
	cp $CurrentImgDir""/WaterCurrentsIdx$N"".png $ModelDir""/water_currents_idx$N""/materials/textures/

done
