# Creates the models needed for a Gazebo world based on a region of interest.

# User supplies:
# - Region Map:
#   'USV_MissionPlanning/DataLayers/scenario1/Map/regionLand_2.tif'
# - Currents Raster Bricks:
#   'USV_MissionPlanning/Playground/testCurrentsDirection.tif'
#   'USV_MissionPlanning/Playground/testCurrentsMagitude.tif'

from pylab import *
from PIL import Image
import numpy as np
import matplotlib.tri as Tri
import matplotlib.pyplot as plt
import time
from osgeo import gdal
import georaster
from numpy import ma
import os
import re

GAZEBO_IMG_DIM = (513, 513) # Must be 2^X + 1

def makeGazeboRegion (region):

    oldSize = region.size
    ratio = float (GAZEBO_IMG_DIM[0]) / max (oldSize)
    newSize = tuple ([int (x*ratio) for x in oldSize])
    regionResize = region.resize (newSize, Image.ANTIALIAS)
    regionNew = Image.new ("RGBA", GAZEBO_IMG_DIM, (255, 255, 255))
    regionNew.paste (regionResize, ((GAZEBO_IMG_DIM[0] - newSize[0]),
        (GAZEBO_IMG_DIM[1] - newSize[1])))

    # https://stackoverflow.com/questions/3752476/python-pil-replace-a-single-rgba-color
    data = np.array(regionNew)
    red, green, blue, alpha = data.T
    blackArea = (red < 10) & (blue < 10) & (green < 10)
    data[..., :-1][~blackArea.T] = (255, 255, 255) # Transpose back needed
    regionNew = Image.fromarray (data)

    return regionNew, oldSize, newSize

def makeGazeboGeneric (directory, prefix, region):

    files = [f for f in os.listdir(directory) if re.match(prefix + '.*\.png', f)]

    for f in files:
        i = re.findall('\d+.png', f)
	i = i[0].split ('.')[0]

        f = Image.open (directory + f)
        oldSize = f.size
        ratio = float (GAZEBO_IMG_DIM[0]) / max (oldSize)
        newSize = tuple ([int (x*ratio) for x in oldSize])
        fResize = f.resize (newSize, Image.ANTIALIAS)
        fNew = Image.new ("RGBA", GAZEBO_IMG_DIM, (255, 255, 255))
        fNew.paste (fResize, ((GAZEBO_IMG_DIM[0] - newSize[0]),
            (GAZEBO_IMG_DIM[1] - newSize[1])))

        outFile = "region2world_temp/TargetsIdx" + str (i) + ".png"
        fNew.save (outFile)



def makeGazeboCurrent (currentDirection, currentMagnitude, regionRaster, size):

    fig = plt.figure (figsize = (size[0] * .01, size[1] * .01), facecolor = 'white')
    ax = fig.add_axes([0,0,1,1])
    ax.axis("off")
    ax.margins(0)

    dCurrentsGrid = np.fliplr (currentDirection.ReadAsArray().T)
    mCurrentsGrid = np.fliplr (currentMagnitude.ReadAsArray().T)

    xSamples = []
    ySamples = []
    uSamples = []
    vSamples = []

    gridFlip = np.fliplr (regionRaster.GetRasterBand (1).ReadAsArray().T)

    sampleInterval = 10 # pixels
    for x in range (0, size[0], sampleInterval):
        for y in range (0, size[1], sampleInterval):
            xSamples.append (x)
            ySamples.append (y)
  
            if (gridFlip[x][y] == 0):
		    uSamples.append (mCurrentsGrid[x][y] * cos (dCurrentsGrid[x][y]))
		    vSamples.append (mCurrentsGrid[x][y] * sin (dCurrentsGrid[x][y]))
            else:
                uSamples.append (0)
                vSamples.append (0)

    plt.quiver(xSamples[::10], ySamples[::10], uSamples[::10], vSamples[::10])
    
    return plt

def currents (currentsDirectionBrick, currentsMagnitudeBrick, regionRaster):

    size = (currentsDirectionBrick.RasterXSize, currentsDirectionBrick.RasterYSize)

    for i in range (currentsDirectionBrick.RasterCount):
	i += 1
        CDi = currentsDirectionBrick.GetRasterBand (i)
        CMi = currentsMagnitudeBrick.GetRasterBand (i)
        
        makeGazeboCurrent (CDi, CMi, regionRaster, size)
        
        canvas = plt.get_current_fig_manager().canvas
        canvas.draw()
        
        cImg = Image.frombytes('RGB', canvas.get_width_height(), 
             canvas.tostring_rgb())

        oldSize = cImg.size
        ratio = float (GAZEBO_IMG_DIM[0]) / max (oldSize)
        newSize = tuple ([int (x*ratio) for x in oldSize])
        cResize = cImg.resize (newSize, Image.ANTIALIAS)
        cNew = Image.new ("RGBA", GAZEBO_IMG_DIM, (255, 255, 255))
        cNew.paste (cResize, ((GAZEBO_IMG_DIM[0] - newSize[0]),
            (GAZEBO_IMG_DIM[1] - newSize[1])))

        outFile = "region2world_temp/WaterCurrentsIdx" + str (i) + ".png"
        cNew.save (outFile)

    return None

regionFile = '../../DataLayers/scenario1/Map/regionLand_2.tif'
currentsDirectionFile = '../../Playground/SimuRaster_CurrentsDirection.tif'
currentsMagnitudeFile = '../../Playground/SimuRaster_CurrentsMagnitude.tif'
targetsDir = '../../DataGeneration/experiments/results/EXP_R1_S9/';


region = Image.open (regionFile)
regionRaster = gdal.Open (regionFile)
currentsDirectionBrick = gdal.Open (currentsDirectionFile)
currentsMagnitudeBrick = gdal.Open (currentsMagnitudeFile)

if (currentsDirectionBrick.RasterCount !=  currentsMagnitudeBrick.RasterCount):
    print ("[-] Current directions brick differs in bands from currents magnitude brick")
    print ("[-] Directions: " + str (currentsDirectionBrick.RasterCount) + 
        ",Magnitudes: " + str (currentsMagnitudeBrick.RasterCount))


makeGazeboGeneric (targetsDir, "iter+", region)
exit ()


regionGazebo, oldSize, newSize = makeGazeboRegion (region)

currents (currentsDirectionBrick, currentsMagnitudeBrick, regionRaster)


print ("Input Dimensions: " + str (oldSize))
print ("Resized Dimension: " + str (newSize))
print ("Gazebo dimensions: " + str (GAZEBO_IMG_DIM))

