# DataLayers

This directory contains the information needed to create the layers of information used for path planning.
For example, "Map" contains GIS shapefiles of operating regions and 
"Current" contains points and interpolated rasters of water current readings/predictions. 

Each scenario has its own set of DataLayers. If a scenario has multiple instances of layers, this is because the layers can change over time. In this case, the layer file names will include a timestamp. 


### Map

Land and water region specifications

**Shapefiles**

- *regionExtent*: Rectangle defining region area.
- *regionLand*: Shapefile of terrain
- *regionWater*: Shapefile of water
- *regularGrid*: Regular points across region. Used for populating grid values by sampling the space.  

**Non Shapefiles**

- *regionLand.tif*: Raster of the map. Presence (green) is land. Absence (black) is water. Intended to use as grid configuration space.

### Current

Water current readings. Currently, this is taken from [The Northeast Coastal Ocean Forecast System (NECOFS)](http://fvcom.smast.umassd.edu/necofs/).

**Shapefiles**

- *currentsGridValues*: Sample of the interpolated grid at the regular interval defined by *regularGrid*
- *currentsGridValuesWater*: Just the *currentsGridValues* that intersect the water region.

**Non Shapefile**

- *currents.csv*: Current data. Has columns for latitude, longitude, u and v components.
- *currentsExp.csv*: Expanded. Includes columns for magnitude and direction, computed from u and v. 
- *currents_magnitude.tif*: Interpolated grid of current magnitude.
- *currents_direction.tif*: Interpolated grid of current direction.
- *currents_u.tif*: Interpolated grid of u components.
- *currents_v.tif*: Interpolated grid of v components.

### Target

The USV's target substrate. Seagrass is of interest in real life, but simulation will use a generic substrate model. 


