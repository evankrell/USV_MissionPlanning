# Evan Krell
# interpolateCurrents.py
# Uses Nearest Neighbor to convert current point data to raster grids (.tif)
# The grids are: magnitude, direction, u-component, v-component

# HEAVILY sourced from: http://geoexamples.blogspot.com/2012/05/creating-grid-from-scattered-data-using.html
# I basically just replaced the code inverse distance with nearest neighbor interpolation.


# Load libraries
from optparse import OptionParser
import numpy as np
from math import pow
from math import sqrt
from osgeo import gdal
from osgeo import osr
from osgeo import ogr
from osgeo.gdalconst import *
import sys


def pointValue(x,y,xv,yv,values):
	# Original "closest point" is infinite distance away
	closestPointDist = float('Inf')
	closestPointIdx = -1	

	for i in range(0,len(values)):
		dist = sqrt((x-xv[i])*(x-xv[i])+(y-yv[i])*(y-yv[i]));
		if (dist < closestPointDist):
			closestPointDist = dist
			closestPointIdx = i

	return values[closestPointIdx]
		


def nearestNeighbor(xv, yv, values, geotransform, proj, xSize, ySize, driverName, outFile):
	#Transform geographic coordinates to pixels
	for i in range(0,len(xv)):
		xv[i] = (xv[i]-geotransform[0])/geotransform[1]
	for i in range(0,len(yv)):
		yv[i] = (yv[i]-geotransform[3])/geotransform[5]
	#Creating the file
	driver = gdal.GetDriverByName( driverName )
	ds = driver.Create( outFile, xSize, ySize, 1, gdal.GDT_Float32)
	if proj is not None:
		ds.SetProjection(proj.ExportToWkt())
	ds.SetGeoTransform(geotransform)
	valuesGrid = np.zeros((ySize,xSize))
	
	#Getting the interpolated values
	for x in range(0,xSize):
		for y in range(0,ySize):
			valuesGrid[y][x] = pointValue(x,y,xv,yv,values)

	ds.GetRasterBand(1).WriteArray(valuesGrid)
	ds = None
	return valuesGrid


def readPoints(dataFile, zField='Z'):
	data = {}
	xv=[]
	yv=[]
	values=[]
	ds = ogr.Open(dataFile)
	if ds is None:
		raise Exception('Could not open ' + dataFile)


	layer = ds.GetLayer()
	proj = layer.GetSpatialRef()
	extent = layer.GetExtent()

	print (layer)
	print (proj)
	
	feature = layer.GetNextFeature()
	if feature.GetFieldIndex(zField) == -1:
		raise Exception('zField is not valid: ' + zField)

	while feature:
		geometry = feature.GetGeometryRef()
		xv.append(geometry.GetX())
		yv.append(geometry.GetY())
		values.append(feature.GetField(zField))
		feature = layer.GetNextFeature()

	data['extent'] = extent
	data['xv']=xv
	data['yv']=yv
	data['values']=values
	data['proj'] = proj
	ds = None
	return data	


def main():

	# Read options
	parser = OptionParser(usage="usage: %prog [options] filename",
                          version="%prog 0.01")
	parser.add_option("-d", "--dataFile",
                      action="store",
                      dest="dataFile",
                      default="currentsExp.csv",
                      help="currents CSV with header 'lon,lat,u,v,magnitude,direction")
	parser.add_option("-o", "--outputFile",
                      action="store",
                      dest="outputFile",
                      help="output interpolated raster geotif file")
	parser.add_option("-z", "--zField",
                      action="store",
                      dest="zField",
                      default="Z",
                      help="Name of data field to rasterize")
	parser.add_option("-x", "--xSize",
                      action="store", type = "int",
                      dest="xSize",
                      default="1000",
                      help="Number of pixels along x direction")
	parser.add_option("-y", "--ySize",
                      action="store", type = "int", 
                      dest="ySize",
                      default="1000",
                      help="Number of pixels along y direction")
	


	(options, args) = parser.parse_args()

	# Read water current data (typically "currentsExp.csv")

	# Default values
	zField=options.zField
	dataFile=options.dataFile
	outFile=options.outputFile
	driverName='GTiff'

	xSize=options.xSize
	ySize=options.ySize

	if dataFile is None or outFile is None:
		exit (1)

	try:
		data = readPoints(dataFile,zField)
	except Exception,ex:
		print ex
		sys.exit(0)
	xMin=data['extent'][0]
	xMax=data['extent'][1]
	yMin=data['extent'][2]
	yMax=data['extent'][3]

	geotransform=[]
	geotransform.append(xMin)
	geotransform.append((xMax-xMin)/xSize)
	geotransform.append(0)
	geotransform.append(yMax)
	geotransform.append(0)
	geotransform.append((yMin-yMax)/ySize)

	proj = data['proj']

	try:
		ZI = nearestNeighbor(data['xv'],data['yv'],data['values'],geotransform,proj,xSize,ySize,driverName,outFile)
	except Exception,ex:
		print ex
		sys.exit(0)

if __name__ == '__main__':
	main()
