# Evan Krell

# Converts a shapefile to a raster.
# While general purpose, the intent of the this script
# is to convert the 'regionLand' shapefile to a 'regionLand' raster
# Which represents both the "land" (presence) and "water" (absence).

# EXAMPLE: bash gdal_rasterize archive/map/regionLand rasters/regionLand.tif

# Parameters
INPUT_SHAPEFILE=$1   # DO NOT include the extension
OUTPUT_GEOTIF=$2    # DO include the '.tif' extension
XLEN=$3              # Number of pixels along x axis
YLEN=$4              # Number of pixels along y axis

# GREEN color for land:
R=100
G=200
B=100

# Extract just the filename from path
SHAPEFILE=$(basename $INPUT_SHAPEFILE)

gdal_rasterize -burn $R -burn $G -burn $B -ot Byte -l $SHAPEFILE $INPUT_SHAPEFILE"".shp -ts $XLEN $YLEN $OUTPUT_GEOTIF
