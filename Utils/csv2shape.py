from optparse import OptionParser
import pandas as pd
from geopandas import GeoDataFrame
from shapely.geometry import Point


def main():

	# Read options
	parser = OptionParser(usage="usage: %prog [options] filename",
		version="%prog 0.01")
	parser.add_option("-d", "--dataFile",
		action="store",
                dest="dataFile",
                default="currentsExp.csv",
                help="currents CSV with header lon,lat,u,v,magnitude,direction")
	parser.add_option("-o", "--outFile",
                action="store",
                dest="outFile",
                help="currents shapefile with attributes lon,lat,u,v,magnitude,direction")

	(options, args) = parser.parse_args()


	df = pd.read_csv(options.dataFile)

	geometry = [Point(xy) for xy in zip(df.lon, df.lat)]
	df = df.drop(['lon', 'lat'], axis=1)
	crs = {'init': 'epsg:4326'}
	gdf = GeoDataFrame(df, crs=crs, geometry=geometry)

	gdf.to_file(options.outFile)
	
if __name__ == '__main__':
	main()	


