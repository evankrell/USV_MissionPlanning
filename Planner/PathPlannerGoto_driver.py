import dill            as pickle
import PlannerVis      as PlannerVis
import PlannerTools    as PlannerTools
import PathPlannerGoto as PPG

def parseOptions():
    from optparse import OptionParser

    # Define options
    parser = OptionParser()
    parser.add_option("-c", "--config",         dest="config",         metavar="CONFIG",
        help="Path to config file")
    parser.add_option("-x", "--xpoints",        dest="xpoints",        metavar="XPOINTS",
        help="Comma-separated list of x components")
    parser.add_option("-y", "--ypoints",        dest="ypoints",        metavar="YPOINTS",
        help="Comma-separated list of y components")
    parser.add_option("-o", "--outdata",        dest="outdata",        metavar="OUTDATA",
        help="Output file to store pickled path data")
    parser.add_option("-f", "--outfigure",      dest="outfigure",      metavar="OUTFIGURE",
        help="Output file to store path figure")
    parser.add_option("-b", "--build_currents", dest="build_currents", metavar="BUILD_CURRENTS",
        action="store_false", default=True,
        help="Builds currents raster set. Otherwise, use existing rasters.")
    parser.add_option("-g", "--generations",    dest="generations",    metavar="GENERATIONS",
        help="Number of optimization generations")
    parser.add_option("-i", "--individuals",    dest="individuals",    metavar="INDIVIDUALS",
        help="Number of optimization pool individuals")
    parser.add_option("-w", "--weights",        dest="weights",        metavar="WEIGHTS",
        help="Weights for each fitness criteria")
    



    # Get options
    (options, args) = parser.parse_args()

    # Verify correct options
    if options.config  is None or options.xpoints is None or \
       options.ypoints is None or options.outdata is None or \
       options.outfigure is None:
        (options, args) = None, None

    return options, args

def main():

    # Parse options
    (options, args) = parseOptions()
    if options is None:
        print("Bad options")
        exit()

    # Load data
    environment = PlannerTools.initializeByFile(options.config, haveCurrents = options.build_currents)

    # Override configuration optimization settings
    if options.generations is not None:
        environment["plannerGoto"]["generations"] = int(options.generations)
    if options.individuals is not None:
        environment["plannerGoto"]["individuals"] = int(options.individuals)
    if options.weights is not None:
        weights = options.weights.split(',')
        environment["plannerGoto"]["distanceWeight"] = weights[0]
        environment["plannerGoto"]["obstacleWeight"] = weights[1]
        environment["plannerGoto"]["currentWeight"]  = weights[2]
        environment["plannerGoto"]["entityWeight"]   = weights[3]


    # Parse targets from options
    yCoords = [float(y) for y in options.ypoints.split(',')]
    xCoords = [float(x) for x in options.xpoints.split(',')]
    if len(xCoords) != len(yCoords):
        print ("number of x and y coordinates do not match")
        exit()
    targets = []
    for i in range(len(xCoords)):
        targets.append({ "Lat" : yCoords[i], "Lon" : xCoords[i] })

    start = targets.pop(0)
    end   = targets.pop(len(targets) - 1)

    # Do path planning
    gotoResults = []

    if len(targets) > 0:
        # Path frpm start to first target
        segment = PPG.driver(start, targets[0], environment)
        gotoResults.append(segment)
        # Path between each target
        for i in range(len(targets) - 1):
            segment = PPG.driver(targets[i], targets[i + 1], environment)
            gotoResults.append(segment) 
        # path from last target to end
        segment = PPG.driver(targets[len(targets)-1], end, environment)
        gotoResults.append(segment)
    else:
        segment = PPG.driver(start, end, environment)
        gotoResults.append(segment)

    gotoResults = [g[0] for g in gotoResults]

    # Map the results
    plannerGotoMap = PlannerVis.makeGotoMap(environment["region"]["raster"],
                         environment["region"]["file"], gotoResults,
                         5, options.outfigure)
    
    # Save results
    with open(options.outdata, 'wb') as outfile:
        pickle.dump(gotoResults, outfile, protocol = pickle.HIGHEST_PROTOCOL)
    

if __name__ == "__main__":
    main()
