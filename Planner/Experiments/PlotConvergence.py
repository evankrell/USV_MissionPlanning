
# Very hard coded
import pandas as pd
import matplotlib.pyplot as plt
import string
import numpy as np

# filenames
infile = "out/EXP3_CONVERGENCE_COMBO.csv"

algs = ["PSO", "DE", "GA", "BEE"]

results = [ {"alg" : a, 
             "worst_b" : None, "best_b" : None, "avg_b" : None} for a in algs]
# Load pandas
colnames = ["alg", "type", "run", "iteration", "fitness"]
df = pd.read_csv(infile, names = colnames)


df_a = df[df["type"] == "A"]
df_a.groupby(["iteration", "alg"]).aggregate(np.mean)['fitness'].unstack().plot(linewidth = 4)
plt.ylabel("Fitness")
plt.ylim(0, 6000)
plt.xlabel("Iteration")
plt.title("Average convergence for each algorithm, terrain only")
plt.savefig("conv_avg_a.eps", bbox_inches='tight', 
                       format = 'eps', dpi = 1000)

plt.clf()
 
df_b = df[df["type"] == "B"]
df_b.groupby(["iteration", "alg"]).aggregate(np.mean)['fitness'].unstack().plot(linewidth = 4)
plt.ylabel("Fitness")
plt.xlabel("Iteration")
plt.title("Average convergence for each algorithm: terrain, work, and reward")
plt.savefig("conv_avg_b.eps", bbox_inches='tight', 
                       format = 'eps', dpi = 1000)

plt.clf()
 

for r in results:
    dfalg = df[df["alg"] == r["alg"]]
    r["avg_a"] = dfalg[df["type"] == "A"].groupby(['run','iteration']).mean()
    r["avg_b"] = dfalg[df["type"] == "B"].groupby(['run','iteration']).mean()



    dfalga = dfalg[df["type"] == "A"]
    dfalgb = dfalg[df["type"] == "B"]

    fig = plt.figure(figsize = (10,10))
    ax = fig.add_subplot(111)

    minval_a = dfalga["fitness"].min()
    maxval_a = dfalga[dfalga["iteration"] == 99]["fitness"].max()
    print(r["alg"], "A", minval_a, maxval_a, maxval_a - minval_a)

    dfalga.groupby(['iteration','run']).sum().unstack().plot()
    plt.ylabel("Fitness")
    plt.ylim(0, 10000)
    plt.xlabel("Iteration")
    plt.axhline(y = minval_a, color = 'r', linestyle = '--', label = str(minval_a))
    plt.text(1, minval_a - 5, "{0:.2f}".format(minval_a), fontsize = 12)
    plt.legend(labels=["Run {}".format(str(i)) for i in range(1, 11)])
    plt.title("Convergence for {}, terrain only".format(r["alg"]), y = 1.05)
    plt.gca().legend_.remove()
    plt.savefig("conv_{}_a.eps".format(r["alg"]), bbox_inches='tight', 
                                           format = 'eps', dpi = 1000)

    minval_b = dfalgb["fitness"].min()
    maxval_b = dfalgb[dfalgb["iteration"] == 99]["fitness"].max()
    print(r["alg"], "B", minval_b, maxval_b, maxval_b - minval_b)

    dfalgb.groupby(['iteration','run']).sum().unstack().plot()
    plt.ylabel("Fitness")
    plt.ylim(1000, 50000)
    plt.xlabel("Iteration")
    plt.axhline(y = minval_b, color = 'r', linestyle = '--', label = str(minval_a))
    plt.text(1, minval_b - 5, "{0:.2f}".format(minval_b), fontsize = 12)
    plt.legend(labels=["Run {}".format(str(i)) for i in range(1, 11)])
    plt.title("Convergence for {}, terrain, work, and reward".format(r["alg"]), y = 1.05)
    plt.gca().legend_.remove()
    plt.savefig("conv_{}_b.eps".format(r["alg"]), bbox_inches='tight', 

                                           format = 'eps', dpi = 1000)

