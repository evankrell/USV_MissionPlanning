import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


regions = ["R1", "R2", "R3", "R4", "R5"]

work_colwise_updates   = [ 513465.892, 11903001.793, 47145978.158, 105728388.430,  187649992.045 ]

work_rowwise_updates   = [ 513452.073, 11890847.012, 47119054.520, 105685825.846, 187181080.951 ]

work_colwise_noupdates = [ 513465.892, 11903001.793, 47146078.734, 105704102.314, 187497561.286 ]

work_rowwise_noupdates = [ 513452.073, 11890847.012, 47124199.557, 105728540.700, 187650706.827 ]

work_diff_updates      = [ 13.819, 12154.781, 26923.638, 42562.584, 468911.094 ]

work_diff_noupdates    = [ 13.819, 12154.781, 21879.177, 24438.386, 153145.541 ]

work_diff_upornot      = [ 0, 0, 5044.461, 18124.198, 315765.553 ]


df = pd.DataFrame(
    { 
      "region"                 : regions,
      "work_colwise_updates"   : work_colwise_updates,
      "work_rowwise_updates"   : work_rowwise_updates,
      "work_colwise_noupdates" : work_colwise_noupdates,
      "work_rowwise_noupdates" : work_rowwise_noupdates,
    }).transpose()

df2 = pd.DataFrame(
    {
      "region"                 : regions,
      "work_diff_updates"      : work_diff_updates,
      "work_diff_noupdates"    : work_diff_noupdates,
      "work_diff_upornot"      : work_diff_upornot,
    }).transpose()


new_header = df.iloc[0] #grab the first row for the header
df = df[1:] #take the data less the header row
df.columns = new_header #set the header row as the df header

new_header = df2.iloc[0] #grab the first row for the header
df2 = df2[1:] #take the data less the header row
df2.columns = new_header #set the header row as the df header



print (df)


sns.set_palette(sns.color_palette("colorblind"))

sns.barplot(x=df.index, y='R1', data=df)
plt.ticklabel_format(style='plain', axis='y')
plt.xticks(rotation=90)
plt.ylabel("Work")
plt.ylim(df["R1"].min() - 1000, df["R1"].max() + 1000)
plt.title("Work in region 1", y = 1.05)
plt.gca().set_xticklabels(["column-wise\n(no updates)", "column-wise\n(updates)", "row-wise\n(no updates)", "row-wise\n(updates)"])
plt.savefig("work_r1.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()
sns.barplot(x=df.index, y='R2', data=df)
plt.ticklabel_format(style='plain', axis='y')
plt.xticks(rotation=90)
plt.ylabel("Work")
plt.ylim(df["R2"].min() - 1000, df["R2"].max() + 1000)
#plt.ylim((450000, 550000))
plt.title("Work in region 2", y = 1.05)
plt.gca().set_xticklabels(["column-wise\n(no updates)", "column-wise\n(updates)", "row-wise\n(no updates)", "row-wise\n(updates)"])
plt.savefig("work_r2.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()
sns.barplot(x=df.index, y='R3', data=df)
plt.ticklabel_format(style='plain', axis='y')
plt.xticks(rotation=90)
plt.ylabel("Work")
plt.ylim(df["R3"].min() - 1000, df["R3"].max() + 1000)
#plt.ylim((450000, 550000))
plt.title("Work in region 3", y = 1.05)
plt.gca().set_xticklabels(["column-wise\n(no updates)", "column-wise\n(updates)", "row-wise\n(no updates)", "row-wise\n(updates)"])
plt.savefig("work_r3.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()
sns.barplot(x=df.index, y='R4', data=df)
plt.ticklabel_format(style='plain', axis='y')
plt.xticks(rotation=90)
plt.ylabel("Work")
plt.ylim(df["R4"].min() - 1000, df["R4"].max() + 1000)
#plt.ylim((450000, 550000))
plt.title("Work in region 4", y = 1.05)
plt.gca().set_xticklabels(["column-wise\n(no updates)", "column-wise\n(updates)", "row-wise\n(no updates)", "row-wise\n(updates)"])
plt.savefig("work_r4.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.show()
plt.clf()
sns.barplot(x=df.index, y='R5', data=df)
plt.ticklabel_format(style='plain', axis='y')
plt.xticks(rotation=90)
plt.ylabel("Work")
plt.ylim(df["R5"].min() - 10000, df["R5"].max() + 10000)
#plt.ylim((450000, 550000))
plt.title("Work in region 5", y = 1.05)
plt.gca().set_xticklabels(["column-wise\n(no updates)", "column-wise\n(updates)", "row-wise\n(no updates)", "row-wise\n(updates)"])
plt.savefig("work_r5.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()

sns.barplot(x=df2.index, y='R1', data=df2)
plt.ticklabel_format(style='plain', axis='y')
plt.xticks(rotation=90)
plt.ylabel("Work difference")
plt.ylim(0, df2["R1"].max() + 10)
plt.title("Work difference in region 1", y = 1.05)
plt.gca().set_xticklabels(["row-wise vs column-wise\n(static)", "row-wise vs column-wise\n(updates)", "static vs updates"])
plt.savefig("diff_r1.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()
sns.barplot(x=df2.index, y='R2', data=df2)
plt.ticklabel_format(style='plain', axis='y')
plt.xticks(rotation=90)
plt.ylabel("Work difference")
plt.ylim(0, df2["R2"].max() + 1000)
plt.title("Work difference in region 2", y = 1.05)
plt.gca().set_xticklabels(["row-wise vs column-wise\n(static)", "row-wise vs column-wise\n(updates)", "static vs updates"])
plt.savefig("diff_r2.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()
sns.barplot(x=df2.index, y='R3', data=df2)
plt.ticklabel_format(style='plain', axis='y')
plt.xticks(rotation=90)
plt.ylabel("Work difference")
plt.ylim(0, df2["R3"].max() + 1000)
plt.title("Work difference in region 3", y = 1.05)
plt.gca().set_xticklabels(["row-wise vs column-wise\n(static)", "row-wise vs column-wise\n(updates)", "static vs updates"])
plt.savefig("diff_r3.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()
sns.barplot(x=df2.index, y='R4', data=df2)
plt.ticklabel_format(style='plain', axis='y')
plt.xticks(rotation=90)
plt.ylabel("Work difference")
plt.ylim(0, df2["R4"].max() + 1000)
plt.title("Work difference in region 4", y = 1.05)
plt.gca().set_xticklabels(["row-wise vs column-wise\n(static)", "row-wise vs column-wise\n(updates)", "static vs updates"])
plt.savefig("diff_r4.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()
sns.barplot(x=df2.index, y='R5', data=df2)
plt.ticklabel_format(style='plain', axis='y')
plt.xticks(rotation=90)
plt.ylabel("Work difference")
plt.ylim(0, df2["R5"].max() + 10000)
plt.title("Work difference in region 5", y = 1.05)
plt.gca().set_xticklabels(["row-wise vs column-wise\n(static)", "row-wise vs column-wise\n(updates)", "static vs updates"])
plt.savefig("diff_r5.eps", bbox_inches='tight', format = 'eps', dpi = 1000)

