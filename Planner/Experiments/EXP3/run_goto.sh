#!/bin/bash

getCommandStr() {
    CONFIG=$1
    YPOINTS=$2
    XPOINTS=$3
    OUTDATA=$4
    OUTFIGURE=$5
    GENERATIONS=$6
    INDIVIDUALS=$7
    WEIGHTS=$8

    CMD="../../PathPlannerGoto_driver.py -c $CONFIG -x $XPOINTS -y $YPOINTS -o $OUTDATA"
    CMD="$CMD -f $OUTFIGURE -g $GENERATIONS -i $INDIVIDUALS -w $WEIGHTS"

    echo $CMD
}

# Setup experiment constants
CONFIG=config.yaml
GENERATIONS="100"
INDIVIDUALS="100"

# Path Aa: 1 -> 5
# Path Ab: 5 -> 1
# Path Ba: 5 -> 2 
# Path Bb: 2 -> 5
# Path Ca: 1 -> 3
# Path Cb: 3 -> 1

###########
# Path Aa #
###########
YPOINTS=42.300,42.348
XPOINTS=-70.970,-70.930

    # No obstacles, no work, no reward
OUTDATA="EXP3RG_PathAa_-1_0_0_0.pickle"
OUTFIGURE="EXP3RG_PathAa_-1_0_0_0.png"
WEIGHTS="-1,0,0,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, no work, no reward
OUTDATA="EXP3RG_PathAa_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathAa_-1_-1_0_0.png"
WEIGHTS="-1,-1,0,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, yes work, no reward
OUTDATA="EXP3RG_PathAa_-1_-1_0.001_0.pickle"
OUTFIGURE="EXP3RG_PathAa_-1_-1_0.001_0.png"
WEIGHTS="-1,-1,0.001,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, no work, yes reward
OUTDATA="EXP3RG_PathAa_-1_-1_0_0d01.pickle"
OUTFIGURE="EXP3RG_PathAa_-1_-1_0_0d01.png"
WEIGHTS="-1,-1,0,0.01"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, yes work, yes reward
OUTDATA="EXP3RG_PathAa_-1_-1_0d01_0d005.pickle"
OUTFIGURE="EXP3RG_PathAa_-1_-1_0d01_0d005.png"
WEIGHTS="-1,-1,0.01,0.005"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &


###########
# Path Ab #
###########
YPOINTS=42.348,42.300
XPOINTS=-70.930,-70.970

    # No obstacles, no work, no reward
OUTDATA="EXP3RG_PathAb_-1_0_0_0.pickle"
OUTFIGURE="EXP3RG_PathAb_-1_0_0_0.png"
WEIGHTS="-1,0,0,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, no work, no reward
OUTDATA="EXP3RG_PathAb_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathAb_-1_-1_0_0.png"
WEIGHTS="-1,-1,0,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, yes work, no reward
OUTDATA="EXP3RG_PathAb_-1_-1_0.001_0.pickle"
OUTFIGURE="EXP3RG_PathAb_-1_-1_0.001_0.png"
WEIGHTS="-1,-1,0.001,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, no work, yes reward
OUTDATA="EXP3RG_PathAb_-1_-1_0_0d01.pickle"
OUTFIGURE="EXP3RG_PathAb_-1_-1_0_0d01.png"
WEIGHTS="-1,-1,0,0.01"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, yes work, yes reward
OUTDATA="EXP3RG_PathAb_-1_-1_0d01_0d005.pickle" 
OUTFIGURE="EXP3RG_PathAb_-1_-1_0d01_0d005.png"
WEIGHTS="-1,-1,0.01,0.005" 
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &


####################
# Path Ba (5 -> 2) #
####################
YPOINTS=42.348,42.327
XPOINTS=-70.930,-70.881

    # No obstacles, no work, no reward
OUTDATA="EXP3RG_PathBa_-1_0_0_0.pickle"
OUTFIGURE="EXP3RG_PathBa_-1_0_0_0.png"
WEIGHTS="-1,0,0,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, no work, no reward
OUTDATA="EXP3RG_PathBa_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathBa_-1_-1_0_0.png"
WEIGHTS="-1,-1,0,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, yes work, no reward
OUTDATA="EXP3RG_PathBa_-1_-1_0.001_0.pickle"
OUTFIGURE="EXP3RG_PathBa_-1_-1_0.001_0.png"
WEIGHTS="-1,-1,0.001,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, no work, yes reward
OUTDATA="EXP3RG_PathBa_-1_-1_0_0d01.pickle"
OUTFIGURE="EXP3RG_PathBa_-1_-1_0_0d01.png"
WEIGHTS="-1,-1,0,0.01"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, yes work, yes reward
OUTDATA="EXP3RG_PathBa_-1_-1_0d01_0d005.pickle" 
OUTFIGURE="EXP3RG_PathBa_-1_-1_0d01_0d005.png"
WEIGHTS="-1,-1,0.01,0.005" 
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &


###########
# Path Bb #
###########
YPOINTS=42.327,42.348
XPOINTS=-70.881,-70.930


    # No obstacles, no work, no reward
OUTDATA="EXP3RG_PathBb_-1_0_0_0.pickle"
OUTFIGURE="EXP3RG_PathBb_-1_0_0_0.png"
WEIGHTS="-1,0,0,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, no work, no reward
OUTDATA="EXP3RG_PathBb_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathBb_-1_-1_0_0.png"
WEIGHTS="-1,-1,0,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, yes work, no reward
OUTDATA="EXP3RG_PathBb_-1_-1_0.001_0.pickle"
OUTFIGURE="EXP3RG_PathBb_-1_-1_0.001_0.png"
WEIGHTS="-1,-1,0.001,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, no work, yes reward
OUTDATA="EXP3RG_PathBb_-1_-1_0_0d01.pickle"
OUTFIGURE="EXP3RG_PathBb_-1_-1_0_0d01.png"
WEIGHTS="-1,-1,0,0.01"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, yes work, yes reward
OUTDATA="EXP3RG_PathBb_-1_-1_0d01_0d005.pickle" 
OUTFIGURE="EXP3RG_PathBb_-1_-1_0d01_0d005.png"
WEIGHTS="-1,-1,0.01,0.005" 
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &



####################
# Path Ca (1 -> 3) #
####################
YPOINTS=42.300,42.287
XPOINTS=-70.970,-70.920

    # No obstacles, no work, no reward
OUTDATA="EXP3RG_PathCa_-1_0_0_0.pickle"
OUTFIGURE="EXP3RG_PathCa_-1_0_0_0.png"
WEIGHTS="-1,0,0,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, no work, no reward
OUTDATA="EXP3RG_PathCa_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathCa_-1_-1_0_0.png"
WEIGHTS="-1,-1,0,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, yes work, no reward
OUTDATA="EXP3RG_PathCa_-1_-1_0.001_0.pickle"
OUTFIGURE="EXP3RG_PathCa_-1_-1_0.001_0.png"
WEIGHTS="-1,-1,0.001,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, no work, yes reward
OUTDATA="EXP3RG_PathCa_-1_-1_0_0d01.pickle"
OUTFIGURE="EXP3RG_PathCa_-1_-1_0_0d01.png"
WEIGHTS="-1,-1,0,0.01"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, yes work, yes reward
OUTDATA="EXP3RG_PathCa_-1_-1_0d01_0d005.pickle" 
OUTFIGURE="EXP3RG_PathCa_-1_-1_0d01_0d005.png"
WEIGHTS="-1,-1,0.01,0.005" 
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &


###########
# Path Cb #
###########
YPOINTS=42.287,42.300
XPOINTS=-70.920,-70.970

    # No obstacles, no work, no reward
OUTDATA="EXP3RG_PathCb_-1_0_0_0.pickle"
OUTFIGURE="EXP3RG_PathCb_-1_0_0_0.png"
WEIGHTS="-1,0,0,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, no work, no reward
OUTDATA="EXP3RG_PathCb_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathCb_-1_-1_0_0.png"
WEIGHTS="-1,-1,0,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, yes work, no reward
OUTDATA="EXP3RG_PathCb_-1_-1_0.001_0.pickle"
OUTFIGURE="EXP3RG_PathCb_-1_-1_0.001_0.png"
WEIGHTS="-1,-1,0.001,0"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, no work, yes reward
OUTDATA="EXP3RG_PathCb_-1_-1_0_0d01.pickle"
OUTFIGURE="EXP3RG_PathCb_-1_-1_0_0d01.png"
WEIGHTS="-1,-1,-1,0.01"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
#echo "start: " $CMD
#python $CMD &

    # Yes obstacles, yes work, yes reward
OUTDATA="EXP3RG_PathCb_-1_-1_0d01_0d005.pickle" 
OUTFIGURE="EXP3RG_PathCb_-1_-1_0d01_0d005.png"
WEIGHTS="-1,-1,0.01,0.005" 
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &

wait
exit

