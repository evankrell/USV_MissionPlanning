#!/bin/bash

getCommandStr() {
    CONFIG=$1
    YPOINTS=$2
    XPOINTS=$3
    OUTDATA=$4
    OUTFIGURE=$5
    GENERATIONS=$6
    INDIVIDUALS=$7
    WEIGHTS=$8

    CMD="../../PathPlannerGoto_driver.py -c $CONFIG -x $XPOINTS -y $YPOINTS -o $OUTDATA"
    CMD="$CMD -f $OUTFIGURE -g $GENERATIONS -i $INDIVIDUALS -w $WEIGHTS"

    echo $CMD
}

# Setup experiment constants
CONFIG=config.yaml
GENERATIONS="100"
INDIVIDUALS="100"

#####################
# Path Bb : 2 -> 5 #
#####################
YPOINTS=42.327,42.348 
XPOINTS=-70.881,-70.930 

# Only obstacles, terrain
WEIGHTS="-1,-1,0,0"
# run1
OUTDATA="EXP3RG_PathBb_run1_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathBb_run1_-1_-1_0_0.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &


# run2
OUTDATA="EXP3RG_PathBb_run2_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathBb_run2_-1_-1_0_0.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run3
OUTDATA="EXP3RG_PathBb_run3_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathBb_run3_-1_-1_0_0.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run4
OUTDATA="EXP3RG_PathBb_run4_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathBb_run4_-1_-1_0_0.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run5
OUTDATA="EXP3RG_PathBb_run5_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathBb_run5_-1_-1_0_0.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run6
OUTDATA="EXP3RG_PathBb_run6_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathBb_run6_-1_-1_0_0.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run7
OUTDATA="EXP3RG_PathBb_run7_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathBb_run7_-1_-1_0_0.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run8
OUTDATA="EXP3RG_PathBb_run8_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathBb_run8_-1_-1_0_0.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run9
OUTDATA="EXP3RG_PathBb_run9_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathBb_run9_-1_-1_0_0.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run10
OUTDATA="EXP3RG_PathBb_run10_-1_-1_0_0.pickle"
OUTFIGURE="EXP3RG_PathBb_run10_-1_-1_0_0.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &


# Obstacles, terrain, work, reward
WEIGHTS="-1,-1,0.01,0.005"
# run1
OUTDATA="EXP3RG_PathBb_run1_-1_-1_0d01_0d005.pickle"
OUTFIGURE="EXP3RG_PathBb_run1_-1_-1_0d01_0d005.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run2
OUTDATA="EXP3RG_PathBb_run2_-1_-1_0d01_0d005.pickle"
OUTFIGURE="EXP3RG_PathBb_run2_-1_-1_0d01_0d005.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run3
OUTDATA="EXP3RG_PathBb_run3_-1_-1_0d01_0d005.pickle"
OUTFIGURE="EXP3RG_PathBb_run3_-1_-1_0d01_0d005.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run4
OUTDATA="EXP3RG_PathBb_run4_-1_-1_0d01_0d005.pickle"
OUTFIGURE="EXP3RG_PathBb_run4_-1_-1_0d01_0d005.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run5
OUTDATA="EXP3RG_PathBb_run5_-1_-1_0d01_0d005.pickle"
OUTFIGURE="EXP3RG_PathBb_run5_-1_-1_0d01_0d005.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run6
OUTDATA="EXP3RG_PathBb_run6_-1_-1_0d01_0d005.pickle"
OUTFIGURE="EXP3RG_PathBb_run6_-1_-1_0d01_0d005.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run7
OUTDATA="EXP3RG_PathBb_run7_-1_-1_0d01_0d005.pickle"
OUTFIGURE="EXP3RG_PathBb_run7_-1_-1_0d01_0d005.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run8
OUTDATA="EXP3RG_PathBb_run8_-1_-1_0d01_0d005.pickle"
OUTFIGURE="EXP3RG_PathBb_run8_-1_-1_0d01_0d005.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run9
OUTDATA="EXP3RG_PathBb_run9_-1_-1_0d01_0d005.pickle"
OUTFIGURE="EXP3RG_PathBb_run9_-1_-1_0d01_0d005.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &
# run10
OUTDATA="EXP3RG_PathBb_run10_-1_-1_0d01_0d005.pickle"
OUTFIGURE="EXP3RG_PathBb_run10_-1_-1_0d01_0d005.png"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $OUTDATA $OUTFIGURE $GENERATIONS $INDIVIDUALS $WEIGHTS)
echo "start: " $CMD
python $CMD &





wait
exit

