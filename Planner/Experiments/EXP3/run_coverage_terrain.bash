#/bin/bash 
# Individually test the coverage paths in experiment 3

getCommandStr() {
    CONFIG=$1
    YPOINTS=$2
    XPOINTS=$3
    LENGTHS=$4
    WIDTHS=$5
    NUMY=$6
    NUMX=$7
    MARGIN=$8
    ROAM=$9
    OUTDATA=${10}
    OUTFIGURE=${11}
    WEIGHTS=${12}

    CMD="../../PathPlannerCoverage_driver.py -c $CONFIG -x $XPOINTS -y $YPOINTS"
    CMD="$CMD -w $WIDTHS -l $LENGTHS -k $NUMX -j $NUMY -m $MARGIN -r $ROAM"
    CMD="$CMD -o $OUTDATA -f $OUTFIGURE -e $WEIGHTS" 

    echo $CMD
}


# Setup experiment constants
CONFIG="config.yaml"
MARGIN="1"
ROAM="0"


# Run 1: Target 1
YPOINTS="42.311699"
XPOINTS="-70.95326"
LENGTHS="5"
WIDTHS="5"
NUMY="5"
NUMX="5"
OUTDATA="EXP3RC_T1.pickle"
OUTFIGURE="EXP3RC_T1.png"
WEIGHTS="-1,-1,-1,-1,-1"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $LENGTHS $WIDTHS $NUMY $NUMX \
      $MARGIN $ROAM $OUTDATA $OUTFIGURE $WEIGHTS)
echo "start: " $CMD
time python $CMD



# Run 1: Target 1
YPOINTS="42.311"
XPOINTS="-70.953"
LENGTHS="10"
WIDTHS="10"
NUMY="10"
NUMX="10"
OUTDATA="EXP3RC_T1.pickle"
OUTFIGURE="EXP3RC_T1.png"
WEIGHTS="-1,-1,-1,-1,-1"
CMD=$(getCommandStr $CONFIG $YPOINTS $XPOINTS $LENGTHS $WIDTHS $NUMY $NUMX \
      $MARGIN $ROAM $OUTDATA $OUTFIGURE $WEIGHTS)
echo "start: " $CMD
time python $CMD

