## USV Mission Planner 
### Experiment 1


This experiment is for point-to-point GOTO planner.

**Questions:**

- Does the GOTO planner produce an ~optimal path?
- What are the bounds on convergence?
- How do the parameters affect the result?


