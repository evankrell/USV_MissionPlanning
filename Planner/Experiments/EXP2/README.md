## USV Mission Planner
### Experiment 2

This experiment is for lawnmower Coverage planner, used when no obstacles.

**Questions:**

- Does the Coverage planner produce an ~optimal path?
- How does the growth of the region space affect the path generation time?
- Does the choice between rowwise and columnwise appreciably affect fitness?
