import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

mfile="EXP4_MPFC_FULL.csv"
V = "MPFC"
E = "EXP4"

sDistance = "Distance" + V
sDuration = "Duration" + V
sWork     = "Work"     + V
sReward   = "Reward"   + V

# All missions
df = pd.read_csv(mfile)
df['Category'] = pd.Series(["No Best" for i in range(len(df.index))], index=df.index)

# Mission with min duration
minDurationIdx = df[sDuration].idxmin()
print ("Min Duration Mission", df.ix[minDurationIdx])
df.loc[minDurationIdx, "Category"] = "Has Best"
# Mission with min work
minWorkIdx = df[sWork].idxmin()
print ("Min Work Mission", df.ix[minWorkIdx])
df.loc[minWorkIdx, "Category"] = "Has Best"
# Mission with max reward
maxRewardIdx = df[sReward].idxmax()
print ("Max Reward Mission", df.ix[maxRewardIdx])
df.loc[maxRewardIdx, "Category"] = "Has Best"


print(df[df["Category"] == "Has Best"])

fig, axs = plt.subplots(ncols=3)
sns.boxplot(data = df, y = sDuration, x = "Category", palette="colorblind", ax = axs[0])
sns.boxplot(data = df, y = sWork,     x = "Category", palette="colorblind", ax = axs[1])
sns.boxplot(data = df, y = sReward,   x = "Category", palette="colorblind", ax = axs[2])
for ax in fig.axes:
    plt.sca(ax)
    plt.xticks(rotation=90)
fig.tight_layout() 
plt.savefig(E + "_" + V + "_box_r.eps", bbox_inches='tight', format = 'eps', dpi = 1000)


# Disable Reward's Best
df.loc[maxRewardIdx, "Category"] = "No Best"
fig, axs = plt.subplots(ncols=3)
sns.boxplot(data = df, y = sDuration, x = "Category", palette="colorblind", ax = axs[0])
sns.boxplot(data = df, y = sWork,     x = "Category", palette="colorblind", ax = axs[1])
sns.boxplot(data = df, y = sReward,   x = "Category", palette="colorblind", ax = axs[2])
for ax in fig.axes:
    plt.sca(ax)
    plt.xticks(rotation=90)
fig.tight_layout() 
plt.savefig(E + "_" + V + "_box_nr.eps", bbox_inches='tight', format = 'eps', dpi = 1000)


