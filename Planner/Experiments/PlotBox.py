
# Very hard coded
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib import rc

#rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
#rc('text', usetex=True)

# filenames
infile = "out/EXP3_HISTO_COMBO.csv"

algs = ["PSO", "DE", "GA", "BEE"]

df = pd.read_csv(infile)


# Compare, type A
dfa = df[df["type"] == "A"]


# plot distance
bplot=sns.boxplot(y='distance', x='alg', 
                 data=dfa, 
                 width=0.5,
                 palette="colorblind")
# add swarmplot
bplot=sns.swarmplot(y='distance', x='alg',
              data=dfa,
              color='black',
              alpha=0.75)
plt.ylabel("Path distance")
plt.xlabel("Algorithm")
plt.title("Path distance when optimizing terrain, obstacle avoidance", y = 1.05)
plt.savefig("EXP3_histo_distance_a.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()


# plot distance
bplot=sns.boxplot(y='duration', x='alg', 
                 data=dfa, 
                 width=0.5,
                 palette="colorblind")
# add swarmplot
bplot=sns.swarmplot(y='duration', x='alg',
              data=dfa,
              color='black',
              alpha=0.75)
plt.ylabel("Path duration")
plt.xlabel("Algorithm")
plt.title("Path duration when optimizing terrain, obstacle avoidance", y = 1.05)
plt.savefig("EXP3_histo_duration_a.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()


# plot distance
bplot=sns.boxplot(y='work', x='alg', 
                 data=dfa, 
                 width=0.5,
                 palette="colorblind")
# add swarmplot
bplot=sns.swarmplot(y='work', x='alg',
              data=dfa,
              color='black',
              alpha=0.75)
plt.ylabel("Path work")
plt.xlabel("Algorithm")
plt.title("Path work when optimizing terrain, obstacle avoidance", y = 1.05)
plt.savefig("EXP3_histo_work_a.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()


# plot distance
bplot=sns.boxplot(y='reward', x='alg', 
                 data=dfa, 
                 width=0.5,
                 palette="colorblind")
# add swarmplot
bplot=sns.swarmplot(y='reward', x='alg',
              data=dfa,
              color='black',
              alpha=0.75)
plt.ylabel("Path reward")
plt.xlabel("Algorithm")
plt.title("Path reward when optimizing terrain, obstacle avoidance", y = 1.05)
plt.savefig("EXP3_histo_reward_a.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()




# Compare, type B
dfb = df[df["type"] == "B"]


# plot distance
bplot=sns.boxplot(y='distance', x='alg', 
                 data=dfb, 
                 width=0.5,
                 palette="colorblind")
## add swarmplot
bplot=sns.swarmplot(y='distance', x='alg',
              data=dfb,
              color='black',
              alpha=0.75)
plt.ylabel("Path distance")
plt.xlabel("Algorithm")
plt.title("Path distance when optimizing terrain, work, reward, obstacle avoidance", y = 1.05)
plt.savefig("EXP3_histo_distance_b.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()


# plot distance
bplot=sns.boxplot(y='duration', x='alg', 
                 data=dfb, 
                 width=0.5,
                 palette="colorblind")
## add swarmplot
bplot=sns.swarmplot(y='duration', x='alg',
              data=dfb,
              color='black',
              alpha=0.75)
plt.ylabel("Path duration")
plt.xlabel("Algorithm")
plt.title("Path duration when optimizing terrain, work, reward, obstacle avoidance", y = 1.05)
plt.savefig("EXP3_histo_duration_b.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()


# plot distance
bplot=sns.boxplot(y='work', x='alg', 
                 data=dfb, 
                 width=0.5,
                 palette="colorblind")
## add swarmplot
bplot=sns.swarmplot(y='work', x='alg',
              data=dfb,
              color='black',
              alpha=0.75)
plt.ylabel("Path work")
plt.xlabel("Algorithm")
plt.title("Path work when optimizing terrain, work, reward, obstacle avoidance", y = 1.05)
plt.savefig("EXP3_histo_work_b.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()


# plot distance
bplot=sns.boxplot(y='reward', x='alg', 
                 data=dfb, 
                 width=0.5,
                 palette="colorblind")
## add swarmplot
bplot=sns.swarmplot(y='reward', x='alg',
              data=dfb,
              color='black',
              alpha=0.75)
plt.ylabel("Path reward")
plt.xlabel("Algorithm")
plt.title("Path reward when optimizing terrain, work, reward, obstacle avoidance", y = 1.05)
plt.savefig("EXP3_histo_reward_b.eps", bbox_inches='tight', format = 'eps', dpi = 1000)
plt.clf()


