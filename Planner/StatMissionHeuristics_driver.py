import dill                  as pickle
import PlannerVis            as PlannerVis
import PlannerTools          as PlannerTools
import StatMissionHeuristics as SMH
import pandas                as pd

def parseOptions():
    from optparse import OptionParser

    # Define options
    parser = OptionParser()
    parser.add_option("-c", "--config",         dest="config",         metavar="CONFIG",
        help="Path to config file")
    parser.add_option("-m", "--missions",       dest="missions",      metavar="MISSIONS",
        help="Path to missions csv")
    parser.add_option("-o", "--outdata",        dest="outdata",        metavar="OUTDATA",
        help="Output file to store pickled path data")
    #parser.add_option("-f", "--outfigure",      dest="outfigure",      metavar="OUTFIGURE",
    #    help="Output file to store path figure")
    parser.add_option("-b", "--build_currents", dest="build_currents", metavar="BUILD_CURRENTS",
        action="store_false", default=True,
        help="Builds currents raster set. Otherwise, use existing rasters.")
    parser.add_option("-r", "--reuse_planners", dest="reuse_planners", metavar="REUSE_PLANNERS",
        action="store_true", default=False,
        help="Reuse the planned paths from pickle file (-o) instead of planning")

    # Get options
    (options, args) = parser.parse_args()

    # Verify correct options
    if options.config  is None or options.outdata is None:
        (options, args) = None, None

    return options, args

def main():
    # Parse options
    (options, args) = parseOptions()
    if options is None:
        print("Bad options")
        exit()

    # Load data
    environment = PlannerTools.initializeByFile(options.config, haveCurrents = options.build_currents)
    targetsTable    = PlannerTools.initTargetsTableByFile(environment["targets"]["table"])
    safepointsTable = PlannerTools.initSafepointsTableByFile(environment["vehicle"]["safepointsFile"])
    # Hacky rename since I don't like the original name, but need to maintain compatability
    environment["logbook"] = environment["targets"]["weights"]

    startPoint = { "Lat" : environment["vehicle"]["startCoords"][1],
                   "Lon" : environment["vehicle"]["startCoords"][0],
    }

    
    if options.missions is not None:
        missionDF = pd.read_csv(options.missions)
    else:
        missionDF = None

    if options.reuse_planners == True:
        reusePickle = options.outdata
    else:
        reusePickle = None
           

    gotoMeasures, coverageMeasures, missions= SMH.statMissions(targetsTable,
           startPoint, safepointsTable, environment, missionDF, reusePickle)


    if options.outdata is not None:
        json_data = { "gotoMeasures"     : gotoMeasures,
                      "coverageMeasures" : coverageMeasures,
                      "missions"         : missions,
        } 
        pickle.dump(json_data, open(options.outdata, 'wb'), protocol = pickle.HIGHEST_PROTOCOL)
    else:
        print ("Could not save with no outfile specified")

 
if __name__ == "__main__":
    main()
