'''
PathPlannerGoto.py
Author: Evan Krell

A metaheuristic-based path planner to
travel to a goal location while miniziming distance,
minimizing energy use, avoiding obstacles, and 
maximiing target capture. 
'''

from PyGMO.problem import base
from PyGMO import algorithm, island, problem, archipelago
from math import acos, cos, sin, ceil
import pandas as pd
import os
import sys

lib_path = os.path.abspath(os.path.join(__file__, '..', '..', 'AnalystTools'))
sys.path.append(lib_path)
import TargetFitness as TargetFitness # needs to 'AnalystTools'
import GridUtils as GridUtil
import PlannerTools as PlannerTools

environment = None
startPoint  = None
endPoint    = None

weights = { 'distance' : 1,
            'obstacle' : 100,
            'current'  : 0.00001,
            'entity'   : 0.001, # 0.01
          } 

def solveProblem (start, target, environment):
        
        # Configure problem using environment and target configuration
        dim  = environment['plannerGoto']['numWaypoints'] * 2
        prob = highestCurrentProblem(dim = dim)

        algo = algorithm.pso (gen = environment['plannerGoto']['generations'])
        isl = island(algo, prob, environment['plannerGoto']['individuals'])
        isl.evolve(1)

        # Try gen at a time
        log = []
        #algo = algorithm.pso(gen = 1)
        #isl = island(algo, prob, environment['plannerGoto']['individuals'])
        #for i in range(environment['plannerGoto']['generations']):
        #    isl.evolve(1)
        #    print('{},{}'.format(i,isl.population.champion.f[0]))
        #    log.append(isl.population.champion.f[0])


        #archi = archipelago(algo, prob, 5, 5 )# environment['plannerGoto']['individuals'])
        #archi.evolve(1)
        #isl = archi[0]
        #for i in archi:
        #    if i.population.champion.f < isl.population.champion.f:
        #        isl = i


        pathInfo = { "path"        : isl.population.champion.x, 
                     "constraints" : isl.population.champion.c,  
                     "fitness"     : isl.population.champion.f,
                     "start"       : (environment['vehicle']['startCoordsArchive']['row'], 
                                      environment['vehicle']['startCoordsArchive']['col']), 
                     "stop"        : (target['row'], target['col']),
                     "heading"     : None, 
                     "distance"    : None, 
                     "duration"    : None, 
                     "work"        : None, 
                     "coords"      : None,
                     "reward"      : None,
                     "optlog"      : log,
        }

        sp = PlannerTools.statPath (environment, isl.population.champion.x, 
                start, target)

        pathInfo["heading"]  = sp[0]
        pathInfo["distance"] = sp[1]
        pathInfo["duration"] = sp[2]
        pathInfo["work"]     = sp[3]
        pathInfo["coords"]   = sp[4]
        pathInfo["reward"]   = sp[5]

        pathPD = PlannerTools.path2pandas (pathInfo)

        return (pathInfo, pathPD)

class highestCurrentProblem(base):

        def __init__(self, dim = 10): # 10 is default dims -> 5 waypoints

                global environment
                global startPoint
                global endPoint

                # Set problem dimensions to be twice the number of waypoints,
                #   to account for the x and y coordinates of each
                dim = environment['plannerGoto']['numWaypoints'] * 2

                super (highestCurrentProblem, self).__init__(dim)
                
                self.yStart = startPoint['row']
                self.xStart = startPoint['col']
                self.yStop  = endPoint['row']
                self.xStop  = endPoint['col']

                # Get size of pixel (resolution in meters)
                self.pixelSize_m = PlannerTools.calcPixelResolution_m (environment['region']['grid'], 
                                                    environment['region']['raster'].GetGeoTransform())

                # Set problem bounds.
                upperBounds = [0] * dim
                for i in range (0, dim):
                        if (i % 2 == 0):
                                # Y value bounds
                                upperBounds[i] = environment['region']['extent']['rows'] - 1
                        else:
                                #X value bounds
                                upperBounds[i] = environment['region']['extent']['cols'] - 1

                self.set_bounds ([0] * dim, upperBounds)


        # The actual objective function, X is the N-dimension solution vector.
        def _objfun_impl (self, x):

                ###############
                # Non temporal#
                ###############

                heading = PlannerTools.pathHeading (x, self.yStart,
                        self.xStart,
                        self.yStop,
                        self.xStop)

                distance = PlannerTools.pathDistance (x,
                        self.yStart,
                        self.xStart,
                        self.yStop,
                        self.xStop,
                        environment["region"]["extent"]["rows"],
                        environment["region"]["extent"]["cols"],
                        environment['region']['grid'],
                        environment['plannerGoto']['obstacle_flag'])

                duration = PlannerTools.pathDuration (x,
                        distance,
                        environment["vehicle"]["speed"])

                reward = PlannerTools.pathReward (x,
                        self.yStart,
                        self.xStart,
                        self.yStop,
                        self.xStop,
                        environment["region"]["extent"]["rows"],
                        environment["region"]["extent"]["cols"],
                        environment["targets"]["grid"],
                        environment['logbook'])

                ###########
                # Temporal#
                ###########

                work = PlannerTools.pathEnergy (x,
                        self.yStart,
                        self.xStart,
                        self.yStop,
                        self.xStop,
                        environment["region"]["extent"]["rows"],
                        environment["region"]["extent"]["cols"],
                        environment["currents"]["magnitude"]["raster"],
                        environment["currents"]["direction"]["raster"],
                        self.pixelSize_m,
                        distance,
                        duration,
                        heading,
                        environment["vehicle"],
                        environment["timespan"]["interval"],
                        environment["timespan"]["offset"])

                global weights
                # Combine objectives into single fitness function
                f = distance["total"]            * weights["distance"]  \
                  + distance["penaltyWeighted"]  * weights["obstacle"]  \
                  + work["total"]                * weights["current"]   \
                  - reward['weightedTotal']      * weights["entity"]    
                
                return (f, )

def driver(sPoint, ePoint, env, offset = 0):
    
    startPointArchive  = GridUtil.getArchiveByWorld(sPoint["Lat"], 
                             sPoint["Lon"],
                             env["region"]["grid"],
                             env["region"]["raster"].GetGeoTransform())

    targetPointArchive = GridUtil.getArchiveByWorld(ePoint["Lat"], 
                             ePoint["Lon"],
                             env["region"]["grid"],
                             env["region"]["raster"].GetGeoTransform())

    global environment
    environment = env

    # Offset the start time
    environment["timespan"]["offset"] = offset

    global weights
    if float(environment["plannerGoto"]["distanceWeight"]) >= 0:
        weights["distance"] = float(environment["plannerGoto"]["distanceWeight"])
    if float(environment["plannerGoto"]["obstacleWeight"]) >= 0:
        weights["obstacle"] = float(environment["plannerGoto"]["obstacleWeight"])
    if float(environment["plannerGoto"]["currentWeight"]) >= 0:
        weights["current"] = float(environment["plannerGoto"]["currentWeight"])
    if float(environment["plannerGoto"]["entityWeight"]) >= 0:
        weights["entity"] = float(environment["plannerGoto"]["entityWeight"])

    # Hacky fix to a bad name
    environment["logbook"] = environment["targets"]["weights"]
    
    global startPoint
    startPoint = startPointArchive
    
    global endPoint
    endPoint = targetPointArchive

    (solutionPath, pathPandas) = solveProblem (startPointArchive, 
                                           targetPointArchive,
                                           environment)

    solution = { "solutionPath" : solutionPath,
                 "pathPandas"   : pathPandas,
                 "startPoint"   : sPoint,
                 "endPoint"     : ePoint,
    }

    return solution, pathPandas["DURATION"].sum()

