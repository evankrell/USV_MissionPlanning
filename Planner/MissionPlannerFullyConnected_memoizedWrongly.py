'''
MissionPlannerFullyConnected.py
Author: Evan Krell

This mission planner is an alternative to
the 'MissionPlanner.py'. Every possible goto
path is evaluated to choose the best sequence.
'''

import GridUtils as GridUtil
import RasterSetInterface as rsi
import time
from math import acos, cos, sin, ceil
import pandas as pd
import yaml as yaml
import numpy as np
import osr
import paramiko as paramiko
from scp import SCPClient
from datetime import date, datetime, timedelta
import subprocess
import os
import sys
import dill as pickle
import itertools
from ortools.constraint_solver import pywrapcp
from ortools.constraint_solver import routing_enums_pb2

lib_path = os.path.abspath(os.path.join(__file__, '..', '..', 'AnalystTools'))
sys.path.append(lib_path)
import TargetFitness as TargetFitness # needs to 'AnalystTools'
import PlannerTools  as PlannerTools
import FindPivots    as FindPivots

# Import path planning modules
import PathPlannerTargetSelect as PPT
import PathPlannerGoto         as PPG
import PathPlannerCoverage     as PPC


def getCoverageResult(inNode, targetNode, outNode, extendedCoverageResults, region2point, targetsTable, environment, duration):
    

    # ! Commenting out the memoization, becuase
    # it is bad due to dependency on duration this far.
    # (memoization could be used, but needs to be indexed by
    # entire history
    ##### Function based on memoization. 
    ##### If the coverage result exists: use it, 
    ##### otherwise: compute it
    #####if extendedCoverageResults[inNode][outNode][targetNode - 1] is None:

    # Start point
    startCoords = (region2point[targetNode][inNode]["row"], 
                   region2point[targetNode][inNode]["col"])
    # End point
    stopCoords  = (region2point[targetNode][outNode]["row"], 
                   region2point[targetNode][outNode]["col"])
    # Target info
    trow = targetsTable.loc[[targetNode]]
    
    cPoints, cPath = PPC.driver(float(targetsTable.loc[[targetNode]]["Lat"]),
        float(targetsTable.loc[[targetNode]]["Lon"]),
        float(targetsTable.loc[[targetNode]]["Width"]),
        float(targetsTable.loc[[targetNode]]["Length"]),
        int(targetsTable.loc[[targetNode]]["Ypoints"]),
        int(targetsTable.loc[[targetNode]]["Xpoints"]),
        int(targetsTable.loc[[targetNode]]["Margin"]),
        int(targetsTable.loc[[targetNode]]["Roam"]),
        environment, 
        duration,
        startCoords, 
        stopCoords)

    # Convert path such that points are relative to world grid
    gridPath = PPC.convertTargetPath2WorldPath(cPoints, cPath)
    gridPathLong, gStart, gTarget = PlannerTools.getPathLongForm(gridPath)

    # Get path stats
    cHeading, cDistance, cDuration, cWork, cCoord, cReward = \
        PlannerTools.statPath(environment, gridPathLong, gStart, gTarget)

    cStartWorld  = GridUtil.grid2world(gStart["row"], gStart["col"],
                   environment["region"]["raster"].GetGeoTransform(),
                   environment["region"]["extent"]["cols"])
    cTargetWorld = GridUtil.grid2world(gTarget["row"], gTarget["col"],
                   environment["region"]["raster"].GetGeoTransform(),
                   environment["region"]["extent"]["cols"])
    cStartWorld  = { "Lat" : cStartWorld[0],  "Lon" : cStartWorld[1] }
    cTargetWorld = { "Lat" : cTargetWorld[0], "Lon" : cTargetWorld[1] }
    cPathInfo = { "path"       : gridPathLong,
                  "start"      : (gStart["row"], gStart["col"]),
                  "stop"       : (gTarget["row"], gTarget["col"]),
                  "startWorld" : cStartWorld,
                  "endWorld"   : cTargetWorld,
                  "heading"    : cHeading,
                  "distance"   : cDistance,
                  "duration"   : cDuration,
                  "work"       : cWork,
                  "coords"     : cCoord,
                  "reward"     : cReward,
                }
    cPathPandas = PlannerTools.path2pandas(cPathInfo)

    # Memoize result
    extendedCoverageResults[inNode][outNode][targetNode - 1] = \
                           { "points"       : cPoints,
                             "pathPandas"   : cPathPandas,
                             "path"         : cPath,
                             "gridPath"     : gridPath,
                             #"computeTime"  : computeTime,
                             "startGrid"    : gStart,
                             "endGrid"      : gTarget,
                             "startWorld"   : cPathInfo["startWorld"],
                             "endWorld"     : cPathInfo["endWorld"],
    }
   
    return extendedCoverageResults[inNode][outNode][targetNode - 1]


def parseOptions():
    from optparse import OptionParser

    # Define options
    parser = OptionParser()
    parser.add_option("-c", "--config",         dest="config",         metavar="CONFIG",
        help="Path to configuration file.")
    parser.add_option("-d", "--pickle",         dest="pickle",         metavar="PICKLE",
        help="Path to pickle file (will create if not present).")
    parser.add_option("-r", "--repeat",         dest="repeat",         metavar="REPEAT",
        action="store_true",  default=False,
        help="Use existing pickle file to repeat final route selection")
    parser.add_option("-p", "--password",       dest="password",       metavar="PASS",
        help="Password for SCP to transfer report to server.")
    parser.add_option("-b", "--build_currents", dest="build_currents", metavar="BUILD_CURRENTS",
        action="store_false", default=True,
        help="Builds currents raster set. Otherwise, use existing rasters.")
    parser.add_option("-s", "--show_results",   dest="show_results",   metavar="SHOW_RESULTS",
        action="store_true",  default=False,
        help="Show contents of pickle file and exit.")

    # Get options
    (options, args) = parser.parse_args()

    # Verify correct options
    if options.config is None:
        (options, args) = None, None

    return options, args

def main():

    ##################
    # Initialization #
    ##################

    # Constraints <------ Should be options!!!!!
    durationMax = 12
    workMax     = 62523000

    # All important data should be linked here
    # in order to eventually save all results to json
    json_data = { "input"                 : dict(),
                  "plannerTargetSelect"   : dict(),
                  "plannerCoverage"       : dict(),
                  "plannerGoto"           : dict(),
                  "plannerSequence"       : None,
                  "plannerSequencePandas" : None,
                  "plannerSummaryPandas"  : None,
                  "timeLog"               : None,
                  "routesFeasible"        : None,
                  "nodeSequence"          : None,
                  "routesInfeasible"      : None,
    }

    # Parse options
    (options, args) = parseOptions()
    if options is None:
        exit()

    # If requested, show data and exit
    if options.show_results == True:
        json_data = pickle.load(open(options.pickle, 'rb'))
        for r in json_data["routesFeasible"]:
            print (r)
        exit()

    # Load data
    environment     = PlannerTools.initializeByFile(options.config, haveCurrents = options.build_currents)
    targetsTable    = PlannerTools.initTargetsTableByFile(environment["targets"]["table"])
    safepointsTable = PlannerTools.initSafepointsTableByFile(environment["vehicle"]["safepointsFile"])

    # Hacky rename since I don't like the original name, but need to maintain compat.
    environment["logbook"] = environment["targets"]["weights"]

    startPoint = { "Lat" : environment["vehicle"]["startCoords"][1],
                   "Lon" : environment["vehicle"]["startCoords"][0],
    }
    startArchive = GridUtil.getArchiveByWorld(startPoint["Lat"], startPoint["Lon"],
        environment["region"]["grid"], environment["region"]["raster"].GetGeoTransform())

    # Add to json
    json_data["input"]["targetsTable"]    = targetsTable
    json_data["input"]["safepointsTable"] = safepointsTable

    # Build region2point matrix
    region2point = PlannerTools.region2pointBuild(startPoint, targetsTable, safepointsTable, environment)

               # num targets           # num safepoints            # num start
    numNodes = targetsTable.shape[0]   + safepointsTable.shape[0]  + 1

    # row : select IN node, col : select OUT node, list : select coverage target
    extendedCoverageResults = [[[None for t in range(targetsTable.shape[0])] \
                         for c in range(numNodes)] for r in range(numNodes)]

    # Load existing json, if using repeat
    if options.repeat == True:
        json_data = pickle.load(open(options.pickle, 'rb'))
        extendedCoverageResults = json_data["plannerCoverage"]["extendedCoverageResults"]
        coverageResults         = json_data["plannerCoverage"]["coverageResults"]
        startGotos              = json_data["plannerGoto"]["startResults"]
        interiorGotos           = json_data["plannerGoto"]["interiorResults"]
        stopGotos               = json_data["plannerGoto"]["stopResults"]

    else:

        #############################
        # Path Planner Goto - Start #
        #############################

        

        startGotos = []
        for TID in range(1, targetsTable.shape[0] + 1):
            trow = targetsTable.loc[[TID]]
            tpi = GridUtil.grid2world(region2point[TID][0]["row"], region2point[TID][0]["col"],
                          environment["region"]["raster"].GetGeoTransform(),
                          environment["region"]["extent"]["cols"])
            targetPoint = { "Lat" : tpi[0],
                            "Lon" : tpi[1],
            }

            startGoto = PPG.driver(startPoint, targetPoint, environment, duration)
            startGotos.append(startGoto)
        json_data["plannerGoto"]["startResults"] = startGotos

        ################################
        # Path Planner Goto - Interior #
        ################################
        interiorGotos = [[dict() for j in range(targetsTable.shape[0])] for i in range(targetsTable.shape[0])]
        for i in range(0, targetsTable.shape[0]):
            for j in range(0, targetsTable.shape[0]):

                tpi = GridUtil.grid2world(region2point[i + 1][j + 1]["row"], region2point[i + 1][j + 1]["col"],
                              environment["region"]["raster"].GetGeoTransform(),
                              environment["region"]["extent"]["cols"])
                targetPointI = { "Lat" : tpi[0],
                                 "Lon" : tpi[1],
                }

                tpj = GridUtil.grid2world(region2point[j + 1][i + 1]["row"], region2point[i + 1][j + 1]["col"],
                              environment["region"]["raster"].GetGeoTransform(),
                              environment["region"]["extent"]["cols"])
                targetPointJ = { "Lat" : tpj[0],
                                 "Lon" : tpj[1],
                }

                interiorGotoIJ      = PPG.driver(targetPointI, targetPointJ, environment, duration)
                interiorGotos[i][j] = interiorGotoIJ
        json_data["plannerGoto"]["interiorResults"] = interiorGotos

        ############################
        # Path Planner Goto - Stop #
        ############################
        stopGotos = [[dict() for j in range(safepointsTable.shape[0])] for i in range(targetsTable.shape[0])]
        for i in range(0, targetsTable.shape[0]):
            for j in range(0, safepointsTable.shape[0]):

                jAsNodeIdx = targetsTable.shape[0] + j

                tpi = GridUtil.grid2world(region2point[i + 1][jAsNodeIdx]["row"], 
                              region2point[i + 1][jAsNodeIdx]["col"],
                              environment["region"]["raster"].GetGeoTransform(),
                              environment["region"]["extent"]["cols"])
                targetPointI = { "Lat" : tpi[0],
                                 "Lon" : tpi[1],
                }
                stopPointJ   = { "Lat" : safepointsTable.loc[[j + 1]]["Lat"],
                                 "Lon" : safepointsTable.loc[[j + 1]]["Lon"], }
                stopGotoIJ      = PPG.driver(targetPointI, stopPointJ, environment, duration)
                stopGotos[i][j] = stopGotoIJ
        json_data["plannerGoto"]["stopResults"] = stopGotos

    #############################################
    # Path Planner - Sequence Select & Coverage #
    #############################################
    
    def getDistMatrix(startGotos, interiorGotos, coverageResults):
 
        numStarts  = 1                   # Always have one start
        numTargets = len(interiorGotos)  # Number of gotos 
        numStops   = len(stopGotos[0])   # Multiple possible destinations
        numNodes   = numStarts + numTargets + numStops

        starts  = [0]
        targets = range(numStarts, numTargets + 1)
        stops   = range(numTargets + 1, numNodes) 

        numCells = numNodes * numNodes
        
        # Init empty matrix
        fitnessMat  = np.array([[-1 for x in range(numNodes)] for y in range(numNodes)])
        distanceMat = np.array([[-1 for x in range(numNodes)] for y in range(numNodes)])
        durationMat = np.array([[-1 for x in range(numNodes)] for y in range(numNodes)])
        workMat     = np.array([[-1 for x in range(numNodes)] for y in range(numNodes)])
        rewardMat   = np.array([[-1 for x in range(numNodes)] for y in range(numNodes)])

        # Fill values for 'start -> target' gotos
        fitnessMat[0][0]  = 0
        distanceMat[0][0] = 0
        durationMat[0][0] = 0
        workMat[0][0]     = 0
        rewardMat[0][0]   = 0
        for i in range(numTargets):
            nodeIDX = targets[i]
            fitnessMat[0][nodeIDX]  = \
                startGotos[i]["solutionPath"]["fitness"][0] 
            distanceMat[0][nodeIDX] = \
                startGotos[i]["solutionPath"]["distance"]["total"] 
            durationMat[0][nodeIDX] = \
                startGotos[i]["solutionPath"]["duration"]["total"] 
            workMat[0][nodeIDX]     = \
                startGotos[i]["solutionPath"]["work"]["total"]     
            rewardMat[0][nodeIDX]   = \
                startGotos[i]["solutionPath"]["reward"]["total"]   


        # Fill values for 'target <-> target' gotos
        fitnessMat[0][0]  = 0
        distanceMat[0][0] = 0
        durationMat[0][0] = 0
        workMat[0][0]     = 0
        rewardMat[0][0]   = 0
        for i in range(numTargets):
            nodeIDXi = targets[i]
            for j in range(numTargets):
                nodeIDXj = targets[j]
                if i == j :
                    fitnessMat[nodeIDXi][nodeIDXj]  = 0
                    workMat[nodeIDXi][nodeIDXj]     = 0
                    durationMat[nodeIDXi][nodeIDXj] = 0
                else:
                    fitnessMat[nodeIDXi][nodeIDXj] = \
                        interiorGotos[i][j]["solutionPath"]["fitness"][0]
                    distanceMat[nodeIDXi][nodeIDXj] = \
                        interiorGotos[i][j]["solutionPath"]["distance"]["total"] 
                    durationMat[nodeIDXi][nodeIDXj] = \
                        interiorGotos[i][j]["solutionPath"]["duration"]["total"] 
                    workMat[nodeIDXi][nodeIDXj] = \
                        interiorGotos[i][j]["solutionPath"]["work"]["total"]     
                    rewardMat[nodeIDXi][nodeIDXj] = \
                        interiorGotos[i][j]["solutionPath"]["reward"]["total"]   

        # Fill values for 'target -> stop' gotos
        for t in range(numTargets):
            nodeIDXt = targets[t]
            for s in range(numStops):
                nodeIDXs = stops[s]
                fitnessMat[nodeIDXt][nodeIDXs]  = \
                    stopGotos[t][s]["solutionPath"]["fitness"][0]
                distanceMat[nodeIDXt][nodeIDXs]  = \
                    stopGotos[t][s]["solutionPath"]["distance"]["total"]
                durationMat[nodeIDXt][nodeIDXs] = \
                    stopGotos[t][s]["solutionPath"]["duration"]["total"]
                workMat[nodeIDXt][nodeIDXs]     = \
                    stopGotos[t][s]["solutionPath"]["work"]["total"]
                rewardMat[nodeIDXt][nodeIDXs]   = \
                    stopGotos[t][s]["solutionPath"]["reward"]["total"]

        return fitnessMat, distanceMat, durationMat, workMat, rewardMat

    numStarts  = 1                   # Always have one start
    numTargets = len(interiorGotos)  # Number of gotos 
    numStops   = len(stopGotos[0])   # Multiple possible destinations
    numNodes   = numStarts + numTargets + numStops

    starts  = [0]
    targets = range(numStarts, numTargets + 1)
    stops   = range(numTargets + 1, numNodes) 

    # Build distance matrix
    fitnessMatrix, distanceMatrix, durationMatrix, \
        workMatrix, rewardMatrix = \
        getDistMatrix(startGotos, interiorGotos, coverageResults)

    # Generate all possible routes
    routeTemplate = { "sequence" : [], "fitness"  : 0, "distance" : 0,
                      "work"     : 0,  "duration" : 0, "reward"   : 0,
                      "feasible" : False  }

    routes           = []
    routesFeasible   = []
    routesInfeasible = []

    # Generate all possible route sequences
    permTargets = []
    for r in range(numTargets):
        permTargets.extend(itertools.permutations(targets, r + 1))
    for permTarget in permTargets:
        sequencePre = [0] + [p for p in permTarget] 
        for s in stops:
            sequence = sequencePre + [s]
            route = routeTemplate.copy()
            route["sequence"] = sequence
            routes.append(route)

    # Evaluate all possible routes
    for route in routes:
        sAsNodeIdx  = route["sequence"][-1]
        stopIdx     = s - targetsTable.shape[0] 
        stopArchive = GridUtil.getArchiveByWorld(safepointsTable.loc[stopIdx]["Lat"],
                  safepointsTable.loc[stopIdx]["Lon"], environment["region"]["grid"], 
                                   environment["region"]["raster"].GetGeoTransform())
        nodeA = route["sequence"][0]
        
        # Get GOTO costs & rewards
        for nodeB in route["sequence"][1:]:

            route["fitness"]  = route["fitness"]  + fitnessMatrix[nodeA][nodeB]
            route["distance"] = route["distance"] + distanceMatrix[nodeA][nodeB]
            route["duration"] = route["duration"] + durationMatrix[nodeA][nodeB]
            route["work"]     = route["work"]     + workMatrix[nodeA][nodeB]
            route["reward"]   = route["reward"]   + rewardMatrix[nodeA][nodeB]
            nodeA = nodeB

        # get COVERAGE costs and rewards
        seqcoords = PlannerTools.sequence2coords(route["sequence"], startArchive, stopArchive, region2point)

        for covIdx in range(1, len(seqcoords), 2):
            inIdx  = covIdx - 1
            outIdx = covIdx + 1
            inNode  = seqcoords[ inIdx]["startNode"]
            outNode = seqcoords[outIdx][ "stopNode"]
            covNode = seqcoords[covIdx]["startNode"]
            covRes = getCoverageResult(inNode, covNode, outNode, 
                     extendedCoverageResults, region2point, targetsTable, environment)
            route["distance"] = route["distance"] + covRes["pathPandas"]["DISTANCE"].sum()
            route["duration"] = route["duration"] + covRes["pathPandas"]["DURATION"].sum()
            route["work"]     = route["work"]     + covRes["pathPandas"]["WORK"].sum()
            route["reward"]   = route["reward"]   + covRes["pathPandas"]["REWARD"].sum()

        if route["duration"] <= durationMax and route["work"] <= workMax:
            route["feasible"] = True
            routesFeasible.append(route)   # Shallow copy!
        else:
            route["feasible"] = False
            routesInfeasible.append(route) # Shallow copy!

    # Sort routes
    routesFeasible   = list(sorted(routesFeasible,            key = lambda k: k["duration"]))
    routesFeasible   = list(sorted(routesFeasible,            key = lambda k: k["work"]))
    routesFeasible   = list(reversed(sorted(routesFeasible,   key = lambda k: k["reward"])))
    routesInfeasible = list(sorted(routesInfeasible,          key = lambda k: k["duration"]))
    routesInfeasible = list(sorted(routesInfeasible,          key = lambda k: k["work"]))
    routesInfeasible = list(reversed(sorted(routesInfeasible, key = lambda k: k["reward"])))

    json_data["routesFeasible"]   = routesFeasible
    json_data["routesInfeasible"] = routesInfeasible

    # Save coverage results, both full and as a sequence for top feasible route
    seq = routesFeasible[0]["sequence"]
    stopAsNodeIdx = seq[-1] - targetsTable.shape[0]
    stopArchive = GridUtil.getArchiveByWorld(safepointsTable.loc[stopAsNodeIdx]["Lat"],
                  safepointsTable.loc[stopAsNodeIdx]["Lon"], environment["region"]["grid"], 
                                   environment["region"]["raster"].GetGeoTransform())
    seqcoords = PlannerTools.sequence2coords(routesFeasible[0]["sequence"], startArchive, stopArchive, region2point)
    coverageResults = []
    for covIdx in range(1, len(seqcoords), 2):
            inIdx  = covIdx - 1
            outIdx = covIdx + 1
            inNode  = seqcoords[ inIdx]["startNode"]
            outNode = seqcoords[outIdx][ "stopNode"]
            covNode = seqcoords[covIdx]["startNode"]
            covRes = getCoverageResult(inNode, covNode, outNode, 
                     extendedCoverageResults, region2point, targetsTable, environment)
            coverageResults.append(covRes.copy())

    json_data["plannerCoverage"]["coverageResults"]         = coverageResults
    json_data["plannerCoverage"]["extendedCoverageResults"] = extendedCoverageResults
    
    ################
    # Save results #
    ################

    # Save JSON 
    with open(options.pickle, 'wb') as outfile:
        pickle.dump(json_data, outfile, protocol = pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":
    main()

