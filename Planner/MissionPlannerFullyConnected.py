'''
MissionPlannerFullyConnected.py
Author: Evan Krell

This mission planner is an alternative to
the 'MissionPlanner.py'. Every possible goto
path is evaluated to choose the best sequence.
'''


import GridUtils as GridUtil
import RasterSetInterface as rsi
import time
from math import acos, cos, sin, ceil
import pandas as pd
import yaml as yaml
import numpy as np
import osr
import paramiko as paramiko
from scp import SCPClient
from datetime import date, datetime, timedelta
import subprocess
import os
import sys
import dill as pickle
import itertools
from ortools.constraint_solver import pywrapcp
from ortools.constraint_solver import routing_enums_pb2

lib_path = os.path.abspath(os.path.join(__file__, '..', '..', 'AnalystTools'))
sys.path.append(lib_path)
import TargetFitness as TargetFitness # needs to 'AnalystTools'
import PlannerTools  as PlannerTools
import FindPivots    as FindPivots

# Import path planning modules
import PathPlannerTargetSelect as PPT
import PathPlannerGoto         as PPG
import PathPlannerCoverage     as PPC

def parseOptions():
    from optparse import OptionParser

    # Define options
    parser = OptionParser()
    parser.add_option("-c", "--config",         dest="config",         metavar="CONFIG",
        help="Path to configuration file.")
    parser.add_option("-d", "--pickle",         dest="pickle",         metavar="PICKLE",
        help="Path to pickle file (will create if not present).")
    parser.add_option("-r", "--repeat",         dest="repeat",         metavar="REPEAT",
        action="store_true",  default=False,
        help="Use existing pickle file to repeat final route selection")
    parser.add_option("-p", "--password",       dest="password",       metavar="PASS",
        help="Password for SCP to transfer report to server.")
    parser.add_option("-b", "--build_currents", dest="build_currents", metavar="BUILD_CURRENTS",
        action="store_false", default=True,
        help="Builds currents raster set. Otherwise, use existing rasters.")
    parser.add_option("-s", "--show_results",   dest="show_results",   metavar="SHOW_RESULTS",
        action="store_true",  default=False,
        help="Show contents of pickle file and exit.")

    # Get options
    (options, args) = parser.parse_args()

    if options.config is None:
        (options, args) = None, None

    return options, args

def planSequence(selectedTargets, stopIdx, seqcoords, targetsTable, safepointsTable, environment, startPoint, timeLog,
    numSkip = 0, coverageSaved = None, gotoSaved = None, duration = 0):

    print("")
    print("Start Path Evaluation")
    print(selectedTargets)

    ##################################
    # Path planner Goto and Coverage #
    ##################################
    coverageResults = []
    gotoResults     = []
    nodeSequence    = []
    duration        = 0
    coverageSkipIDX = 0
    gotoSkipIDX     = 0

    numTargets = len(selectedTargets)

    tpf = GridUtil.grid2world(seqcoords[0]["stopRow"], seqcoords[0]["stopCol"],
                       environment["region"]["raster"].GetGeoTransform(), 
                       environment["region"]["extent"]["cols"])
    targetPointF  = { "Lat" : tpf[0], "Lon" : tpf[1]}

    tpe = GridUtil.grid2world(seqcoords[-1]["startRow"], seqcoords[-1]["startCol"],
                       environment["region"]["raster"].GetGeoTransform(), 
                       environment["region"]["extent"]["cols"])
    targetPointE  = { "Lat" : tpe[0], "Lon" : tpe[1]}

    idx = stopIdx # = int(selectedTargets["solution"].x[len(selectedTargets["solution"].x) - 1]) + 1
 
    endPoint      = { "Lat" : safepointsTable.loc[[idx]]["Lat"],
                      "Lon" : safepointsTable.loc[[idx]]["Lon"]}

    #####################
    # Path Planner Goto #
    #####################
    print("Duration", duration) 
    print("Goto", startPoint, seqcoords[0])

    # Plan path from start to first target
    if numSkip > 0:
        gotoResults.append(gotoSaves[gotoSkipIDX].copy())
        elapsed = gotoResults[-1]["pathPandas"]["DURATION"].sum()
        gotoSkipIDX = gotoSkipIDX + 1
        numSkip = numSkip - 1
    else:
        start = time.time()
        segmentF, elapsed = PPG.driver(startPoint, targetPointF, environment, duration)
        computeTime = time.time() - start
        timeLog.loc[len(timeLog)] = ["Goto: (S," + str(selectedTargets[0]) + ")", 3, computeTime]
        gotoResults.append(segmentF)

    nodeSequence.append( { "from" : 0, "to" : selectedTargets[0] } )

    # Update duration
    duration = duration + elapsed
    
    # Plan Segments
    gotoIdx = 0
    for x in range(len(selectedTargets) - 1):

        #########################
        # Path Planner Coverage #
        #########################
        TID = selectedTargets[x]
        print("Duration", duration)
        print("Coverage", TID)

        trow = targetsTable.loc[[TID]]

        # Index of coverage task in seqcoords
        covidx = 1
        for sc in seqcoords[1::2]:
            if sc["startNode"] == TID:
                break
            covidx = covidx + 2

        if numSkip > 0:
            coverageResults.append(coverageSaves[coverageSkipIDX].copy())
            elapsed = coverageResults[-1]["pathPandas"]["DURATION"].sum()
            coverageSkipIDX = coverageSkipIDX + 1
            numSkip = numSkip - 1
        else:        
            start = time.time()
            cPoints, cPath = PPC.driver(float(targetsTable.loc[[TID]]["Lat"]), 
                float(targetsTable.loc[[TID]]["Lon"]), 
                float(targetsTable.loc[[TID]]["Width"]),
                float(targetsTable.loc[[TID]]["Length"]), 
                int(targetsTable.loc[[TID]]["Ypoints"]), 
                int(targetsTable.loc[[TID]]["Xpoints"]), 
                int(targetsTable.loc[[TID]]["Margin"]), 
                int(targetsTable.loc[[TID]]["Roam"]),
                environment, 
                duration,
                (seqcoords[covidx]["startRow"], seqcoords[covidx]["startCol"]),
                (seqcoords[covidx]["stopRow"],  seqcoords[covidx]["stopCol"]))

            duration = duration + cPath["duration"]
             
            computeTime = time.time() - start
            timeLog.loc[len(timeLog)] = ["coverage: " + str(TID), 2, computeTime]
            # Convert path such that points are relative to world grid
            gridPath = PPC.convertTargetPath2WorldPath(cPoints, cPath)
            gridPathLong, gStart, gTarget = PlannerTools.getPathLongForm(gridPath)

            # Get path stats
            cHeading, cDistance, cDuration, cWork, cCoord, cReward = \
                PlannerTools.statPath(environment, gridPathLong, gStart, gTarget)

            cStartWorld  = GridUtil.grid2world(gStart["row"], gStart["col"], 
                           environment["region"]["raster"].GetGeoTransform(), 
                           environment["region"]["extent"]["cols"])
            cTargetWorld = GridUtil.grid2world(gTarget["row"], gTarget["col"], 
                           environment["region"]["raster"].GetGeoTransform(),
                           environment["region"]["extent"]["cols"])
            cStartWorld  = { "Lat" : cStartWorld[0],  "Lon" : cStartWorld[1] }
            cTargetWorld = { "Lat" : cTargetWorld[0], "Lon" : cTargetWorld[1] }
            cPathInfo = { "path"       : gridPathLong,
                          "start"      : (gStart["row"], gStart["col"]),
                          "stop"       : (gTarget["row"], gTarget["col"]),
                          "startWorld" : cStartWorld,
                          "endWorld"   : cTargetWorld,
                          "heading"    : cHeading, 
                          "distance"   : cDistance,
                          "duration"   : cDuration,
                          "work"       : cWork,
                          "coords"     : cCoord,
                          "reward"     : cReward,
                        }
            cPathPandas = PlannerTools.path2pandas(cPathInfo)
            coverageResults.append({ "points"       : cPoints,
                                     "pathPandas"   : cPathPandas,
                                     "path"         : cPath,
                                     "gridPath"     : gridPath,
                                     "computeTime"  : computeTime,
                                     "startGrid"    : gStart,
                                     "endGrid"      : gTarget,
                                     "startWorld"   : cPathInfo["startWorld"],
                                     "endWorld"     : cPathInfo["endWorld"],
            })
        
            nodeSequence.append( { "from" : TID, "to" : TID } )

        #####################
        # Path Planner Goto #
        #####################
        TID_i = selectedTargets[x]

        try:
            TID_j = selectedTargets[x + 1]
        except:
            TID_j = TID_i
            break

        print("Duration", duration)
        print("Goto", TID_i, TID_j)

        tpi = GridUtil.grid2world(seqcoords[gotoIdx]["startRow"], seqcoords[gotoIdx]["startCol"],
                          environment["region"]["raster"].GetGeoTransform(), 
                          environment["region"]["extent"]["cols"])
        targetPointI  = { "Lat" : tpi[0], "Lon" : tpi[1]}
        
        tpj = GridUtil.grid2world(seqcoords[gotoIdx]["stopRow"], seqcoords[gotoIdx]["stopCol"],
                          environment["region"]["raster"].GetGeoTransform(), 
                          environment["region"]["extent"]["cols"])
        targetPointJ  = { "Lat" : tpj[0], "Lon" : tpj[1]}

        if numSkip > 0:
            gotoResults.append(gotoSaves[gotoSkipIDX].copy())
            elapsed = gotoResults[-1]["pathPandas"]["DURATION"].sum()
            gotoSkipIDX = gotoSkipIDX + 1
            numSkip = numSkip - 1
        else:
            start = time.time()
            segmentIJ, elapsed = PPG.driver(targetPointI, targetPointJ, environment, duration)
            computeTime = time.time() - start
            timeLog.loc[len(timeLog)] = ["Goto: (" + str(selectedTargets[x]) + 
                                         "," + str(selectedTargets[x + 1]) + ")", 3, computeTime]
            gotoResults.append(segmentIJ)

            nodeSequence.append( { "from" : TID_i, "to" : TID_j } )

        gotoIdx = gotoIdx + 2

        # Update duration
        duration = duration + elapsed

    # Deal with last segment

    #########################
    # Path Planner Coverage #
    #########################
    try:
        TID = TID_j
    except:
        TID = selectedTargets[0]

    print("Duration", duration)
    print("Coverage", TID)

    trow = targetsTable.loc[[TID]]

    # Index of coverage task in seqcoords
    covidx = 1
    for sc in seqcoords[1::2]:
        if sc["startNode"] == TID:
            break
        covidx = covidx + 2

    if numSkip > 0:
        coverageResults.append(coverageSaves[coverageSkipIDX].copy())
        elapsed = coverageResults[-1]["pathPandas"]["DURATION"].sum()
        coverageSkipIDX = coverageSkipIDX + 1
        numSkip = numSkip - 1
    else:           
        start = time.time()
        cPoints, cPath = PPC.driver(float(targetsTable.loc[[TID]]["Lat"]), 
            float(targetsTable.loc[[TID]]["Lon"]), 
            float(targetsTable.loc[[TID]]["Width"]),
            float(targetsTable.loc[[TID]]["Length"]), 
            int(targetsTable.loc[[TID]]["Ypoints"]), 
            int(targetsTable.loc[[TID]]["Xpoints"]), 
            int(targetsTable.loc[[TID]]["Margin"]), 
            int(targetsTable.loc[[TID]]["Roam"]),
            environment, 
            duration,
            (seqcoords[covidx]["startRow"], seqcoords[covidx]["startCol"]),
            (seqcoords[covidx]["stopRow"],  seqcoords[covidx]["stopCol"]))

        duration = duration + cPath["duration"]
         
        computeTime = time.time() - start
        timeLog.loc[len(timeLog)] = ["coverage: " + str(TID), 2, computeTime]
        # Convert path such that points are relative to world grid
        gridPath = PPC.convertTargetPath2WorldPath(cPoints, cPath)
        gridPathLong, gStart, gTarget = PlannerTools.getPathLongForm(gridPath)

        # Get path stats
        cHeading, cDistance, cDuration, cWork, cCoord, cReward = \
            PlannerTools.statPath(environment, gridPathLong, gStart, gTarget)

        cStartWorld  = GridUtil.grid2world(gStart["row"], gStart["col"], 
                       environment["region"]["raster"].GetGeoTransform(), 
                       environment["region"]["extent"]["cols"])
        cTargetWorld = GridUtil.grid2world(gTarget["row"], gTarget["col"], 
                       environment["region"]["raster"].GetGeoTransform(),
                       environment["region"]["extent"]["cols"])
        cStartWorld  = { "Lat" : cStartWorld[0],  "Lon" : cStartWorld[1] }
        cTargetWorld = { "Lat" : cTargetWorld[0], "Lon" : cTargetWorld[1] }
        cPathInfo = { "path"       : gridPathLong,
                      "start"      : (gStart["row"], gStart["col"]),
                      "stop"       : (gTarget["row"], gTarget["col"]),
                      "startWorld" : cStartWorld,
                      "endWorld"   : cTargetWorld,
                      "heading"    : cHeading, 
                      "distance"   : cDistance,
                      "duration"   : cDuration,
                      "work"       : cWork,
                      "coords"     : cCoord,
                      "reward"     : cReward,
                    }
        cPathPandas = PlannerTools.path2pandas(cPathInfo)
        coverageResults.append({ "points"       : cPoints,
                                 "pathPandas"   : cPathPandas,
                                 "path"         : cPath,
                                 "gridPath"     : gridPath,
                                 "computeTime"  : computeTime,
                                 "startGrid"    : gStart,
                                 "endGrid"      : gTarget,
                                 "startWorld"   : cPathInfo["startWorld"],
                                 "endWorld"     : cPathInfo["endWorld"],
        })
       
        nodeSequence.append( { "from" : TID, "to" : TID } )

    #####################
    # Path Planner Goto #
    #####################
    # Plan path from final target to end point
    print ("Duration", duration)
    print ("Goto", seqcoords[-1], endPoint)

    if numSkip > 0:
        gotoResults.append(gotoSaves[gotoSkipIDX].copy())
        elapsed = gotoResults[-1]["pathPandas"]["DURATION"].sum()
        gotoSkipIDX = gotoSkipIDX + 1
        numSkip = numSkip - 1
    else:
        start = time.time()
        segmentE, elapsed = PPG.driver(targetPointE, endPoint, environment, duration)
        computeTime = time.time() - start
        timeLog.loc[len(timeLog)] = ["Goto: (" + str(selectedTargets[numTargets - 1]) + ",E)", 3, computeTime]
        gotoResults.append(segmentE)
        
        nodeSequence.append( { "from" : TID, "to" : stopIdx } )

    # Update duration
    duration = duration + elapsed
    print("Duration", duration)
    print ("End Path Evaluation")

    return gotoResults, coverageResults, duration, nodeSequence


def getPathInfo(gotoResults, coverageResults):

    # Build sequence log
    resultSequenceItemTemplate = \
        { "seqnum"     : 0, 
          "type"       : "", 
          "seqnumFrom" : None,
          "seqnumTo"   : None,
          "pathPandas" : None,
          "distance"   : 0, 
          "duration"   : 0, 
          "work"       : 0, 
          "reward"     : 0,
          "result"     : None,
    }
    
    resultSequenceLog = []

    # Add first Goto
    resSeqItem = resultSequenceItemTemplate.copy()
    resSeqItem["seqnum"]     = 0
    resSeqItem["type"]       = "goto" 
    resSeqItem["start"]      = gotoResults[0]["startPoint"]
    resSeqItem["end"]        = gotoResults[0]["endPoint"]
    resSeqItem["pathPandas"] = gotoResults[0]["pathPandas"]
    resSeqItem["result"]     = gotoResults[0]
    resultSequenceLog.append(resSeqItem)

    seqnum = 1
    for i in range(len(coverageResults)):
        ci = i     # Index of coverage res
        gi = i + 1 # Index of goto res

        # Add coverage item
        resSeqItem               = resultSequenceItemTemplate.copy()
        resSeqItem["seqnum"]     = seqnum
        resSeqItem["type"]       = "coverage"
        resSeqItem["start"]      = coverageResults[ci]["startWorld"]
        resSeqItem["end"]        = coverageResults[ci]["endWorld"]
        resSeqItem["pathPandas"] = coverageResults[ci]["pathPandas"]
        resSeqItem["result"]     = coverageResults[ci]
        resultSequenceLog.append(resSeqItem)
        
        seqnum = seqnum + 1

        # Add goto item
        resSeqItem               = resultSequenceItemTemplate.copy()
        resSeqItem["seqnum"]     = seqnum
        resSeqItem["type"]       = "goto" 
        resSeqItem["start"]      = gotoResults[gi]["startPoint"]
        resSeqItem["end"]        = gotoResults[gi]["endPoint"]
        resSeqItem["pathPandas"] = gotoResults[gi]["pathPandas"]
        resSeqItem["result"]     = gotoResults[gi]
        resultSequenceLog.append(resSeqItem)

        seqnum = seqnum + 1

    # Combine all planning results into single pandas table
    resultSequencePathPandas = PlannerTools.pathPandasConcat(resultSequenceLog)

    for i in range(len(resultSequenceLog)):
        resultSequenceLog[i]["distance"] = \
            resultSequencePathPandas.loc[resultSequencePathPandas['SEGMENT'] == i]["DISTANCE"].sum()
        resultSequenceLog[i]["duration"] = \
            resultSequencePathPandas.loc[resultSequencePathPandas['SEGMENT'] == i]["DURATION"].sum()
        resultSequenceLog[i]["work"]     = \
            resultSequencePathPandas.loc[resultSequencePathPandas['SEGMENT'] == i]["WORK"].sum()
        resultSequenceLog[i]["reward"]     = \
            resultSequencePathPandas.loc[resultSequencePathPandas['SEGMENT'] == i]["REWARD"].sum()

    resultSequenceLogPandas = PlannerTools.pathSequenceSummary2pandas(resultSequenceLog)

    return resultSequenceLog, resultSequencePathPandas, resultSequenceLogPandas




def main():

    ##################
    # Initialization #
    ##################

    # Constraints <------ Should be options!!!!!
    durationMax = 1000
    workMax     = 666666666666666

    # All important data should be linked here
    # in order to eventually save all results to json
    json_data = { "input"                 : dict(),
                  "plannerTargetSelect"   : dict(),
                  "plannerCoverage"       : dict(),
                  "plannerGoto"           : dict(),
                  "plannerSequence"       : None,
                  "plannerSequencePandas" : None,
                  "plannerSummaryPandas"  : None,
                  "nodeSequence"          : None,
                  "timeLog"               : None,
    }

    # Table for timing the path planners
    colnames = ["Task", "Type", "Time"]
    timeLog  = pd.DataFrame(columns = colnames)
    json_data["timeLog"] = timeLog

    # Parse options
    (options, args) = parseOptions()
    if options is None:
        exit()

    # Load data
    environment     = PlannerTools.initializeByFile(options.config, haveCurrents = options.build_currents)
    targetsTable    = PlannerTools.initTargetsTableByFile(environment["targets"]["table"])
    safepointsTable = PlannerTools.initSafepointsTableByFile(environment["vehicle"]["safepointsFile"])

    # Hacky rename since I don't like the original name, but need to maintain compatability
    environment["logbook"] = environment["targets"]["weights"]



    ################################
    # Option: Print results & exit #
    ################################
    if options.show_results == True:
        count = 0
        print ("in..")
        json_data = pickle.load(open(options.pickle, 'rb'))
        print ("open")

        # Save routes with maximal targets
        maxlen = 7
        routesMax = []
        for r in json_data["routesFeasible"]:
            if len(r["sequence"]) == 7:
                routesMax.append(r)

                ########### This section was to verify that distance was correctly calculated
                #d = 0
                #for g in r["gotoResults"]:
                #    d = d + g["solutionPath"]["distance"]["total"]
                #for c in r["coverageResults"]:
                #    gridPathLong, gStart, gTarget = PlannerTools.getPathLongForm(c["gridPath"])
                #    cHeading, cDistance, cDuration, cWork, cCoord, cReward = \
                #        PlannerTools.statPath(environment, gridPathLong, gStart, gTarget)  
                #    d = d + cDistance["total"]
                #    print cDistance["total"]

                print(r["sequence"]) 
                print("{},{},{},{}".format(r["distance"], r["duration"], r["work"], r["reward"]))

            else:
                continue

        for r in routesMax:
            print(r["sequence"]) 
            print("{},{},{},{}".format(r["distance"], r["duration"], r["work"], r["reward"]))

        exit()

        #for r in json_data["routesFeasible"][0:5]:
        #    print (r["sequence"])
        #    if count == 5:
        #        break
        #exit()             


    startPoint = { "Lat" : environment["vehicle"]["startCoords"][1],
                   "Lon" : environment["vehicle"]["startCoords"][0],
    }

    # Add to json
    json_data["input"]["targetsTable"]    = targetsTable
    json_data["input"]["safepointsTable"] = safepointsTable

    # Build region2point matrix
    region2point = PlannerTools.region2pointBuild(startPoint, targetsTable, safepointsTable, environment)

    numStarts  = 1                        # Always have one start
    numTargets = targetsTable.shape[0]    # Number of gotos
    numStops   = safepointsTable.shape[0] # Number of stops
    numNodes   = numStarts + numTargets + numStops

    starts = []
    targets = range(numStarts, numTargets + 1)
    stops   = range(numTargets + 1, numNodes)

    startArchive = GridUtil.getArchiveByWorld(startPoint["Lat"],
                                              startPoint["Lon"],
                                  environment["region"]["grid"],
              environment["region"]["raster"].GetGeoTransform())

    stopArchives = [None for i in stops]
    for i in range(numStops):
        stopArchives[i]  = GridUtil.getArchiveByWorld(safepointsTable.loc[i + 1]["Lat"],
                                                      safepointsTable.loc[i + 1]["Lon"],
                                                          environment["region"]["grid"],
                                      environment["region"]["raster"].GetGeoTransform())

    ###############################
    # Evaluate all possible paths #
    ###############################

    routeTemplate = { "sequence" : [], "fitness"  : 0,     "distance"  : 0,
                      "work"     : 0,  "duration" : 0,     "reward"    : 0,
                      "stopIdx"  : 0,  "feasible" : False, "seqcoords" : [],
    }

    routes           = []
    routesFeasible   = []
    routesInfeasible = []

    # Generate all possible route sequences 
    permTargets = []
    

    ############

    #for r in range(numTargets):
        #permTargets.extend(itertools.permutations(targets, r + 1))

    permTargets.extend(itertools.permutations(targets, len(targets)))

    ############


    for permTarget in permTargets:
        sequencePre = [0] + [p for p in permTarget]
        for s in stops:
            sequence                 = sequencePre + [s]
            route                    = routeTemplate.copy()
            route["sequence"]        = sequence
            route["stopIdx"]         = sequence[-1] - numTargets  # End point index
            route["selectedTargets"] = sequence[1:-1]             # Only targets
            route["seqcoords"]       = PlannerTools.sequence2coords(route["sequence"], startArchive, \
                                                   stopArchives[route["stopIdx"] - 1], region2point)
            routes.append(route)

    for route in routes:
        gotoResults, coverageResults, duration, nodeSequence = \
            planSequence(route["selectedTargets"], route["stopIdx"], route["seqcoords"], 
                        targetsTable, safepointsTable, environment, startPoint, timeLog)
        resultSequenceLog, resultSequenceLogPandas, resultSequencePathPandas = \
            getPathInfo(gotoResults, coverageResults)
      
        route["distance"]           = resultSequenceLogPandas["DISTANCE"].sum()
        route["duration"]           = resultSequenceLogPandas["DURATION"].sum()
        route["work"]               = resultSequenceLogPandas["WORK"]    .sum()
        route["reward"]             = resultSequenceLogPandas["REWARD"]  .sum()
        route["gotoResults"]        = gotoResults
        route["coverageResults"]    = coverageResults
        route["nodeSequence"]       = nodeSequence
        route["sequenceLog"]        = resultSequenceLog
        route["sequenceLogPandas"]  = resultSequenceLogPandas
        route["sequencePathPandas"] = resultSequencePathPandas

        if route["duration"] <= durationMax and route["work"] <= workMax:
            route["feasible"] = True
            routesFeasible.append(route)   # Shallow copy!
        else:
            route["feasible"] = False
            routesInfeasible.append(route) # Shallow copy!

    # Sort routes
    routesFeasible   = list(sorted(routesFeasible,            key = lambda k: k["duration"]))
    routesFeasible   = list(sorted(routesFeasible,            key = lambda k: k["work"]))
    routesFeasible   = list(reversed(sorted(routesFeasible,   key = lambda k: k["reward"])))
    routesInfeasible = list(sorted(routesInfeasible,          key = lambda k: k["duration"]))
    routesInfeasible = list(sorted(routesInfeasible,          key = lambda k: k["work"]))
    routesInfeasible = list(reversed(sorted(routesInfeasible, key = lambda k: k["reward"])))
  
    json_data["routesFeasible"]   = routesFeasible
    json_data["routesInfeasible"] = routesInfeasible

    # Fill json with info for top hit (based on reward)
    json_data["plannerCoverage"]["coverageResults"] = routesFeasible[0]["coverageResults"]
    json_data["plannerGoto"]["gotoResults"]         = routesFeasible[0]["gotoResults"]
    json_data["plannerSequence"]                    = routesFeasible[0]["sequenceLog"]
    json_data["plannerSequencePandas"]              = routesFeasible[0]["sequenceLogPandas"]
    json_data["plannerSummaryPandas"]               = routesFeasible[0]["sequencePathPandas"]
    json_data["nodeSequence"]                       = routesFeasible[0]["nodeSequence"]

    ################
    # Save results #
    ################
    with open(options.pickle, 'wb') as outfile:
        pickle.dump(json_data, outfile, protocol = pickle.HIGHEST_PROTOCOL)

if __name__ == "__main__":
    main()


    
