weights=(1 5 10 15 20)

####for distance in  "${weights[@]}"
####do
####    for obstacle in "${weights[@]}"
####    do
####        for skipTarget in "${weights[@]}"
####        do
####            for work in "${weights[@]}"
####            do
####                for repeat in "${weights[@]}"
####                do
####                   name=trial_$distance""_$obstacle""_$skipTarget""_$work""_$repeat""  
####                   python PathPlannerCoverage_driver.py -c TestDataArchive/config.yaml \
####                       -x "-70.9842" -y "42.3289" -w 50 -l 50 -k 10 -j 10 -m 2 -r 2 \
####                       -o testout/$name"".csv -f testout/$name"".png \
####                       -e $distance"",$obstacle"",$skipTarget"",$work"",$repeat 
####    
####                done
####            done
####        done
####    done
####done



python PathPlannerCoverage_driver.py -c TestDataArchive/config.yaml \
	-x "-70.9842" -y "42.3289" -w 50 -l 50 -k 10 -j 10 -m 2 -r 2 \
	-o testout/NEW-5-5-5-5-15.csv -f testout/NEW-5-5-5-5-15"".png \
	-e 5,5,5,5,15 

python PathPlannerCoverage_driver.py -c TestDataArchive/config.yaml \
	-x "-70.9842" -y "42.3289" -w 50 -l 50 -k 10 -j 10 -m 2 -r 2 \
	-o testout/NEW-5-5-5-5-20.csv -f testout/NEW-5-5-5-5-20"".png \
	-e 5,5,5,5,20

python PathPlannerCoverage_driver.py -c TestDataArchive/config.yaml \
	-x "-70.9842" -y "42.3289" -w 50 -l 50 -k 10 -j 10 -m 2 -r 2 \
	-o testout/NEW-5-5-5-15-15.csv -f testout/NEW-5-5-5-15-15"".png \
	-e 5,5,5,15,15 

python PathPlannerCoverage_driver.py -c TestDataArchive/config.yaml \
	-x "-70.9842" -y "42.3289" -w 50 -l 50 -k 10 -j 10 -m 2 -r 2 \
	-o testout/NEW-5-5-10-5-10.csv -f testout/NEW-5-5-10-5-10"".png \
	-e 5,5,10,5,10 

python PathPlannerCoverage_driver.py -c TestDataArchive/config.yaml \
	-x "-70.9842" -y "42.3289" -w 50 -l 50 -k 10 -j 10 -m 2 -r 2 \
	-o testout/NEW-5-5-10-5-20.csv -f testout/NEW-5-5-10-5-20"".png \
	-e 5,5,10,5,20 

python PathPlannerCoverage_driver.py -c TestDataArchive/config.yaml \
	-x "-70.9842" -y "42.3289" -w 50 -l 50 -k 10 -j 10 -m 2 -r 2 \
	-o testout/NEW-5-5-10-10-10.csv -f testout/NEW-5-5-10-10-10"".png \
	-e 5,5,10,10,10 

python PathPlannerCoverage_driver.py -c TestDataArchive/config.yaml \
	-x "-70.9842" -y "42.3289" -w 50 -l 50 -k 10 -j 10 -m 2 -r 2 \
	-o testout/NEW-5-5-10-15-1.csv -f testout/NEW-5-5-10-15-1"".png \
	-e 5,5,10,15,1 

python PathPlannerCoverage_driver.py -c TestDataArchive/config.yaml \
	-x "-70.9842" -y "42.3289" -w 50 -l 50 -k 10 -j 10 -m 2 -r 2 \
	-o testout/NEW-5-5-15-1-10.csv -f testout/NEW-5-5-15-1-10"".png \
	-e 5,5,15,1,10 

python PathPlannerCoverage_driver.py -c TestDataArchive/config.yaml \
	-x "-70.9842" -y "42.3289" -w 50 -l 50 -k 10 -j 10 -m 2 -r 2 \
	-o testout/NEW-5-5-15-20-5.csv -f testout/NEW-5-5-15-20-5"".png \
	-e 5,5,15,20,5 

python PathPlannerCoverage_driver.py -c TestDataArchive/config.yaml \
	-x "-70.9842" -y "42.3289" -w 50 -l 50 -k 10 -j 10 -m 2 -r 2 \
	-o testout/NEW-10-5-5-5-15.csv -f testout/NEW-10-5-15-20-5"".png \
	-e 10,5,15,20,5 


