import os

#########################
## TEST A:             ## 
## Currents & Entities ##
#########################

project = "A"

targets = [(42.2928, -70.9842),
           (42.3289, -70.9746),
           (42.3065, -70.9480),
           (42.3554, -70.8891),
           (42.3184, -70.8900),
           (42.2891, -70.8991),
           (42.2992, -70.9517)
          ]
ypoints = [p[0] for p in targets]
xpoints = [p[1] for p in targets]

configFile = "TestDataArchive/config.yaml"

# PSO params
generations = 500
individuals = 50
weights     = "-1,-1,-1,-1"

# Trials
trials = 2

outputLocation = "testout"
dataOutputPrefix = outputLocation + "/" + project



# Command
cmdS = "python PathPlannerGoto_driver.py " + \
       "-c " + str(configFile)    + " " + \
       "-y " + "'" + str(ypoints).replace('[','').replace(']','')  + "'" + " " + \
       "-x " + "'" + str(xpoints).replace('[','').replace(']','')  + "'" + " " + \
       "-w " + "'" + str(weights) + "'" + " " + \
       "-i " + str(individuals)   + " " + \
       "-g " + str(generations)   + " "
       
for i in range(trials):
    cmd = cmdS + "-o " + "trial-"  + dataOutputPrefix + "-" + str(i) + ".csv " \
                 "-f " + "figure-" + dataOutputPrefix + "-" + str(i) + ".png "

    os.system(cmd)

#########################
## TEST B:             ## 
## Currents Only       ##
#########################

#########################
## TEST C:             ## 
## Entities Only       ##
#########################


##########################
## TEST C:              ## 
## No Currents,Entities ##
##########################


