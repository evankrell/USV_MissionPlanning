import matplotlib.pyplot as plt
import dill              as pickle

import PlannerVis        as PlannerVis
import PlannerTools      as PlannerTools


def parseOptions():
    from optparse import OptionParser

    # Define options
    parser = OptionParser()
    parser.add_option("-c", "--config", dest="config", metavar="CONFIG",
        help="Path to configuration file")
    parser.add_option("-p", "--pickle", dest="pickle", metavar="PICKLE",
        help="Path to pickled results file")

    # Get options
    (options, args) = parser.parse_args()

    # Verify correct options
    if options.config is None:
        (options, args) = None, None
    if options.pickle is None:
        (options, args) = None, None

    return options, args


# Parse options
(options, args) = parseOptions()
if options == None:
    print ("Incorrect options")

mapWidth = 6

# Outputfiles
gotoTables = ["gotoSegment-1.csv",
              "gotoSegment-2.csv",
              "gotoSegment-3.csv",
              "gotoSegment-4.csv",
              "gotoSegment-5.csv",
             ]

plannerSequenceTable = "plannerSequence.csv"

blankMapFile     = "Fig_blankMap.png"
safezonesMapFile = "Fig_safezonesMap.png"
entitiesMapFile  = "Fig_entitiesMap.png"
targetsMapFile   = "Fig_targetsMap.png"
currentsMapFiles = [ "Fig_currentsMap-1.png"  ]

selectedTargetsMapFile = "Fig_selectedTargetsMap.png"
coverageMapFile        = "Fig_coverageMap.png"
gotoMapFile            = "Fig_gotoMap.png"
gotoMapFile            = "Fig_gotoMap.png"
coverageZoomMapFiles   = ["Fig_coverageZoomMap-1.png", 
                          "Fig_coverageZoomMap-2.png",
                          "Fig_coverageZoomMap-3.png",
                          "Fig_coverageZoomMap-4.png"
                         ]
gotoMapFile            = "Fig_gotoMap.png"


# Get environment
environment = PlannerTools.initializeByFile(options.config, haveCurrents = True)

# Load results pickle
state = pickle.load(open(options.pickle, "rb"))

# Write sequence summary csv
state["PlannerSequencePandas"].to_csv(plannerSequenceTable)

# Write Goto path csv
count = 0
for gotoSegment in state["plannerGoto"]["gotoResults"]:
    gotoSegment["pathPandas"].to_csv(gotoTables[count])
    count = count + 1

# Map of just the region
blankMap    = PlannerVis.makeBlankMap(environment["region"]["raster"],
                 mapWidth,
                 environment["region"]["file"],
                 blankMapFile)
# Map of region with water currents
currentsMap = PlannerVis.makeWaterCurrentMap(environment["region"]["raster"],
                  environment["currents"]['magnitude']['raster'],
                  environment["currents"]['direction']['raster'],
                  mapWidth,
                  environment["region"]["file"],
                  currentsMapFiles[0],
                  1,
                  10)
# Map of region with safezones
safezoneMap = PlannerVis.makeSafezonesMap(environment["region"]["raster"],
                 mapWidth,
                 environment["region"]["file"],
                 state["input"]["safepointsTable"],
                 safezonesMapFile)
# Map of region with entities
entitiesMap = PlannerVis.makeEntitiesMap(environment["region"]["raster"],
                 mapWidth,
                 environment["region"]["file"],
                 environment["targets"]["file"],
                 entitiesMapFile)
# Map of region with targets
targetsMap  = PlannerVis.makeTargetsMap(environment["region"]["raster"],
                  mapWidth,
                  environment["region"]["file"],
                  state["input"]["targetsTable"],
                  targetsMapFile)
# Map of region with selected target order
plannerTargetSelectMap = PlannerVis.makeTargetSelectMap(environment["region"]["raster"],
                  environment["region"]["file"],
                  state["input"]["targetsTable"],
                  state["plannerTargetSelect"]["selectedTargets"]["targetSequence"],
                  mapWidth,
                  selectedTargetsMapFile)
# Map of region with coverage paths
plannerCoverageMap    = PlannerVis.makeCoverageMap(environment["region"]["raster"],
                  environment["region"]["file"],
                  state["plannerCoverage"]["coverageResults"],
                  mapWidth,
                  coverageMapFile)
# Zoomed in maps of each target region
plannerCoverageZooms  = PlannerVis.makeCoverageZoomMaps(environment["region"]["raster"],
                  environment["region"]["file"],
                  state["plannerCoverage"]["coverageResults"],
                  mapWidth,
                  coverageZoomMapFiles)
# Map of each goto segment
plannerGotoMap        = PlannerVis.makeGotoMap(environment["region"]["raster"],
                  environment["region"]["file"],
                  state["plannerGoto"]["gotoResults"],
                  mapWidth,
                  gotoMapFile)

