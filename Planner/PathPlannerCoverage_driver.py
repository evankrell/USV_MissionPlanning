import dill                as pickle
import PlannerVis          as PlannerVis
import PlannerTools        as PlannerTools
import PathPlannerCoverage as PPC


def parseOptions():
    from optparse import OptionParser

    # Define options
    parser = OptionParser()
    parser.add_option("-c", "--config",         dest="config",         metavar="CONFIG",
        help="Path to config file")
    parser.add_option("-x", "--xpoints",        dest="xpoints",        metavar="XPOINTS",
        help="Comma-separated list of x components")
    parser.add_option("-y", "--ypoints",        dest="ypoints",        metavar="YPOINTS",
        help="Comma-separated list of y components")
    parser.add_option("-w", "--widths",         dest="widths",         metavar="WIDTHS",
        help="Comma-separated list of target area widths")
    parser.add_option("-l", "--lengths",        dest="lengths",        metavar="LENGTHS",
        help="Comma-separated list of target area lengths")
    parser.add_option("-k", "--numx",           dest="numx",           metavar="NUMX",
        help="Number of sample points along x axis")
    parser.add_option("-j", "--numy",           dest="numy",           metavar="NUMY",
        help="Number of sample points along y axis")
    parser.add_option("-m", "--margin",        dest="margin",          metavar="MARGIN",
        help="Points to keep away from obstacles")
    parser.add_option("-r", "--roam",          dest="roam",            metavar="ROAM",
        help="Extra points not in target area but allowed space for path manuevers")
    parser.add_option("-o", "--outdata",        dest="outdata",        metavar="OUTDATA",
        help="Output file to store pickled path data")
    parser.add_option("-f", "--outfigure",      dest="outfigure",      metavar="OUTFIGURE",
        help="Output file to store path figure")
    parser.add_option("-b", "--build_currents", dest="build_currents", metavar="BUILD_CURRENTS",
        action="store_false", default=True,
        help="Builds currents raster set. Otherwise, use existing rasters.")
    parser.add_option("-g", "--generations",    dest="generations",    metavar="GENERATIONS",
        help="Number of optimization generations")
    parser.add_option("-i", "--individuals",    dest="individuals",    metavar="INDIVIDUALS",
        help="Number of optimization pool individuals")
    parser.add_option("-e", "--weights",        dest="weights",        metavar="WEIGHTS",
        help="Weights for each fitness criteria")
    
   # Get options
    (options, args) = parser.parse_args()

    # Verify correct options
    if options.config  is None or \
       options.xpoints is None or options.ypoints   is None or \
       options.widths  is None or options.lengths   is None or \
       options.numx    is None or options.numy      is None or \
       options.margin  is None or options.roam      is None or \
       options.outdata is None or options.outfigure is None:
        (options, args) = None, None

    return options, args

def main():

    # Parse options
    (options, args) = parseOptions()
    if options is None:
        print("Bad options")
        exit()

    # Load data
    environment = PlannerTools.initializeByFile(options.config, haveCurrents = options.build_currents)

    # Parse targets from options
    yCoords = [float(y) for y in options.ypoints.split(',')]
    xCoords = [float(x) for x in options.xpoints.split(',')]
    widths  = [float(w) for w in options.widths.split(',')]
    lengths = [float(l) for l in options.widths.split(',')]
    yNums   = [float(m) for m in options.numy.split(',')]
    xNums   = [float(n) for n in options.numx.split(',')]

    if len(xCoords) != len(yCoords):
        print ("number of x and y coordinates do not match")
        exit()

    # Override configuration optimization settings
    if options.generations is not None:
        environment["plannerCoverage"]["generations"] = int(options.generations)
    if options.individuals is not None:
        environment["plannerCoverage"]["individuals"] = int(options.individuals)
    if options.weights is not None:
        weights = options.weights.split(',')
        environment["plannerCoverage"]["distanceWeight"]    = weights[0]
        environment["plannerCoverage"]["obstacleWeight"]    = weights[1]
        environment["plannerCoverage"]["skipTargetWeight"]  = weights[2]
        environment["plannerCoverage"]["workWeight"]        = weights[3]
        environment["plannerCoverage"]["repeatWeight"]      = weights[4]

    targets = []
    outzoomfigures = []
    for i in range(len(xCoords)):
        targets.append({ "Lat"     : float(yCoords[i]),   "Lon"     : float(xCoords[i]), 
                         "Width"   : int(widths[i]),      "Length"  : int(lengths[i]),
                         "Ypoints" : int(yNums[i]),       "Xpoints" : int(xNums[i]),
                         "Margin"  : int(options.margin), "Roam"    : int(options.roam)
                       })
        outzoomfigures.append(options.outfigure.split('.png')[0] + "-" + str(i) + ".png")
 
    print outzoomfigures

    # Do the coverage paths
    coverageResults = []
    for target in targets:
        points, path = PPC.driver(target["Lat"], target["Lon"], target["Width"], target["Length"], 
                                  target["Ypoints"], target["Xpoints"], target["Margin"], target["Roam"],
                                  environment, offset = 0)
        coverageResults.append({ "points" : points, "path" : path })

    ## Map of region with water currents
    #currentsMap = PlannerVis.makeWaterCurrentMap(environment["region"]["raster"],
    #              environment["currents"]['magnitude']['raster'],
    #              environment["currents"]['direction']['raster'],
    #              10,
    #              environment["region"]["file"],
    #              "currentsTest.png",
    #              1,
    #              10)



    # Map the results
    coverageMap = PlannerVis.makeCoverageMap(environment["region"]["raster"],
                      environment["region"]["file"],
                      coverageResults,
                      10,
                      options.outfigure)
    coverageZoomMaps = PlannerVis.makeCoverageZoomMaps(environment["region"]["raster"],
                           environment["region"]["file"],
                           coverageResults,
                           10,
                           outzoomfigures)

    # Save results
    with open(options.outdata, 'wb') as outfile:
        pickle.dump(coverageResults, outfile, protocol = pickle.HIGHEST_PROTOCOL)

    






if __name__ == "__main__":
    main()



