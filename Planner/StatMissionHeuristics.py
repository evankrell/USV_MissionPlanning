#!/usr/bin/python

# Dependecies, external
import pandas as pd
import numpy as np
from osgeo import gdal
from PyGMO.problem import base, tsp
from PyGMO import algorithm, island, problem, population
import math
import sys
import dill as pickle

# Dependencies, part of mission planner
import GridUtils           as GridUtil
import RasterSetInterface  as rsi
import PlannerTools        as PlannerTools
import PathPlannerCoverage as PPC
import PathPlannerGoto     as PPG

def getGotoMeasures(targetsTable, targetArchives, startArchive, safepointsTable, stopArchives, environment):

    # Get info on nodes
    numStarts  = 1                        # Always have one start
    numTargets = targetsTable.shape[0]    # Number of gotos
    numStops   = safepointsTable.shape[0] # Number of stops
    numNodes   = numStarts + numTargets + numStops
    starts = []   # <--- ??????
    targets = range(numStarts, numTargets + 1)
    stops   = range(numTargets + 1, numNodes)



    # Init table to hold goto measurements
    gotoMeasureTemplate = { "distance" : -1,
                            "duration" : -1,
                            "work"     : -1,
                            "reward"   : -1,
    }

    gotoMeasures = [[gotoMeasureTemplate.copy() for col in range(numNodes)] for row in range(numNodes)]

    gotoDetails  = [[gotoMeasureTemplate.copy() for col in range(numNodes)] for row in range(numNodes)]


    # Plan all start -> target

    row = 0 # Start node
    col = 0 # start node
    gotoMeasures[row][col]["distance"] = 0
    gotoMeasures[row][col]["duration"] = 0
    gotoMeasures[row][col]["work"]     = 0
    gotoMeasures[row][col]["reward"]   = 0

    print (row, col) 

    col = col + 1

    for t in targetArchives:
        gPath, elapsed = PPG.driver(startArchive, t, environment, 0)

        gotoMeasures[row][col]["distance"] = gPath["solutionPath"]["distance"]["total"]
        gotoMeasures[row][col]["duration"] = gPath["solutionPath"]["duration"]["total"]
        gotoMeasures[row][col]["work"]     = gPath["solutionPath"]["work"]    ["total"]
        gotoMeasures[row][col]["reward"]   = gPath["solutionPath"]["reward"]  ["total"]
        print (row, col)
        col = col + 1
        
    # Plan all target -> target
    row = 1
    for ti in targetArchives:
        col = 1
        for tj in targetArchives:
            gPath, elapsed = PPG.driver(ti, tj, environment, 0)

            gotoMeasures[row][col]["distance"] = gPath["solutionPath"]["distance"]["total"]
            gotoMeasures[row][col]["duration"] = gPath["solutionPath"]["duration"]["total"]
            gotoMeasures[row][col]["work"]     = gPath["solutionPath"]["work"]    ["total"]
            gotoMeasures[row][col]["reward"]   = gPath["solutionPath"]["reward"]  ["total"]
            print (row, col)
            col = col + 1
        row = row + 1

    # Plan all target -> stop
    row = 1
    col = numTargets + 1
    for ti in targetArchives:
        col  = numTargets + 1
        for s in stopArchives:
            gPath, elapsed = PPG.driver(ti, tj, environment, 0)

            gotoMeasures[row][col]["distance"] = gPath["solutionPath"]["distance"]["total"]
            gotoMeasures[row][col]["duration"] = gPath["solutionPath"]["duration"]["total"]
            gotoMeasures[row][col]["work"]     = gPath["solutionPath"]["work"]    ["total"]
            gotoMeasures[row][col]["reward"]   = gPath["solutionPath"]["reward"]  ["total"]
            print (row, col)
            col = col + 1
        row = row + 1

    return gotoMeasures



def getTargetCoverageList(targetsTable, environment):
    
    coverageMeasureTemplate = { "distance" : None, "duration" : None, "work" : None, "reward" : None }
    coverageMeasures = [coverageMeasureTemplate.copy() for i in targetsTable.iterrows()]
        

    for TID, ROW in targetsTable.iterrows():
        points, path = PPC.driver(float(ROW["Lat"]), float(ROW["Lon"]), float(ROW["Width"]), float(ROW["Length"]), 
             int(ROW["Ypoints"]), int(ROW["Xpoints"]), int(ROW["Margin"]), int(ROW["Roam"]), environment, offset = 0)
        # Convert path such that points are relative to world grid
        gridPath = PPC.convertTargetPath2WorldPath(points, path)
        gridPathLong, gStart, gTarget = PlannerTools.getPathLongForm(gridPath)
        # Get path stats
        cHeading, cDistance, cDuration, cWork, cCoord, cReward = \
            PlannerTools.statPath(environment, gridPathLong, gStart, gTarget)

        coverageMeasures[TID - 1]["distance"] = cDistance["total"]
        coverageMeasures[TID - 1]["duration"] = cDuration["total"]
        coverageMeasures[TID - 1]["work"]     = cWork    ["total"]
        coverageMeasures[TID - 1]["reward"]   = cReward  ["total"]

    return coverageMeasures


def getWorkH(m, targetsTable, startArchive, targetArchives, stopArchives, coverageMeasures, gotoMeasures):
    workPlan = 0
     # Sum coverageMeasures works
    for t in m["sequence"][1:-1]:
        workPlan = workPlan + coverageMeasures[t - 1]["work"]
        

    targets = m["sequence"][1:-1]

    # Work between start and first target
    i = targets[0]
    wp = gotoMeasures[0][i]["work"]
    workPlan  = workPlan  + wp

    # Work between targets
    if len(targets) > 1:   # Only if more than one target
        for x in range(len(targets) - 1):
            i = targets[x]
            j = targets[x + 1]
            wp = gotoMeasures[i][j]["work"]
            workPlan  = workPlan  + wp
    else:
        j = i

    # Work between last target and end
    wp = gotoMeasures[j][m["sequence"][-1]]["work"]
    workPlan  = workPlan  + wp

    return workPlan
    
def getRewardH(m, targetsTable, startArchive, targetArchives, stopArchives, coverageMeasures, gotoMeasures):
    rewardPlan = 0
     # Sum coverageMeasures rewards
    for t in m["sequence"][1:-1]:
        rewardPlan = rewardPlan + coverageMeasures[t - 1]["reward"]
        

    targets = m["sequence"][1:-1]

    # Reward between start and first target
    i = targets[0]
    rp = gotoMeasures[0][i]["reward"]
    rewardPlan  = rewardPlan  + rp

    # Reward between targets
    if len(targets) > 1:   # Only if more than one target
        for x in range(len(targets) - 1):
            i = targets[x]
            j = targets[x + 1]
            rp = gotoMeasures[i][j]["reward"]
            rewardPlan  = rewardPlan  + rp
    else:
        j = i

    # Reward between last target and end
    rp = gotoMeasures[j][m["sequence"][-1]]["reward"]
    rewardPlan  = rewardPlan  + rp

    return rewardPlan
   

def getDistanceH(m, targetsTable, startArchive, targetArchives, stopArchives, coverageMeasures, gotoMeasures):
    
    distanceBasic = 0
    distancePlan  = 0

    # Sum coverageMeasures distances
    for t in m["sequence"][1:-1]:
        distanceBasic = distanceBasic + coverageMeasures[t - 1]["distance"]
        distancePlan = distancePlan + coverageMeasures[t - 1]["distance"]
        

    targets = m["sequence"][1:-1]

    # Distance between start and first target
    i = targets[0]
    db = PlannerTools.euclideanDistance(startArchive["col"],         startArchive["row"], 
                              targetArchives[i - 1]["col"], targetArchives[i - 1]["row"])
    dp = gotoMeasures[0][i]["distance"]
    distanceBasic = distanceBasic + db
    distancePlan  = distancePlan  + dp

    # Distance between targets
    if len(targets) > 1:   # Only if more than one target
        for x in range(len(targets) - 1):
            i = targets[x]
            j = targets[x + 1]
            db = PlannerTools.euclideanDistance(targetArchives[i - 1]["col"], targetArchives[i - 1]["row"],
                                                targetArchives[j - 1]["col"], targetArchives[j - 1]["row"])
            dp = gotoMeasures[i][j]["distance"]
            distanceBasic = distanceBasic + db
            distancePlan  = distancePlan  + dp
    else:
        j = i

    # Distance between last target and end
    db = PlannerTools.euclideanDistance(targetArchives[j - 1]["col"], targetArchives[j - 1]["row"],
                      stopArchives[m["stopIdx"] - 1]["col"], stopArchives[m["stopIdx"] - 1]["row"])
    dp = gotoMeasures[j][m["sequence"][-1]]["distance"]
    distanceBasic = distanceBasic + db
    distancePlan  = distancePlan  + dp

    return distanceBasic, distancePlan


def statMissions(targetsTable, startPoint, safepointsTable, environment, missionsTable = None, pickleFile = None):

    missionTemplate = { "ID"        : None,
                        "sequence"  : None, 
                        "stopIdx"   : None,
                        "fitness"   : None,
                       }
    missions = []

    # Get info on nodes
    numStarts  = 1                        # Always have one start
    numTargets = targetsTable.shape[0]    # Number of gotos
    numStops   = safepointsTable.shape[0] # Number of stops
    numNodes   = numStarts + numTargets + numStops
    starts = []   # <--- ??????
    targets = range(numStarts, numTargets + 1)
    stops   = range(numTargets + 1, numNodes)


    startArchive = GridUtil.getArchiveByWorld(startPoint["Lat"],
                                              startPoint["Lon"],
                                  environment["region"]["grid"],
              environment["region"]["raster"].GetGeoTransform())
    # Case-sensitivity hack
    startArchive["Lat"] = startArchive["lat"]
    startArchive["Lon"] = startArchive["lon"]

    targetArchives = [None for i in targets]
    for i in range(numTargets):
        targetArchives[i] = GridUtil.getArchiveByWorld(targetsTable.loc[i + 1]["Lat"],
                                                       targetsTable.loc[i + 1]["Lon"],
                                                          environment["region"]["grid"],
                                      environment["region"]["raster"].GetGeoTransform())
        targetArchives[i]["Lat"] = targetArchives[i]["lat"]
        targetArchives[i]["Lon"] = targetArchives[i]["lon"]

    stopArchives = [None for i in stops]
    for i in range(numStops):
        stopArchives[i]  = GridUtil.getArchiveByWorld(safepointsTable.loc[i + 1]["Lat"],
                                                      safepointsTable.loc[i + 1]["Lon"],
                                                          environment["region"]["grid"],
                                      environment["region"]["raster"].GetGeoTransform())
        stopArchives[i]["Lat"] = stopArchives[i]["lat"]
        stopArchives[i]["Lon"] = stopArchives[i]["lon"]

    if pickleFile is None:
        # Get coverage results (always with offset = 0)
        coverageMeasures = getTargetCoverageList(targetsTable, environment)

        for c in coverageMeasures:
            print (c)
        
        # Get goto results (always with offset = 0)
        gotoMeasures = getGotoMeasures(targetsTable, targetArchives, startArchive, 
                                       safepointsTable, stopArchives, environment)

    else:
        json_data = pickle.load(open(pickleFile, 'rb'))
        
        gotoMeasures     = json_data["gotoMeasures"]
        coverageMeasures = json_data["coverageMeasures"]


    # Init list of requested missions
    if missionsTable is not None:

        for TID, ROW in missionsTable.iterrows():
     
            m = missionTemplate.copy()
            m["ID"]         = TID
            m["sequence"]   = [int(i) for i in ROW["Route"].replace("[", "").replace("]", "").split(" ")]
            m["stopIdx"]    = m["sequence"][-1] - numTargets  # End point index (as row id in safepointsTable)

            missions.append(m)

    # Init list of all possible missions
    else:

        exit()


    # Evaluate missions
    for m in missions:
        m["distanceBasic"], m["distancePlan"] = getDistanceH(m, targetsTable, startArchive,
                              targetArchives, stopArchives, coverageMeasures, gotoMeasures)
        m["workPlan"]   = getWorkH(m, targetsTable, startArchive,
                              targetArchives, stopArchives, coverageMeasures, gotoMeasures)
        m["rewardPlan"] = getRewardH(m, targetsTable, startArchive,
                              targetArchives, stopArchives, coverageMeasures, gotoMeasures)

    missions = list(sorted(missions, key = lambda k: k["distancePlan"]))

    count = 1
    for m in missions:
        print (count, m)
        count = count + 1
    
    for c in coverageMeasures:
        print (c)


    return gotoMeasures, coverageMeasures, missions

