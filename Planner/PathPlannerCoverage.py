#!/usr/bin/python
from __future__ import print_function

import GridUtils          as GridUtil
import PlannerTools       as PlannerTools
import RasterSetInterface as rsi
import pandas             as pd
import numpy              as np
from osgeo         import gdal
from PyGMO.problem import base
from PyGMO         import algorithm, island, problem
from scipy.spatial import distance
from deap          import creator, base, tools, algorithms
from bresenham     import bresenham
from numpy         import inf
from math          import ceil
import math
import sys
import random

########################
# OUTSTANDING ISSUES:
# 1. idx2rowcol was broken. 
#    Has every instance been replaced with node2coords?
# 2. lawnmower solver does not ignore non-target free nodes
# 3. tspSolver's distance matrix is not based on __real__ node coords
########################

# Globals
ENVIRONMENT     = None
REGION          = None
COVERAGE_PARAMS = None
COVERAGE_POINTS = None
COVERAGE_PATH   = None
OPTIMIZE_PARAMS = None
GOAL_POINTS     = None
DIM             = None
weights = { 'distance'   : 1,
            'obstacle'   : 100,
            'skipTarget' : 10,
            'work'       : 0,
            'repeat'     : 0,
          }
BOX            = {}


#def idx2rowcoa(idx, ncol):
#    row = int(idx / ncol)
#    col = int(idx % ncol)
#    return row, col

def rowcol2idx(row, col, ncol):
    idx = (row * ncol) + col
    return idx

def printPoints(points):
    nrows = len(points)
    ncols = len(points[0])

    for row in range(nrows):
        for col in range(ncols):
            if points[row][col]["occupied"] == True:
                sys.stdout.write('x ')
            elif points[row][col]["target"] == False:
                sys.stdout.write('- ')
            else:
                sys.stdout.write('  ')
        sys.stdout.write('\n')

def convertTargetPath2WorldPath(coveragePoints, coveragePath):
    worldPath = []

    if isinstance(coveragePath, dict):
        path = coveragePath["path"]
    else:
        path = coveragePath

    count = 0
    node2coords = np.array([(0, 0) for x in range(len(coveragePoints) * len(coveragePoints[0]))])
    for r in range(len(coveragePoints)):
        for c in range(len(coveragePoints[0])):
            node2coords[count][0] = r
            node2coords[count][1] = c
            count = count + 1

    p = { "row" : None, "col" : None }
    for point in path:
        pcoords = node2coords[point]
        p["row"] = coveragePoints[pcoords[0]][pcoords[1]]["row"]
        p["col"] = coveragePoints[pcoords[0]][pcoords[1]]["col"]
        worldPath.append(p.copy())

    return worldPath
        

def characterizeCoverageRegion(coveragePoints):

    # Obstacle counter
    numObstacles = 0

    # count obstacles
    nrows = len(coveragePoints)
    ncols = len(coveragePoints[0])
    for i in range(nrows):
        for j in range(ncols):
            if  coveragePoints[i][j]["occupied"] == True:
                numObstacles = numObstacles + 1

    info = { "obstacles" : numObstacles,
           }

    return info


def getCoveragePoints(region, center, size, npoints, margin, roam):

    def makePointNode(row, col, nrows, 
        occupied = False, target = True):
        pointNode = { "occupied": occupied, 
                      "target": target,
                      "row": row,
                      "col": col,
                      "rowFromBottom" : (-1) * row + nrows,
        }
        return pointNode

    #def printRectangle (ul, ll, ur, lr):
    #    print ("(%f,%f) ----- (%f,%f)" % (ul[0], ul[1], ur[0], ur[1]))
    #    print ("   |   (%f,%f)   |   " % (center[0], center[1]))
    #    print ("(%f,%f) ----- (%f,%f)" % (ll[0], ll[1], lr[0], lr[1]))

    # Make rectangle
        #   (xul, yul) ---------- (xur, yur)
        #       |                     |
        #       |                     |
        #   (xll, yll) ---------- (xlr, ylr)

    nrows = len(region["grid"])   

    # Rectangle with roam buffer
    ul = (int(center[0] - 0.5 * (size[0] + roam)), 
          int(center[1] - 0.5 * (size[1] + roam)))
    ll = (int(center[0] + 0.5 * (size[0] + roam)),
          int(center[1] - 0.5 * (size[1] + roam)))
    ur = (int(center[0] - 0.5 * (size[0] + roam)), 
          int(center[1] + 0.5 * (size[1] + roam)))
    lr = (int(center[0] + 0.5 * (size[0] + roam)),
          int(center[1] + 0.5 * (size[1] + roam)))


    arul = GridUtil.getArchiveByGrid(ul[0], ul[1], 
              region["grid"],
              region["raster"].GetGeoTransform())
    arul = [arul["lat"], arul["lon"]]
    arll = GridUtil.getArchiveByGrid(ll[0], ll[1], 
              region["grid"],
              region["raster"].GetGeoTransform())
    arll = [arll["lat"], arll["lon"]]
    arur = GridUtil.getArchiveByGrid(ur[0], ur[1], 
              region["grid"],
              region["raster"].GetGeoTransform())
    arur = [arur["lat"], arur["lon"]]
    arlr = GridUtil.getArchiveByGrid(lr[0], lr[1], 
              region["grid"],
              region["raster"].GetGeoTransform())
    arlr = [arlr["lat"], arlr["lon"]]
       

    # Rectangle without roam buffer
    wul = [int(center[0] - 0.5 * size[0]),
           int(center[1] - 0.5 * size[1])]
    wll = [int(center[0] + 0.5 * size[0]),
           int(center[1] - 0.5 * size[1])]
    wur = [int(center[0] - 0.5 * size[0]), 
           int(center[1] + 0.5 * size[1])]
    wlr = [int(center[0] + 0.5 * size[0]), 
           int(center[1] + 0.5 * size[1])]

    #printRectangle(ul, ll, ur, lr)
 
    #printRectangle(arul, arll, arur, arlr)


    # Uncomment if you want to save the region grid
    reg = region["grid"][ul[0]:ll[0], ll[1]:ur[1]]
    np.savetxt("margin" + str(center[0]) + ".out", reg, delimiter='', fmt='%i')

    # Generate points in rectangle
    ysample = np.linspace(ul[0], ll[0], npoints[0])
    xsample = np.linspace(ul[1], ur[1], npoints[1])


    points = []

    for y in ysample: 
        for x in xsample:
            points.append([y, x])

    pointNodes = []
    idx = 0
    for p in points:

        occupied = False
        target = False
        
        pul = (int(p[0] - 0.5 * margin), 
               int(p[1] - 0.5 * margin))
        pll = (int(p[0] + 0.5 * margin), 
               int(p[1] - 0.5 * margin))
        pur = (int(p[0] - 0.5 * margin),
               int(p[1] + 0.5 * margin))
        plr = (int(p[0] + 0.5 * margin),
               int(p[1] + 0.5 * margin))

        marginRegion = region["grid"][pul[0]:pll[0],pll[1]:pur[1]]

        # Check if occupied
        if np.sum(marginRegion) > 0:
            occupied = True

        # Check if target (via bounds)
        if p[0] > wul[0] and p[0] < wll[0] and \
           p[1] > wll[1] and p[1] < wur[1]:
            target = True

        pointNodes.append(makePointNode(p[0], p[1], len(region["grid"]), occupied, target))
        idx = idx + 1


    gpoints = [[dict() for i in range(npoints[0])] for j in range(npoints[1])]

    idx = 0
    col = 0
    row = 0
    for n in pointNodes:
        gpoints[row][col] = dict(n)
        col = col + 1
        if col % npoints[0] == 0:
            row = row + 1
            col = 0
        if col >= npoints[0]:
             col = 0
        if row >= npoints[1]:
             break
    return gpoints



def getCoveragePath():
    if REGION          == None or \
       COVERAGE_PARAMS == None or \
       OPTIMIZE_PARAMS == None or \
       GOAL_POINTS     == None:
        return None

    coveragePath = { "path"     : [],
                     "fitness"  : -inf,
                     "solver"   : "",
                     "trials"   : 0,
                     "duration" : inf, 
                   }  

    selectedSolver = ""
    # characterize coverage region
    regionInfo = characterizeCoverageRegion(COVERAGE_POINTS)
    # Select solver
    if regionInfo["obstacles"] <= 0:
        selectedSolver = "LAWNMOWER"
    else:
        selectedSolver = "DEAP-GA"

    # select path planner
    if selectedSolver   == "LAWNMOWER":
        result = lawnmowerSolver(COVERAGE_POINTS, GOAL_POINTS["start"], GOAL_POINTS["stop"])
        coveragePath["path"]     = result[0]
        coveragePath["fitness"]  = result[1]
        coveragePath["trials"]   = result[2]
        coveragePath["solver"]   = "LAWNMOWER"
        coveragePath["duration"] = result[3]
    elif selectedSolver == "GOOGLE-VRP":
        result = tspSolver(COVERAGE_POINTS, GOAL_POINTS["start"], GOAL_POINTS["stop"])
        coveragePath["path"]     = result[0]
        coveragePath["fitness"]  = result[1]
        coveragePath["trials"]   = result[2]
        coveragePath["solver"]   = "GOOGLE-VRP"
        coveragePath["duration"] = result[3]
    elif selectedSolver == "DEAP-GA":
        result = gaSolver(COVERAGE_POINTS, GOAL_POINTS["start"], GOAL_POINTS["stop"],
                          OPTIMIZE_PARAMS["generations"], OPTIMIZE_PARAMS["poolsize"],
                          weights)
        coveragePath["path"]     = result[0]
        coveragePath["fitness"]  = result[1]
        coveragePath["trials"]   = result[2]
        coveragePath["solver"]   = "DEAP-GA"
        coveragePath["duration"] = result[3]
    else:
        coveragePath = None

    return coveragePath


def plotPath (path, coveragePoints, region):
    # Source:
    #       https://stackoverflow.com/a/45708066
    import georaster
    from osgeo import gdal
    import matplotlib.pyplot as plt
    import numpy as np
    from mpl_toolkits.basemap import Basemap

    count = 0
    node2coords = np.array([(0, 0) for x in range(len(env) * len(env[0]))])
    for r in range(len(env)):
        for c in range(len(env[0])):
            node2coords[count][0] = r
            node2coords[count][1] = c
            count = count + 1

    fig = plt.figure (figsize = (6, 8))
    fpath = region['file']

    my_image = georaster.SingleBandRaster(fpath, load_data=False)

    # grab limits of image's extent  
    lowerleft  = (coveragePoints[0][0]["row"], 
                  coveragePoints[0][0]["col"])
    upperleft  = (coveragePoints[len(coveragePoints) - 1][0]["row"],
                  coveragePoints[len(coveragePoints) - 1][0]["col"])
    upperright = (coveragePoints[len(coveragePoints) - 1] \
                                [len(coveragePoints[0]) - 1]["row"],
                  coveragePoints[len(coveragePoints) - 1] \
                                [len(coveragePoints[0]) - 1]["col"])
    lowerright = (coveragePoints[0][len(coveragePoints[0]) - 1]["row"],
                  coveragePoints[0][len(coveragePoints[0]) - 1]["col"])


    worldlowerleft = GridUtil.grid2world(
                     lowerleft[0],
                     lowerleft[1],
                     region["raster"].GetGeoTransform())
    worldupperleft = GridUtil.grid2world(
                     upperleft[0],
                     upperleft[1],
                     region["raster"].GetGeoTransform())
    worldupperright = GridUtil.grid2world(
                     upperright[0],
                     upperright[1],
                     region["raster"].GetGeoTransform())
    worldlowerright = GridUtil.grid2world(
                     lowerright[0],
                     lowerright[1],
                     region["raster"].GetGeoTransform())
                  
    minx = worldlowerleft[1]
    miny = worldlowerleft[0]
    maxx = worldlowerright[1]
    maxy = worldupperleft[0]

    #minx, maxx, miny, maxy = my_image.extent

    # set Basemap with slightly larger extents
    # set resolution at intermediate level "i"
    m = Basemap (projection='cyl',
        llcrnrlon=minx-.005, \
        llcrnrlat=miny-.005, \
        urcrnrlon=maxx+.005, \
        urcrnrlat=maxy+.005, \
        resolution='h')

    # load the geotiff image, assign it a variable
    image = georaster.SingleBandRaster( fpath, \
        load_data=(minx, maxx, miny, maxy), \
        latlon=True)

    # plot the image on matplotlib active axes
    # set zorder to put the image on top of coastlines and continent areas
    # set alpha to let the hidden graphics show through
    plt.imshow(image.r, extent=(minx, maxx, miny, maxy), zorder=10, alpha=0.6)

    # Extract latitude and longitude from the path coordinates
    lat = []
    lon = []

    ncols = len(coveragePoints[0])
    for idx in path:
        if idx < 0:
            continue   # Skip nulls
        coords = node2coords[idx]
        row = coords[0]
        col = coords[1]
        worldrow, worldcol = GridUtil.grid2world(
            coveragePoints[row][col]["row"],
            coveragePoints[row][col]["col"],
            region["raster"].GetGeoTransform())
        lat.append(worldrow)
        lon.append(worldcol)
    x, y = m(lon, lat)
    m.plot (x, y, 'rx--')
    plt.savefig ("simplecoverage.png")
    return m

####################
# Solver Utilities #
####################

# Create solution path object
def packagePath(path):
    return None

 # Generate distance matrix from coverage_points 
def getDistMatrixFromCoveragePoints(coveragePoints):

    nrows = len(coveragePoints)
    ncols = len(coveragePoints[0])
    npoints = nrows * ncols

    count = 0
    node2coords = np.array([(0, 0) for x in range(ncols * nrows)])
    for r in range(nrows):
        for c in range(ncols):
            node2coords[count][0] = r
            node2coords[count][1] = c
            count = count + 1

    # Create grid
    dmat = np.array([[0 for x in range(npoints)] for y in range(npoints)])
    for pointA in range(npoints):
        coordsA = node2coords[pointA]
        for pointB in range(npoints):
            coordsB = node2coords[pointB]
            distAB = distance.euclidean(coordsA, coordsB)
            dmat[pointA, pointB] = distAB

    return dmat


    targets      = []      # List of targets by index
    targets2dmat = dict()  # Convert target index to dist matrix index
    dmat2targets = dict()  # Convert dist matrix index to target index

    nodeIDX = 0
    dmatIDX = 0
    for i in range(len(coveragePoints)):
        for j in range(len(coveragePoints[0])):

            # Check if node is unoccupied
            if  coveragePoints[i][j]["occupied"] == False:
                # Add the node to targets list
                targets.append(nodeIDX)
                targets2dmat[str(nodeIDX)] = str(dmatIDX)
                dmat2targets[str(dmatIDX)] = str(nodeIDX)
                # Increment dmatIDX since a target node added
                dmatIDX = dmatIDX + 1
            nodeIDX = nodeIDX + 1

    # Create 2D grid
    dmat  = pd.DataFrame(index = targets, columns = targets)
    # Setup node connections
    # Step 1: Disconnect all nodes
    dmat[:] = -1
    # Step 2: Set adjacent nodes to 1 (straight connection)
    #         or 1.41 (diagonal connection)
    costStraight = 1
    costDiagonal = 1.41

    for t in targets:
        tPos = node2coords[t]
        tRow = tPos[0]
        tCol = tPos[1]

        tDmatIDX = int(targets2dmat[str(t)])

        # Check node UP
        upRow = tRow - 1  # Row of node UP in coveragePoints
        upCol = tCol      # Col of node UP in coveragePoints
        if upRow >= 0:     # Ensure coordinates are within coveragePoints
            upIDX = rowcol2idx(upRow, upCol, ncols)    # Get node UP index at that location
            if upIDX in targets:                       # Ensure that node UP is a target
                # Get location of UP node in distance matrix
                upDmatIDX = int(targets2dmat[str(upIDX)])
                # Make connection in distance matrix
                #print ("UP", t, upIDX )
                dmat.iat[tDmatIDX, upDmatIDX] = costStraight
        # Check node DOWN
        downRow = tRow + 1  # Row of node DOWN in coveragePoints
        downCol = tCol      # Col of node DOWN in coveragePoints
        if downRow < nrows:     # Ensure coordinates are within coveragePoints
            downIDX = rowcol2idx(downRow, downCol, ncols)    # Get node DOWN index at that location
            if downIDX in targets:                           # Ensure that node DOWN is a target
                # Get location of UP node in distance matrix
                downDmatIDX = int(targets2dmat[str(downIDX)])
                # Make connection in distance matrix
                #print ("DOWN", t, downIDX)
                dmat.iat[tDmatIDX, downDmatIDX] = costStraight
        # Check node RIGHT
        rightRow = tRow      # Row of node UP in coveragePoints
        rightCol = tCol + 1  # Col of node UP in coveragePoints
        if rightCol < ncols: # Ensure coordinates are within coveragePoints
            rightIDX = rowcol2idx(rightRow, rightCol, ncols)    # Get node UP index at that location
            if rightIDX in targets:                             # Ensure that node UP is a target
                # Get location of UP node in distance matrix
                rightDmatIDX = int(targets2dmat[str(rightIDX)])
                # Make connection in distance matrix
                #print ("RIGHT", t, rightIDX)
                dmat.iat[tDmatIDX, rightDmatIDX] = costStraight
        # Check node LEFT
        leftRow = tRow      # Row of node UP in coveragePoints
        leftCol = tCol - 1  # Col of node UP in coveragePoints
        if leftCol >= 0:     # Ensure coordinates are within coveragePoints
            leftIDX = rowcol2idx(leftRow, leftCol, ncols)    # Get node UP index at that location
            if leftIDX in targets:                           # Ensure that node UP is a target
                # Get location of UP node in distance matrix
                leftDmatIDX = int(targets2dmat[str(leftIDX)])
                # Make connection in distance matrix
                #print ("LEFT", t, leftIDX)
                dmat.iat[tDmatIDX, leftDmatIDX] = costStraight

        # Check node UPPER-LEFT
        ulRow = tRow - 1  # Row of node UP in coveragePoints
        ulCol = tCol - 1  # Col of node UP in coveragePoints
        if ulRow >= 0 and ulCol >= 0:     # Ensure coordinates are within coveragePoints
            ulIDX = rowcol2idx(ulRow, ulCol, ncols)    # Get node UP index at that location
            if ulIDX in targets:                       # Ensure that node UP is a target
                # Get location of UP node in distance matrix
                ulDmatIDX = int(targets2dmat[str(ulIDX)])
                # Make connection in distance matrix
                #print (tDmatIDX, ulDmatIDX)
                dmat.iat[tDmatIDX, ulDmatIDX] = costDiagonal
        # Check node UPPER-RIGHT
        urRow = tRow - 1  # Row of node DOWN in coveragePoints
        urCol = tCol + 1  # Col of node DOWN in coveragePoints
        if urRow < nrows:     # Ensure coordinates are within coveragePoints
            urIDX = rowcol2idx(urRow, urCol, ncols)    # Get node UP index at that location
            if urIDX in targets:                       # Ensure that node UP is a target
                # Get location of UP node in distance matrix
                urDmatIDX = int(targets2dmat[str(urIDX)])
                # Make connection in distance matrix
                dmat.iat[tDmatIDX, urDmatIDX] = costDiagonal
        # Check node DOWN-LEFT
        dlRow = tRow + 1  # Row of node UP in coveragePoints
        dlCol = tCol - 1  # Col of node UP in coveragePoints
        if dlRow < nrows and dlCol >= 0: # Ensure coordinates are within coveragePoints
            dlIDX = rowcol2idx(dlRow, dlCol, ncols)    # Get node UP index at that location
            if dlIDX in targets:                       # Ensure that node UP is a target
                # Get location of UP node in distance matrix
                dlDmatIDX = int(targets2dmat[str(dlIDX)])
                # Make connection in distance matrix
                dmat.iat[tDmatIDX, dlDmatIDX] = costDiagonal
        # Check node DOWN-RIGHT
        drRow = tRow + 1  # Row of node UP in coveragePoints
        drCol = tCol + 1 # Col of node UP in coveragePoints
        if drRow < nrows and drCol < ncols:     # Ensure coordinates are within coveragePoints
            drIDX = rowcol2idx(drRow, drCol, ncols)    # Get node UP index at that location
            if drIDX in targets:                           # Ensure that node UP is a target
                # Get location of UP node in distance matrix
                drDmatIDX = int(targets2dmat[str(drIDX)])
                # Make connection in distance matrix
                dmat.iat[tDmatIDX, drDmatIDX] = costDiagonal
            
    # Package into dictionary for return
    dmatPack = { "dmat"         : dmat,
                 "targets"      : targets,
                 "targets2dmat" : targets2dmat,
                 "dmat2targets" : dmat2targets,
               }
    return dmatPack


def getSimpleMatrixFromCoveragePoints(coveragePoints):

    # Get region matrix dimensions    
    nrows = len(coveragePoints)
    ncols = len(coveragePoints[0])

    # Init all cells as targets
    mat = np.array([[0 for x in range(ncols)] for y in range(nrows)])

    # Find and assign all occupied cells
    for r in range(nrows):
        for c in range(ncols):
            if coveragePoints[r][c]["occupied"] == True:
                mat[r][c] = 1
    return mat


#####################
# SOLVER: Lawnmower #
#####################

def lawnmowerSolver(coveragePoints, startPoint, endPoint):
    # Generate a lawnmower path

    # Prepare data
        # !!!!! Need to trim the non-target boundary
    env          = getSimpleMatrixFromCoveragePoints(coveragePoints)

    # Generate coord2node table. Converts a 'row, col' to a numeric node ID
    # (Each grid cell is a node)
    count = 0
    coords2node = np.array([[0 for x in range(len(env[0]))] for y in range(len(env))])
    for r in range(len(env)):
        for c in range(len(env[0])):
            coords2node[r][c] = count
            count = count + 1

    # Generate node2coord table. Converts a numeric node ID to 'row,col'.
    # (Each grid cell is a node)
    count = 0
    node2coords = np.array([(0, 0) for x in range(len(env) * len(env[0]))])
    for r in range(len(env)):
        for c in range(len(env[0])):
            node2coords[count][0] = r
            node2coords[count][1] = c
            count = count + 1

    nrows   = len(coveragePoints)
    ncols   = len(coveragePoints[0])
    sCoords = node2coords[startPoint]
    eCoords = node2coords[endPoint]

    spanMethods  = ["ROW", "COL"]
    spanResults  = []
    spanMeasures = []
    for spanMethod in spanMethods:

        # Init path with start point
        path = [startPoint]

        # Goto corner
        go = [None, None]
        # Where to go
            # Up or Down?
        if abs(sCoords[0] - 0) < abs(sCoords[0] - (nrows - 1)):
            go[0] = "UP"
        else:
            go[0] = "DOWN"
        # Left or right? 
        if abs(sCoords[1] - 0) < abs(sCoords[1] - (ncols - 1)):
            go[1] = "LEFT"
        else:
            go[1] = "RIGHT"
        # Go to corner point 
        selectedCornerCoords = [None, None]
        if go[0] == "UP":
            selectedCornerCoords[0] = 0
        else:
            selectedCornerCoords[0] = nrows - 1
        if go[1] == "LEFT":
            selectedCornerCoords[1] = 0
        else:
            selectedCornerCoords[1] = ncols - 1
        path.append(coords2node[selectedCornerCoords[0], selectedCornerCoords[1]])

        # Construct lawnmower path
        at = go
        posCoords = sCoords

        if spanMethod == "ROW":
            if at[0] == "UP":
                rows2traverse = range(nrows)
            else:
                rows2traverse = reversed(range(nrows))
            for r in rows2traverse:
                if at[1] == "LEFT":
                    cols2traverse = range(ncols)
                    at[1] = "RIGHT"
                else:
                    cols2traverse = reversed(range(ncols))
                    at[1] = "LEFT"
                for c in cols2traverse:
                    path.append(coords2node[r, c])
        else: 
            if at[1] == "LEFT":
                cols2traverse = range(ncols)
            else:
                cols2traverse = reversed(range(ncols))
            for c in cols2traverse:
                if at[0] == "UP":
                    rows2traverse = range(nrows)
                    at[0] = "DOWN"
                else:
                    rows2traverse = reversed(range(nrows))
                    at[0] = "UP"
                for r in rows2traverse:
                    path.append(coords2node[r, c])
            
        # Add final point
        path.append(endPoint)

        # Convert path from coverage points to to world grid coords
        gridPath = convertTargetPath2WorldPath(coveragePoints, path)
        gridPathLong, gStart, gTarget = PlannerTools.getPathLongForm(gridPath)
        # Get path stats
        cHeading, cDistance, cDuration, cWork, cCoord, cReward = \
            PlannerTools.statPath(ENVIRONMENT, gridPathLong, gStart, gTarget, ENVIRONMENT["timespan"]["offset"])

        measures = { "method"    : spanMethod,
                     "from"      : [r, c],
                     "endPoint"  : [eCoords[0], eCoords[1]],
                     "distance"  : \
            PlannerTools.euclideanDistance(c, r, eCoords[1], eCoords[0]),
                     "work"      : cWork["total"],
                     "duration"  : cDuration["total"],
        }

        spanResults.append(path)
        spanMeasures.append(measures.copy())

    # Choose best path
    bestMeasures = None
    bestWork     = inf
    bestPath     = None
    idx = 0
    for measures in spanMeasures:
        if measures["work"] < bestWork:
            bestWork     = measures["work"]
            bestMeasures = measures
            bestPath     = spanResults[idx]
        idx = idx + 1

    return bestPath, len(bestPath), 1, bestMeasures["duration"]
    #return (spanResults[1], len(spanResults[1]), 1, spanMeasures[1]["duration"])

######################
# SOLVER: Google TSP #
######################
def tspSolver(coveragePoints, startPoint, endPoint):
    # Based on the example from: https://developers.google.com/optimization/routing/tsp

    # Imports
    from ortools.constraint_solver import pywrapcp
    from ortools.constraint_solver import routing_enums_pb2
   
    # Distance callback
    def createDistanceCallback(distMatrix):
        # Create a callback to calculate distances between cities    
        def distanceCallback(fromNode, toNode):
            return int(distMatrix[fromNode][toNode])
        return distanceCallback

       # Build distance matrix
    distMatrix = getDistMatrixFromCoveragePoints(coveragePoints)

    # TSP params
    tspSize     = len(coveragePoints) * len(coveragePoints[0])
    numRoutes   = 1
    startDepots = [startPoint]
    endDepots   = [endPoint]

    # Create routing model
    if tspSize > 0:
        routing = pywrapcp.RoutingModel(tspSize, numRoutes, startDepots, endDepots)
        searchParameters = pywrapcp.RoutingModel.DefaultSearchParameters()
        searchParameters.first_solution_strategy = (
        routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)
        #searchParameters.time_limit_ms = 50000
        # Create the distance callback
        distCallback = createDistanceCallback(distMatrix)
        routing.SetArcCostEvaluatorOfAllVehicles(distCallback)
        # Solve the problem
        assignment = routing.SolveWithParameters(searchParameters)

        distance = 0
        # Show results
        if assignment:
            distance = assignment.ObjectiveValue()
            routeNumber = 0
            index = routing.Start(routeNumber) # Index of the variable for the starting node.
            path = []
            while not routing.IsEnd(index):
                # Convert variable indices to node indices in the displayed route
                path.append(routing.IndexToNode(index))
                index = assignment.Value(routing.NextVar(index))
            path.append(routing.IndexToNode(index))
        else:
            path = []
    else:
        print('Specify an instance greater than 0.')

    return path, distance, 1, None  # <---- Need to fix that 'None'

#######################
# SOLVER: GA via DEAP #
#######################

def convertIDX(chromosome):
    convertedChromosome = []
    for c in chromosome:
        convertedChromosome.append(BOX["targetMap"][c])
    return convertedChromosome

def simplePath(path):
    return [a for a,b in zip(path, path[1:]+[not path[-1]]) if a != b]

def getInternalNodes(pointA, pointB, node2coords, coords2nodes):
    coordsA        = node2coords[pointA]
    coordsB        = node2coords[pointB]
    internalCoords = list(bresenham(coordsA[1], coordsA[0], coordsB[1], coordsB[0]))
    internalCoords.pop(0)
    internalCoords.pop()
    internalNodes  = [coords2nodes[ic[1], ic[0]] for ic in internalCoords]
    return internalNodes

def expandPathAdj(path, node2coords, coords2nodes):
    newPath = []
    pointA = path[0]
    newPath.append(pointA)
    for p in path[1:]:
        pointB = p
        internalNodes = getInternalNodes(pointA, pointB, node2coords, coords2nodes)
        for i in internalNodes:
            newPath.append(i)
        pointA = pointB
        newPath.append(pointB)
    return newPath

def getPointDist(pointA, pointB, node2coords):
    coordsA = node2coords[pointA]
    coordsB = node2coords[pointB]
    distAB = distance.euclidean(coordsA, coordsB) ** 2
    return distAB

def getPathDist(path, node2coords):
    distance = 0.0
    pointA = path[0]
    for p in path[1:]:
        pointB = p
        distance = distance + getPointDist(pointA, pointB, node2coords)
        pointA = pointB
    return distance

def getPathObstacles(path, env, node2coords):
    numObstacles = 0
    for p in path:
        coordsP = node2coords[p]
        if env[coordsP[0], coordsP[1]] == 1:
            numObstacles = numObstacles + 1
    return numObstacles

def getPathRepeats(path):
    uniques = []
    repeats = 0
    for p in path:
        if p in uniques:
            repeats = repeats + 1
        else:
            uniques.append(p)
    return repeats

def getPathTurns(path, node2coords):

    def getDirection(pointA, pointB):
        direction = 'NONE'
        coordsA = node2coords[pointA]
        coordsB = node2coords[pointB]
        if coordsA[0] + 1 == coordsB[0] and coordsA[1] == coordsB[1]:
            direction = 'DOWN'
        if coordsA[0] - 1 == coordsB[0] and coordsA[1] == coordsB[1]:
            direction = 'UP'
        if coordsA[0] == coordsB[0] and coordsA[1] + 1 == coordsB[1]:
            direction = 'RIGHT'
        if coordsA[0] == coordsB[0] and coordsA[1] - 1 == coordsB[1]:
            direction = 'LEFT'
        if coordsA[0] + 1 == coordsB[0] and coordsA[1] + 1 == coordsB[1]:
            direction = 'DOWN-RIGHT'
        if coordsA[0] + 1 == coordsB[0] and coordsA[1] - 1 == coordsB[1]:
            direction = 'DOWN-LEFT'
        if coordsA[0] - 1 == coordsB[0] and coordsA[1] + 1 == coordsB[1]:
            direction = 'UP-RIGHT'
        if coordsA[0] - 1 == coordsB[0] and coordsA[1] - 1 == coordsB[1]:
            direction = 'UP-LEFT'
        return direction

    numTurns = 0
    pointA = path[0]
    pointB = path[1]
    currentDirection = getDirection(pointA, pointB)
    for pointB in path[1:]:
        newDirection = getDirection(pointA, pointB)
        if (currentDirection != 'NONE' and currentDirection != newDirection):
            numTurns = numTurns + 1
            currentDirection = newDirection
        pointA = pointB
    return numTurns



def gaSolver(coveragePoints, startPoint, endPoint, generations, individuals, weights):

    def evalNodeBased(individual):
        global BOX
    
        # Convert chromosome to actual nodes
        convertedChromosome = convertIDX(individual)
    
        # Convert individual to path
        path = [nodes[BOX["start"][0], BOX["start"][1]]]    # Add start node
        for i in convertedChromosome:     # Optimization individual is interior nodes
            if i > 0:
                path.append(i)
        path.append(nodes[BOX["goal"][0], BOX["goal"][1]]) # Add goal node
        path = simplePath(path)
    
        # Expand path
        path = expandPathAdj(path, BOX["node2coords"], BOX["coords2nodes"])
    
        # Get path information
        pathCount    = len(path)
        pathDistance = getPathDist(path, BOX["node2coords"])
        targetCount  = len(set(path).intersection(set(targetMap)))
    
        # Count obstacles 
        numObstacles = getPathObstacles(path, BOX["env"], BOX["node2coords"])
    
        # Count repeats
        numRepeats = getPathRepeats(path)
    
        # Count turns
        numTurns = getPathTurns(path, BOX["node2coords"])
    
        # Calculate path fitness
        targetsNeed = 18 - targetCount
        fitness = (pathDistance
            + targetsNeed  * weights["skipTarget"]
            + numObstacles * weights["obstacle"]
            + numTurns     * weights["work"]
            + numRepeats   * weights["repeat"])
        fitness = (-1) * fitness
    
        if BOX["verbose"] == True:
            print(path, "obstacleCount:", numObstacles, 
                        "targetCount:", targetCount, 
                         "pathDistance:", pathDistance, 
                         "numTurns", numTurns, 
                         "fitness:", fitness)
    
        return [fitness]


    # Prepare dataset
    env   = getSimpleMatrixFromCoveragePoints(coveragePoints)
    
    # Setup utilities
    # Generate coord2node table. Converts a 'row, col' to a numeric node ID
    # (Each grid cell is a node)
    count = 0
    nodes = np.array([[0 for x in range(len(env[0]))] for y in range(len(env))])
    for r in range(len(env)):
        for c in range(len(env[0])):
            nodes[r][c] = count
            count = count + 1
    
    # Generate node2coord table. Converts a numeric node ID to 'row,col'.
    # (Each grid cell is a node)
    count = 0
    node2coords = np.array([(0, 0) for x in range(len(env) * len(env[0]))])
    for r in range(len(env)):
        for c in range(len(env[0])):
            node2coords[count][0] = r
            node2coords[count][1] = c
            count = count + 1

    start = node2coords[startPoint]
    goal  = node2coords[endPoint]

   
    # Generate targetMap. 
    # If there are n targets, they can be indexed 1 .. n.
    # But if obstacles, etc exist, then there are m nodes: m = n + l, where l are non-target nodes.
    # So targetMap maps the nth target to its location within the m nodes. 
    # EXAMPLE:  [0, 1, 2]
    #           [3, 4, 5]
    #           [X, X, 8]
    # There are 6 targets and 2 obstacles. Target #5 is mapped to node 5,
    #                                  but target #6 is mapped to node 8.
    targetMap = []
    count = 0
    for r in range(len(env)):
        for c in range(len(env[0])):
            if env[r, c] == 0:
                targetMap.append(count)
            count = count + 1
    numTargets = len(targetMap)

    global BOX
    BOX["env"]            = env
    BOX["start"]          = start
    BOX["goal"]           = goal 
    BOX["targetMap"]      = targetMap
    BOX["node2coords"]    = node2coords 
    BOX["coords2nodes"]   = nodes
    BOX["verbose"]        = False
    BOX["weights"]        = weights

    # Setup Solver
    gaDimensions = numTargets * 2
    creator.create("FitnessMax", base.Fitness, weights=(1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMax)
    toolbox = base.Toolbox()
    toolbox.register("attr_act", random.randint, 0, len(targetMap) - 1 )
    toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_act, n = gaDimensions)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    toolbox.register("evaluate", evalNodeBased)
    toolbox.register("mate", tools.cxTwoPoint)
    toolbox.register("mutate", tools.mutFlipBit, indpb=0.05)
    toolbox.register("select", tools.selTournament, tournsize=3)
    population = toolbox.population(n=individuals)
    hof = tools.HallOfFame(1)
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", np.mean)
    stats.register("min", np.min)
    stats.register("max", np.max)

    # Number of trials performed
    trials = 0

    # Execute solver
    pop, logbook = algorithms.eaSimple(population, toolbox, 
                   cxpb=0.4, mutpb=0.2, ngen=generations,
                   stats=stats, halloffame=hof, verbose=BOX["verbose"])
    trials = trials + 1

    
    # Convert fittest chromosome to actual nodes
    convertedChromosome = convertIDX(hof[0])
    # Convert individual to path
    path = [nodes[BOX["start"][0], BOX["start"][1]]]    # Add start node
    for i in convertedChromosome:     # Optimization individual is interior nodes
        if i > 0:
            path.append(i)
    path.append(nodes[BOX["goal"][0], BOX["goal"][1]]) # Add goal node
    path = simplePath(path)
    # Expand path
    path = expandPathAdj(path, BOX["node2coords"], BOX["coords2nodes"])

    # Convert path from coverage points to to world grid coords
    gridPath = convertTargetPath2WorldPath(coveragePoints, path)
    gridPathLong, gStart, gTarget = PlannerTools.getPathLongForm(gridPath)
    # Get path stats
    cHeading, cDistance, cDuration, cWork, cCoord, cReward = \
        PlannerTools.statPath(ENVIRONMENT, gridPathLong, gStart, gTarget, ENVIRONMENT["timespan"]["offset"])

    return path, hof[0].fitness.values[0], trials, cDuration["total"]


def driver(lat, lon, width, length, ypoints, xpoints, margin, roam, 
                  environment, offset = 0, startPoint = None , stopPoint = None):

    # Hacky fix to a bad name
    environment["logbook"] = environment["targets"]["weights"]

    terrainFlag = 1

    # Offset the start time
    environment["timespan"]["offset"] = offset

    global weights
    if float(environment["plannerCoverage"]["distanceWeight"]) >= 0:
        weights["distance"]   = float(environment["plannerCoverage"]["distanceWeight"])
    if float(environment["plannerCoverage"]["obstacleWeight"]) >= 0:
        weights["obstacle"]   = float(environment["plannerCoverage"]["obstacleWeight"])
    if float(environment["plannerCoverage"]["skipTargetWeight"]) >= 0:
        weights["skipTarget"] = float(environment["plannerCoverage"]["skipTargetWeight"])
    if float(environment["plannerCoverage"]["workWeight"]) >= 0:
        weights["work"]       = float(environment["plannerCoverage"]["workWeight"])
    if float(environment["plannerCoverage"]["repeatWeight"]) >= 0:
        weights["repeat"]     = float(environment["plannerCoverage"]["repeatWeight"])

    global ENVIRONMENT
    ENVIRONMENT = environment

    global REGION
    REGION = environment["region"]

    # Center of coverage area
    centerArchive = GridUtil.getArchiveByWorld(lat, lon,
                        environment["region"]["grid"], 
                        environment["region"]["raster"].GetGeoTransform())
    center = (centerArchive["row"], centerArchive["col"])

    # Width, length of coverage area
    size = (length, width)
    # Resolution of coverage area (y, x)
    npoints = (ypoints, xpoints)

    global COVERAGE_PARAMS
    COVERAGE_PARAMS = { "center"  : center,
               "size"    : size,
               "npoints" : npoints,
               "margin"  : margin,
               "roam"    : roam,
    }
    
    global OPTIMIZE_PARAMS
    OPTIMIZE_PARAMS = { "generations" : environment["plannerCoverage"]["generations"],
                        "poolsize"    : environment["plannerCoverage"]["individuals"],
    }

    # Get points in coverage area that avoid obstacles
    global COVERAGE_POINTS
    COVERAGE_POINTS = getCoveragePoints(REGION, center, size, npoints, margin, roam)
    #printPoints(COVERAGE_POINTS)
    

    # If no specified start point, 
    # assume upper left corner
    if startPoint == None:
        startPoint = [center[0] - int(0.5 * length),
                      center[1] - int(0.5 * width)]
    if stopPoint  == None:
        stopPoint  = [center[0] + int(0.5 * length - 1),
                      center[1] + int(0.5 * width  - 1)]

    # Start at corner
    start, startError = gridpoint2coveragepoint(startPoint[0], startPoint[1], COVERAGE_POINTS)
    startIdx = rowcol2idx(start[0], start[1], len(COVERAGE_POINTS[0]))

    # Stop at corner
    stop, stopError   = gridpoint2coveragepoint(stopPoint[0], stopPoint[1], COVERAGE_POINTS)
    stopIdx = rowcol2idx(stop[0], stop[1], len(COVERAGE_POINTS[0]))
    
    global GOAL_POINTS
    GOAL_POINTS = { "start" : startIdx,
                    "stop"  : stopIdx,
    }

    # Get path to travel over coverage area
    global COVERAGE_PATH
    COVERAGE_PATH = getCoveragePath()

    return COVERAGE_POINTS, COVERAGE_PATH

def gridpoint2coveragepoint(row, col, coveragePoints):
    # Bad function as it finds all latlons..

    closestCoords = [None, None]
    closestDist   = inf

    # Get region matrix dimensions    
    nrows = len(coveragePoints)
    ncols = len(coveragePoints[0])

    # Find closest point
    for r in range(nrows):
        for c in range(ncols):
            dist = PlannerTools.euclideanDistance(int(coveragePoints[r][c]["col"]),
                       int(coveragePoints[r][c]["row"]), col, row)
            if (dist < closestDist):
                #print(coveragePoints[r][c]["row"], row, r)
                #print(coveragePoints[r][c]["col"], col, c)
                closestDist   = dist
                closestCoords = [r, c]

    return closestCoords, closestDist





    














##################
# SOLVER: OLD GA #
##################

#global ACTIONS
#ACTIONS = ['S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'S', 'N', 
#           'L', 'L', 'L', 'L', 'R', 'R', 'R', 'R', 'U', 'U', 'N']
#ACTIONS = ['N', 'S', 'L', 'R', 'U']
#weights = { 'distance'   : 1,
#            'obstacle'   : 10,
#            'skipTarget' : 10,
#            'work'       : 10,
#            'repeat'     : 10,
#          }

####def pathFitness2(actionSequence, coveragePoints, region):
####
####    # Region boundaries
####    nrows = len(coveragePoints)
####    ncols = len(coveragePoints[0])
####
####    def getMoveIdx(position, orientation, action, ncols):
####        positionCoords = idx2rowcol(position, ncols)
####
####        newCoords = [0, 0]
####        newPosition = position
####
####        work = 0
####        
####        # Using action, select move
####        if   action == 'S':
####            work = 0
####            move = orientation
####        elif action == 'L':
####            work = 5 
####            if   orientation == 'U':
####                
####                    # Get index from row, col
####                    move = 'L'
####            elif orientation == 'L':
####                move = 'D'
####            elif orientation == 'D':
####                move = 'R'
####            elif orientation == 'R':
####                move = 'U'
####        elif action == 'R':
####            work = 5
####            if   orientation == 'U':
####                move = 'R'
####            elif orientation == 'L':
####                move = 'U'
####            elif orientation == 'D':
####                move = 'L'
####            elif orientation == 'R':
####                move = 'D'
####        elif action == 'U':
####            work = 10
####            if   orientation == 'U':
####                move = 'D'
####            elif orientation == 'L':
####                move = 'R'
####            elif orientation == 'D':
####                move = 'U'
####            elif orientation == 'R':
####                move = 'L'
####        else:
####            return position, 'N', 0
####
####        # Execute move
####        if   move == 'U':
####            newCoords[0] = positionCoords[0] - 1
####            newCoords[1] = positionCoords[1]
####        elif move == 'D':
####            newCoords[0] = positionCoords[0] + 1
####            newCoords[1] = positionCoords[1]
####        elif move == 'L':
####            newCoords[0] = positionCoords[0]
####            newCoords[1] = positionCoords[1] - 1
####        elif move == 'R':
####            newCoords[0] = positionCoords[0]
####            newCoords[1] = positionCoords[1] + 1
####
####        newPosition = rowcol2idx(newCoords[0], newCoords[1], ncols)
####        return newPosition, move, work
#### 
####   
####    # Initialize previous position
####    prev = { "act"         : 'N',
####             "idx"         : GOAL_POINTS["start"],
####             "orientation" : 'L',
####    }
####    curr = { "act"         : None,
####             "idx"         : None,
####             "orientation" : None,
####    }
####
####    # Initialize path with starting point
####    path = []
####    path.append(prev.copy())
####
####    work = 0
####    seqlen = float (len(actionSequence))
####    global ACTIONS
####    count = 0
####    for action in actionSequence:
####
####        curr["act"] = ACTIONS[int(action)]
####        # See where you would end up
####        curr["idx"], curr["orientation"], deltaWork = getMoveIdx(prev["idx"], 
####                                                          prev["orientation"], 
####                                                          curr["act"], ncols)
####        # See what the region value is
####        coords = idx2rowcol(curr["idx"], ncols)
####
####        # If no action, go onto next iteration
####        if curr["act"] == 'N':
####            continue
#### 
####        if coords[0] < 0 or coords[0] >= nrows - 1 or \
####           coords[1] < 0 or coords[1] >= ncols - 1:
####            
####            continue
####
####        # Do work
####        work = work + deltaWork
####
####
####        # Add node to path
####        path.append(curr.copy())
####
####        #if count > 5:
####        #    print (path)
####        #    exit()
####        count = count + 1
####
####        # Made current node the previous
####        prev["idx"]       = curr["idx"]
####        prev["act"]       = curr["act"]
####        prev["orientation"] = curr["orientation"]
####
####    # Check path fitness
####    distTotal = 0
####    visitedTargets   = []
####    visitedRepeats   = []
####    visitedObstacles = []
####    nodeB = None
####
####
####    if len(path) == 1:
####        print (path)
####        nodeB  = path[0]
####        coordB = idx2rowcol(nodeB["idx"], ncols)
####        pointB = coveragePoints[coordB[0]][coordB[1]]
####
####
####    for i in range(len(path) - 1):
####        j = i + 1
####        nodeA  = path[i]
####        nodeB  = path[j]
####        coordA = idx2rowcol(nodeA["idx"], ncols)
####        coordB = idx2rowcol(nodeB["idx"], ncols)
####        pointA = coveragePoints[coordA[0]][coordA[1]]
####        pointB = coveragePoints[coordB[0]][coordB[1]]
####        distAB = PlannerTools.euclideanDistance(pointA["row"], pointA["col"],
####                                                pointB["row"], pointB["col"])
####        distTotal = distTotal + distAB
#### 
####        if nodeA["idx"] not in visitedTargets and pointA["target"] == True and pointA["occupied"] == False:
####            visitedTargets.append(nodeA["idx"])
####
####        if nodeA["idx"] in visitedTargets:
####            visitedRepeats.append(nodeA["idx"])
####
####        if pointA["occupied"] == True:
####            visitedObstacles.append(nodeA["idx"])
####
####    
####    if nodeB is not None:
####        if nodeB["idx"] not in visitedTargets and pointB["target"] == True and pointB["occupied"] == False:
####            visitedTargets.append(nodeB["idx"])
####
####        if nodeB["idx"] in visitedTargets:
####            visitedRepeats.append(nodeB["idx"])
####
####        if pointB["occupied"] == True:
####            visitedObstacles.append(nodeB["idx"])
####
####    # Distance from last point to goal
####    coordG = idx2rowcol(GOAL_POINTS["stop"], ncols)
####    pointG = coveragePoints[coordG[0]][coordG[1]]
####    distFG = PlannerTools.euclideanDistance(pointB["row"], pointB["col"],
####                                            pointG["row"], pointG["col"])
####    distTotal = distTotal  ### + distFG   # sometimes * 10
####
####    # Quantify number of targets stats
####    numVisitedTargets = len(visitedTargets)
####    numTargets = 0
####    numPossibleObstacles = 0
####    for i in range(len(coveragePoints)):
####        for j in range(len(coveragePoints[0])):
####            if coveragePoints[i][j]["target"]   == True and \
####               coveragePoints[i][j]["occupied"] == False:
####                numTargets = numTargets + 1
####            if coveragePoints[i][j]["occupied"] == True:
####                numPossibleObstacles = numPossibleObstacles + 1
####
####    numSkippedTargets = numTargets - numVisitedTargets
####    numObstacles = len(visitedObstacles)
####    numRepeats  = len(visitedRepeats)
####
####    #print (distTotal, numSkippedTargets, numObstacles, work)
####
####
####    # Normalize fitness
####    maxDistance = DIM * \
####        PlannerTools.euclideanDistance(coveragePoints[0][0]["row"], coveragePoints[0][0]["col"],
####                                        coveragePoints[0][1]["row"], coveragePoints[0][1]["col"])
####    maxObstacles = numPossibleObstacles
####    maxSkipped   = len(COVERAGE_POINTS) * len(COVERAGE_POINTS[0])
####    maxWork      = DIM * 10
####    maxRepeats   = len(COVERAGE_POINTS) * len(COVERAGE_POINTS[0])
####
####    maxFG        = PlannerTools.euclideanDistance(coveragePoints[0][0]["row"], coveragePoints[0][0]["col"],
####                                                  coveragePoints[nrows - 1][ncols - 1]["row"], coveragePoints[nrows - 1][ncols - 1]["col"])
####
####
####    normalDistTotal         = (float(distTotal))         / float(maxDistance)
####    normalNumSkippedTargets = (float(numSkippedTargets)) / float(maxSkipped)
####    normalNumObstacles      = (float(numObstacles))      / float(maxObstacles)
####    normalWork              = (float(work))              / float(maxWork)
####    normalRepeats           = (float(numRepeats))        / float(maxRepeats)
####    normalFG                = (float(distFG))            / float(maxFG)
####
####
####    weightsFG = 1
####
####
####    print ("-", normalDistTotal, normalNumObstacles, normalNumSkippedTargets, normalWork, normalFG)
####    print ("+", normalDistTotal * 10  * weights["distance"], 
####           normalNumObstacles * 10 * weights["obstacle"], 
####           normalNumSkippedTargets * 10 * weights["skipTarget"],
####           normalWork * 10 * weights['work'],
####           normalFG * 10 * weightsFG)
####
####
####    global weights
####    fitness =   (normalDistTotal * 10)         * weights["distance"]    \
####              + (normalNumSkippedTargets * 10) * weights["skipTarget"] \
####              + (normalNumObstacles * 10)      * weights["obstacle"]   \
####              + (normalFG * 10)
####              #+ (normalWork * 10)              * weights["work"]       \
####    #          + normalRepeats           * weights["repeat"]
####
####   # print ("visited", numVisitedTargets, "skipped", numSkippedTargets, "total", numTargets)
####   # print ("distance", distTotal,  "weighted distance", distTotal * weights['distance'])
####
####    #print (distTotal, numObstacles, numSkippedTargets)
####    #print (distTotal * weights["distance"], numObstacles * weights["obstacle"], numSkippedTargets * weights["skipTarget"])
####
####    #fitness =   distTotal         * weights["distance"]   \
####    #          + numSkippedTargets * weights["skipTarget"] \
####    #          + numObstacles      * weights["obstacle"]   #\
####    #          + work              * weights["work"]       \
####    #          + numRepeats           * weights["repeat"]
####
####    # Reducing the information in path to match what outside function expects
####
####    fitness = fitness * 10
####
####    path = [p["idx"] for p in path]
####    return fitness, path





####def pathFitness2(actionSequence, coveragePoints, region):
####
####    # Region boundaries
####    nrows = len(coveragePoints)
####    ncols = len(coveragePoints[0])
####
####    def getMoveIdx(position, orientation, action, ncols):
####        positionCoords = idx2rowcol(position, ncols)
####
####        newCoords = [0, 0]
####        newPosition = position
####
####        work = 0
####        
####        # Using action, select move
####        if   action == 'S':
####            work = 0
####            move = orientation
####        elif action == 'L':
####            work = 50
####            if   orientation == 'U':
####                move = 'L'
####            elif orientation == 'L':
####                move = 'D'
####            elif orientation == 'D':
####                move = 'R'
####            elif orientation == 'R':
####                move = 'U'
####        elif action == 'R':
####            work = 50
####            if   orientation == 'U':
####                move = 'R'
####            elif orientation == 'L':
####                move = 'U'
####            elif orientation == 'D':
####                move = 'L'
####            elif orientation == 'R':
####                move = 'D'
####        elif action == 'U':
####            work = 100
####            if   orientation == 'U':
####                move = 'D'
####            elif orientation == 'L':
####                move = 'R'
####            elif orientation == 'D':
####                move = 'U'
####            elif orientation == 'R':
####                move = 'L'
####
####        # Execute move
####        if   move == 'U':
####            newCoords[0] = positionCoords[0] - 1
####            newCoords[1] = positionCoords[1]
####        elif move == 'D':
####            newCoords[0] = positionCoords[0] + 1
####            newCoords[1] = positionCoords[1]
####        elif move == 'L':
####            newCoords[0] = positionCoords[0]
####            newCoords[1] = positionCoords[1] - 1
####        elif move == 'R':
####            newCoords[0] = positionCoords[0]
####            newCoords[1] = positionCoords[1] + 1
####
####        newPosition = rowcol2idx(newCoords[0], newCoords[1], ncols)
####        return newPosition, move, work
#### 
####   
####    # Initialize previous position
####    prev = { "act"         : 'N',
####             "idx"         : GOAL_POINTS["start"],
####             "orientation" : 'L',
####    }
####    curr = { "act"         : None,
####             "idx"         : None,
####             "orientation" : None,
####    }
####
####    path = [prev["idx"]]
####
####    visitedTargets   = []
####    visistedRepeats  = []
####    visitedObstacles = []
####    visitedOutbounds = []
####    work = 0
#### 
####    seqlen = float (len(actionSequence))
####
####    for action in actionSequence:
####        global ACTIONS
####        curr["act"] = ACTIONS[int(action)]
####
####        # See where you would end up
####        curr["idx"], curr["orientation"], deltaWork = getMoveIdx(prev["idx"], 
####                                                          prev["orientation"], 
####                                                          curr["act"], ncols)
####        # Accumulate work
####        work = work + deltaWork
#### 
####        # See what the region value is
####        coords = idx2rowcol(curr["idx"], ncols)
####
####        # If no action, go onto next iteration
####        if curr["act"] == 'N':
####            skips.append(curr["idx"])
####            continue
####     
####        if coords[0] <= 0 or coords[0] >= nrows or \
####           coords[1] <= 0 or coords[1] >= ncols:
####            visitedOutbounds.append(curr["idx"])
####            visitedObstacles.append(curr["idx"])
####            continue
####
####        if coveragePoints[coords[0]][coords[1]]["target"] == True:
####            if curr["idx"] not in visitedTargets:
####                visitedTargets.append(curr["idx"])
####            else:
####                visitedRepeats.append(curr["idx"])
####        
####
####        path.append(curr["idx"])
####
####        if coveragePoints[coords[0]][coords[1]]["occupied"] == True:
####            visitedObstacles.append(curr["idx"])
####
####
####        prev["idx"]       = curr["idx"]
####        prev["act"]       = curr["act"]
####        prev["orientation"] = curr["orientation"]
####
####    
####    rewardCoverage   = float(len(visitedTargets))    #/ seqlen
####    penaltyObstacles = float(len(visitedObstacles))  #/ seqlen
####    penaltyWork      = work
####    penaltyRepeats   = float(len(visitedRepeats))
####
####    penaltyMissed    = float(len(coveragePoints) * len(coveragePoints)) - rewardCoverage
####
####    weight = { "coverage"  : -500,
####               "obstacles" : 1000,
####               "work"      : 100,
####               "missed"    : 100,
####               "repeats"   : 100,
####              }
####
####    fitness = \
####              weight["coverage"]  * rewardCoverage + \
####              weight["obstacles"] * penaltyObstacles + \
####              weight["work"]      * penaltyWork + \
####              weight["repeats"]   * penaltyRepeats
####              #weight["missed"]    * penaltyMissed
####
####    #print (rewardCoverage, penaltyObstacles, penaltyWork)
####    #print (weight["coverage"]  * rewardCoverage, 
####    #       weight["obstacles"] * penaltyObstacles, 
####    #       weight["work"]      * penaltyWork)
####
####    print (path)
####    return fitness, rewardCoverage, penaltyObstacles, penaltyWork, path



#####def pathFitness2(actionSequence, coveragePoints, region):
#####
#####    def getMoveIdx(position, action, ncols):
#####        positionCoords = idx2rowcol(position, ncols)
#####
#####        newCoords = [0, 0]
#####        newPosition = position
#####        reldist = 0
#####        if   action == 'U':
#####            newCoords[0] = positionCoords[0] - 1
#####            newCoords[1] = positionCoords[1]
#####            reldist = 1
#####        elif action == 'D':
#####            newCoords[0] = positionCoords[0] + 1
#####            newCoords[1] = positionCoords[1]
#####            reldist = 1
#####        elif action == 'L':
#####            newCoords[0] = positionCoords[0]
#####            newCoords[1] = positionCoords[1] - 1
#####            reldist = 1
#####        elif action == 'R':
#####            newCoords[0] = positionCoords[0]
#####            newCoords[1] = positionCoords[1] + 1
#####            reldist = 1
#####        elif action == 'A':
#####            newCoords[0] = positionCoords[0] - 1
#####            newCoords[1] = positionCoords[1] - 1
#####            reldist = 2
#####        elif action == 'B':
#####            newCoords[0] = positionCoords[0] - 1
#####            newCoords[1] = positionCoords[1] + 1
#####            reldist = 2
#####        elif action == 'C':
#####            newCoords[0] = positionCoords[0] + 1
#####            newCoords[1] = positionCoords[1] - 1
#####            reldist = 2
#####        elif action == 'D':
#####            newCoords[0] = positionCoords[0] + 1
#####            newCoords[1] = positionCoords[1] + 1
#####            reldist = 2
#####        newPosition = rowcol2idx(newCoords[0], newCoords[1], ncols)
#####        return newPosition, reldist
#####
#####    # Region boundaries
#####    nrows = len(coveragePoints)
#####    ncols = len(coveragePoints[0])
#####
#####    # Initialize previous position
#####    prev = { "act": 'N',
#####             "idx": GOAL_POINTS["start"],
#####    }
#####    curr = { "act": None,
#####             "idx": None,
#####    }
#####
#####    path = [prev["idx"]]
#####
#####    visitedTargets   = []
#####    visitedObstacles = []
#####    visitedOutbounds = []
#####    visitedRepeats   = []
#####    skips            = []
#####    progressions     = []
#####    trueProgressions = []
#####    undos            = []
#####    dist = 0
#####
#####    seqlen = float (len(actionSequence))
#####
#####    for action in actionSequence:
#####        global ACTIONS
#####        curr["act"] = ACTIONS[int(action)]
#####
#####        # See where you would end up
#####        curr["idx"], deltaDist = getMoveIdx(prev["idx"], curr["act"], ncols)
#####
#####        # See what the region value is
#####        coords = idx2rowcol(curr["idx"], ncols)
#####
#####        # If no action, go onto next iteration
#####        if curr["act"] == 'N':
#####            skips.append(curr["idx"])
#####            continue
#####     
#####        if coords[0] <= 0 or coords[0] >= nrows or \
#####           coords[1] <= 0 or coords[1] >= ncols:
#####            visitedOutbounds.append(curr["idx"])
#####            continue
#####
#####        path.append(curr["idx"])
#####
##### 
#####        if curr["act"] == prev["act"] and curr["idx"] not in visitedTargets:
#####            trueProgressions.append(curr["idx"])       
#####
#####        if coveragePoints[coords[0]][coords[1]]["target"] == True:
#####            if curr["idx"] in visitedTargets:
#####                visitedRepeats.append(curr["idx"])
#####            else:
#####                visitedTargets.append(curr["idx"])
#####        
#####        if coveragePoints[coords[0]][coords[1]]["occupied"] == True:
#####            visitedObstacles.append(curr["idx"])
#####
#####        if curr["act"] == prev["act"]:
#####            progressions.append(curr["idx"])
#####
#####
#####        if (curr["act"] == "L" and prev["act"] == "R") or \
#####           (curr["act"] == "R" and prev["act"] == "L") or \
#####           (curr["act"] == "U" and prev["act"] == "D") or \
#####           (curr["act"] == "D" and prev["act"] == "U"):
#####            undos.append(curr["idx"])
#####
#####
#####        dist = dist + deltaDist
#####
#####        prev["idx"] = curr["idx"]
#####        prev["act"] = curr["act"]
#####
#####    
#####    rewardCoverage   = float(len(visitedTargets))    #/ seqlen
#####    rewardProgress   = float(len(progressions))      #/ seqlen
#####    rewardTrueProg   = float(len(trueProgressions))
#####    rewardSkips      = float(len(skips))             #/ seqlen
#####    penaltyDist      = float(dist)                   #/ seqlen
#####    penaltyRepeat    = float(len(visitedRepeats))    #/ seqlen
#####    penaltyObstacles = float(len(visitedObstacles))  #/ seqlen
#####    penaltyOutbounds = float(len(visitedOutbounds))  #/ seqlen
#####    penaltyUndos     = float(len(undos))
#####
#####    fitness = \
#####              -500     *    rewardCoverage + \
#####              1000     *    penaltyObstacles #+ \
#####              #500     *    penaltyRepeat + \
#####              #-100     *    rewardTrueProg
#####              #-50      *    rewardSkips + \
#####              #50       *    penaltyUndos
#####              
#####
#####    return fitness, rewardCoverage, penaltyDist, penaltyObstacles, path



#####class coveragePathProblem2(base):
#####    def __init__(self, dim = 10):
#####        # Set problem dimensions based on number of points
#####        #dim = int (DIM + 0.2 * DIM)
#####        dim = DIM
#####
#####
#####        # Uncomment to enforce integer
#####        #super(coveragePathProblem2, self).__init__(dim, dim)
#####
#####        super(coveragePathProblem2, self).__init__(dim)
#####
#####        # bounds based on number of possible actions
#####        global ACTIONS
#####        self.set_bounds(0, len(ACTIONS) - 1)
#####
#####    def _objfun_impl(self, x):
#####        
#####        actionSeq = list(x)
#####        f = pathFitness2(actionSeq, COVERAGE_POINTS, REGION)[0]
#####        return (f, )



# def... getCoveragePath    <-- Had this inside, among more code

    #########numPoints = (len(COVERAGE_POINTS) *   # Number of rows
    #########             len(COVERAGE_POINTS[0])) # Number of cols

    ########## Set problem dimension
    ########## "Heuristic": Should not need more moves than
    ########## 2 * number of points
    #########global DIM
    #########DIM  = int (numPoints)

    #########prob = coveragePathProblem2()
    #########algo = algorithm.pso(gen = OPTIMIZE_PARAMS["generations"])
    #########isl = island(algo, prob, OPTIMIZE_PARAMS["poolsize"])
    #########
    #########hasCollisions = True
    #########trials = 0
    #########while (hasCollisions == True):
    #########    trials = trials + 1
    #########    isl.evolve(1)
    #########    isl.join()
    #########    f, path = pathFitness2(isl.population.champion.x, 
    #########                  COVERAGE_POINTS, REGION)
    #########    #if noccupied == 0:
    #########    hasCollisions = False



    #########actionSequence = list(isl.population.champion.x)

    #########coveragePath = { "path"           : path,
    #########                 "actionSequence" : actionSequence,
    #########                 "fitness"        : isl.population.champion.f,
    #########                 "trials"         : trials,
    #########}
