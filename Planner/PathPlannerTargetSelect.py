#!/usr/bin/python
'''
PathPlannerTargetSelect
Author: Evan Krell

Given a list of targets in an environment, 
and their priority scores,
select a sequence of targets that maximizes target gain,
minimizes distance and energy, and ensures their is time to reach destination.

Note that this is a preliminary step for a full path planner.
It does not create intermediary waypoints for travelling between targets.
It just chooses a sequence of targets that can be visited safely. 
'''

# Dependecies, external
import pandas as pd
import numpy as np
from osgeo import gdal
from PyGMO.problem import base, tsp
from PyGMO import algorithm, island, problem, population
import math
import sys

# Dependencies, part of mission planner
import GridUtils as GridUtil
import RasterSetInterface as rsi
import PlannerTools as PlannerTools

TSP_DATA = { "targetGraph" : None, 
             "dim"         : 0, 
}

def buildTargetGraph(targetsTable, startpoint, stopTable, environment):

    def edgeFitness (edge):
        fitness = (
            edge["distance"] + \
            pow (edge["terrain"], 2) + \
            edge["current"]
        )
        return fitness


    edgeDetail  = [[None for i in targetsTable.index.values] for j in targetsTable.index.values]
    edgeWeight  = [[None for i in targetsTable.index.values] for j in targetsTable.index.values]
    nodeDetail  = [ None for i in targetsTable.index.values]
    nodeWeight  = [ None for i in targetsTable.index.values]
    comboWeight = [[None for i in targetsTable.index.values] for j in targetsTable.index.values]
    
    endPointDetail   = [[None for i in stopTable.index.values] for j in targetsTable.index.values]
    endPointWeight   = [[None for i in stopTable.index.values] for j in targetsTable.index.values]
    startPointDetail = [ None for i in targetsTable.index.values]
    startPointWeight = [ None for i in targetsTable.index.values]


    # Get point information for startpoint
    pointArchive_s = GridUtil.getArchiveByWorld(startpoint["Lat"], startpoint["Lon"],
                         environment["region"]["grid"],
                         environment["region"]["raster"].GetGeoTransform())


    i = 0
    j = 0
    for index_i, row_i in targetsTable.iterrows():
        
        pointArchive_i = GridUtil.getArchiveByWorld(row_i["Lat"], row_i["Lon"],
                             environment["region"]["grid"],
                             environment["region"]["raster"].GetGeoTransform())


        ########################
        # Get node weight of i #
        ########################
        targetCoveragePolygon = PlannerTools.target2polygon(pointArchive_i["row"], pointArchive_i["col"],
                                    row_i["Width"], row_i["Length"])

        nodeCurrentScore, nodeCurrentSummary = PlannerTools.estimateCurrentComplexityOfRegion(targetCoveragePolygon, 
                                                   environment["currents"]["magnitude"]["raster"],
                                                   environment["region"]["extent"]["rows"],
                                                   environment["region"]["extent"]["cols"])

        nodeDetail[i] = { "nodeCurrentSummary" : nodeCurrentSummary,
                          "nodeCurrentScore"   : nodeCurrentScore,
                          "nodeArea"           : row_i["Length"] * row_i["Width"], #targetCoveragePolygon.area,
        } 

        #nodeWeight[i] = (nodeDetail[i]["nodeCurrentScore"] *  
        #                 nodeDetail[i]["nodeArea"]
        #)
        nodeWeight[i] = (nodeDetail[i]["nodeArea"])

        #########################
        # Get weight from start #
        #########################

        currentScore, currentSummary = PlannerTools.estimateCurrentComplexityBetweenPoints(pointArchive_i["row"], pointArchive_i["col"],
                                     pointArchive_s["row"], pointArchive_s["col"],
                                     environment["currents"]["magnitude"]["raster"],
                                     environment["region"]["extent"]["rows"],
                                     environment["region"]["extent"]["cols"])

        startPointDetail[i] = { \
                    "distance" : PlannerTools.euclideanDistance(pointArchive_i["row"], pointArchive_i["col"],
                                     pointArchive_s["row"], pointArchive_s["col"]),
                    "terrain"  : PlannerTools.countLinePenalties(pointArchive_i["row"], pointArchive_i["col"],
                                     pointArchive_s["row"], pointArchive_s["col"],
                                     environment["region"]["grid"],
                                     environment["region"]["extent"]["rows"],
                                     environment["region"]["extent"]["cols"],
                                     environment["plannerTargetSelect"]["obstacleFlag"]),
                    "current"  : currentScore
        }
        startPointWeight[i] = edgeFitness(startPointDetail[i])

        ##########################
        # Get edge weights from i#
        ##########################
        for index_j, row_j in targetsTable.iterrows():
         
            pointArchive_j = GridUtil.getArchiveByWorld(row_j["Lat"], row_j["Lon"],
                                     environment["region"]["grid"],
                                     environment["region"]["raster"].GetGeoTransform())

            if (i == j):
                edgeDetail[i][j] = { \
                    "distance" : 0,
                    "terrain"  : 0,
                    "current"  : 0,
                }
            else:
                currentScore, currentSummary = PlannerTools.estimateCurrentComplexityBetweenPoints(pointArchive_i["row"], pointArchive_i["col"],
                                     pointArchive_j["row"], pointArchive_j["col"],
                                     environment["currents"]["magnitude"]["raster"],
                                     environment["region"]["extent"]["rows"],
                                     environment["region"]["extent"]["cols"])

                edgeDetail[i][j] = { \
                    "distance" : PlannerTools.euclideanDistance(pointArchive_i["row"], pointArchive_i["col"],
                                     pointArchive_j["row"], pointArchive_j["col"]),
                    "terrain"  : PlannerTools.countLinePenalties(pointArchive_i["row"], pointArchive_i["col"],
                                     pointArchive_j["row"], pointArchive_j["col"],
                                     environment["region"]["grid"],
                                     environment["region"]["extent"]["rows"],
                                     environment["region"]["extent"]["cols"],
                                     environment["plannerTargetSelect"]["obstacleFlag"]),
                    "current"  : currentScore
                }
            edgeWeight[i][j] = edgeFitness(edgeDetail[i][j])
  
            j = j + 1

        ##############################
        # Get end edge weights from i#
        ##############################
        e = 0
        for index_e, row_e in stopTable.iterrows():
            pointArchive_e = GridUtil.getArchiveByWorld(row_e["Lat"], row_e["Lon"],
                                     environment["region"]["grid"],
                                     environment["region"]["raster"].GetGeoTransform())

            currentScore, currentSummary = PlannerTools.estimateCurrentComplexityBetweenPoints(pointArchive_i["row"], pointArchive_i["col"],
                                     pointArchive_e["row"], pointArchive_e["col"],
                                     environment["currents"]["magnitude"]["raster"],
                                     environment["region"]["extent"]["rows"],
                                     environment["region"]["extent"]["cols"])

            endPointDetail[i][e] = { \
                    "distance" : PlannerTools.euclideanDistance(pointArchive_i["row"], pointArchive_i["col"],
                                     pointArchive_e["row"], pointArchive_e["col"]),
                    "terrain"  : PlannerTools.countLinePenalties(pointArchive_i["row"], pointArchive_i["col"],
                                     pointArchive_e["row"], pointArchive_e["col"],
                                     environment["region"]["grid"],
                                     environment["region"]["extent"]["rows"],
                                     environment["region"]["extent"]["cols"],
                                     environment["plannerTargetSelect"]["obstacleFlag"]),
                    "current"  : currentScore
            }
            endPointWeight[i][e] = edgeFitness(endPointDetail[i][e])
            e = e + 1

        # Index update
        i = i + 1
        j = 0
        e = 0


    # Loop through each edge again, to combine node weights
    # into the edge weights to support TSP solver
    i = 0
    j = 0
    for index_i, row_i in targetsTable.iterrows():
        for index_j, row_j in targetsTable.iterrows():
            if (i == j):
                comboWeight[i][j] = 0
            else:
                comboWeight[i][j] = edgeWeight[i][j] + nodeWeight[j]
            j = j + 1
        i = i + 1
        j = 0

    targetGraph = { "edgeDetail"       : edgeDetail,
                    "edgeWeight"       : edgeWeight,
                    "nodeWeight"       : nodeWeight,
                    "comboWeight"      : comboWeight,
                    "endPointDetail"   : endPointDetail,
                    "endPointWeight"   : endPointWeight,
                    "startPointDetail" : startPointDetail,
                    "startPointWeight" : startPointWeight,
    }

    return targetGraph


def fitness(path):
    path = [int(p) for p in path]
    
    accumFitness = 0

    global TSP_DATA
    # Note: targets are rows and start/end are columns

    startIdx = path[0]              # Start 
    endIdx   = path[len(path) - 1]  # End
    tFIdx    = path[1]              # First target
    tLIdx    = path[len(path) - 2]  # Last target

    # Evaluate fitness from start to first target
    start2F  = TSP_DATA["targetGraph"]["startPointWeight"][tFIdx] #[startIdx]
    # Evaluate fitness from last target to end
    L2end = TSP_DATA["targetGraph"]["endPointWeight"][tLIdx][endIdx]

    # Evaluate target_i -> target_j segments
    accumFitness = accumFitness + start2F + L2end
    for x in range(1, len(path) - 2):
        i = path[x]
        j = path[x + 1]
        accumFitness = accumFitness + TSP_DATA["targetGraph"]["edgeWeight"][i][j]

    # Enforce that targets are unique
    targets = path[1:len(path) - 1]
    if len(targets) > len(set(targets)):
        t = accumFitness
        accumFitness = accumFitness * (10000000000000000000000000000000000000000)

    return accumFitness
 


class targetSelectionProblem(base):
    def __init__(self, dim = 0):
        # Set problem dimensions based on number of 
        # targets plus start and end
        global TSP_DATA
        dim = TSP_DATA["dim"]

        super(targetSelectionProblem, self).__init__(dim, dim)

        # Set bounds based on number of targets and 
        upperBounds    = [0] * dim
        for i in range(1, dim):
            upperBounds[i] = dim - 3
        upperBounds[len(upperBounds) - 1] = len (TSP_DATA["targetGraph"]["endPointWeight"][0]) - 1
        upperBounds[0] = 1
        self.set_bounds([0] * dim, upperBounds)

    def _objfun_impl(self, x):
        f = fitness(x)
        return (f, )



def driver(targetsTable, startPoint, stopPoints, environment):
    import copy
    
    # Generate graph of targets for TSP solver
    global TSP_DATA
    TSP_DATA["targetGraph"] = buildTargetGraph(targetsTable, startPoint, stopPoints, environment)
    TSP_DATA["dim"]         = len(TSP_DATA["targetGraph"]["nodeWeight"]) + 2

    # Making sure seeing correct region...
    np.savetxt("gridTest.txt", environment["region"]["grid"], fmt="%i")
    np.savetxt("magTest.txt",  
          environment["currents"]["magnitude"]["raster"].GetRasterBand(3).ReadAsArray()[40:60,40:60],
          fmt="%g.2")


    # Solve using PyGMO
    prob = targetSelectionProblem()
    algo = algorithm.sga(gen = environment["plannerTargetSelect"]["generations"])
    isl  = island(algo, prob,  environment["plannerTargetSelect"]["individuals"])
    isl.evolve(1)
    isl.join()

    # Check solution
    #print(isl.population.champion.x)
    #print(isl.population.champion.f)

    selectedTargetsSolution = { "solution"       : isl.population.champion,
                                "targetSequence" : [ int (s) + 1 for s in \
                                                      isl.population.champion.x[1:len(isl.population.champion.x)-1] ]
    }
    
    return selectedTargetsSolution, TSP_DATA["targetGraph"]

