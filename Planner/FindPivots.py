import dill as pickle
import itertools

def parseOptions():
    from optparse import OptionParser

    # Define options
    parser = OptionParser()
    parser.add_option("-p", "--pickle", dest="pickle", metavar="PICKLE",
        help="Path to pickled results file")

    # Get options
    (options, args) = parser.parse_args()

    # Verify correct options
    if options.pickle is None:
        (options, args) = None, None

    return options, args


def findPivots(pathLog, durationMax, workMax):

    # Pivots, as found, will be added to this
    # Note: A list of lists! Multiple segments can work together as pivot
    pivots = []

    # Check if pivots are needed
    durationTotal = pathLog["DURATION"].sum()
    durationDiff  = durationMax - durationTotal
    workTotal     = pathLog["WORK"].sum()
    workDiff      = workMax     - workTotal

    print(durationTotal, workTotal)
    print(durationDiff, workDiff)

    if durationDiff >= 0 and workDiff >= 0:
        return pivots # Passes constraints, return no pivots

    # Failed check --> need to find pivots
    segments = pathLog["SEGMENT"]
    maxPivots = len(segments)
    
    for numPivots in range(1, maxPivots + 1):

        # Find all candidate pivots of length numPivots
        # Find non-goto 
        segs2eval_nongoto = list(itertools.combinations(segments[1::2], numPivots))


        # Add connected gotos 
        segs2eval = []        
        for segs in segs2eval_nongoto:
            extendedsegs = list(segs)
            for s in segs:
                extendedsegs.append(s + 1)
                extendedsegs.append(s - 1)
            extendedsegs = list(set(extendedsegs))    
            segs2eval.append(extendedsegs)
      
        # Evaluate candidates
        for segs in segs2eval:

            # Check constraints    
            durationTotal = pathLog[~pathLog["SEGMENT"].isin(segs)]["DURATION"].sum()
            durationDiff  = durationMax - durationTotal
            workTotal     = pathLog[~pathLog["SEGMENT"].isin(segs)]["WORK"].sum()
            workDiff      = workMax     - workTotal
   
            if durationDiff > 0 and workDiff > 0:
                rewardTotal = pathLog[~pathLog["SEGMENT"].isin(segs)]["REWARD"].sum()
                pivots.append({ "segments"     : segs,
                                "durationLeft" : durationTotal,
                                "workLeft"     : workTotal,
                                "rewardLeft"   : rewardTotal,
                             })
    return pivots

def reducePivots(pivots):
    
    # Indices of pivots to remove
    remPivotIDXs  = []
    # Indiced of pivots to keep
    reducedPivots = [] 

    # Check for redundent pivots
    for pai in range(len(pivots)):
        spa = set(pivots[pai]["segments"])
        for pbi in range(len(pivots)):
            spb = set(pivots[pbi]["segments"])
            if spa < spb:
                remPivotIDXs.append(pbi)

    # Remove the found redudencies
    reducedPivotIDXs = [pi for pi in range(len(pivots)) if pi not in remPivotIDXs]
    for rpi in reducedPivotIDXs:
        reducedPivots.append(pivots[rpi])
    
    return reducedPivots

def rankPivots(pivots):
    # Order by reward
    rankedPivots = list(reversed(sorted(pivots, key=lambda k: k['rewardLeft'])))
    return rankedPivots

def findPivotsDriver(plannerSequencePandas, durationMax, workMax):
    pivots        = findPivots(plannerSequencePandas, durationMax, workMax)
    reducedPivots = reducePivots(pivots)
    rankedPivots  = rankPivots(reducedPivots) 
    return rankedPivots



## Parse options
#(options, args) = parseOptions()
#if options == None:
#    print ("Incorrect options")

## Load results pickle
#state = pickle.load(open(options.pickle, "rb"))

## Test
#durationMax = 10
#workMax     = 62523000

#plannerSequencePandas = state["plannerSummaryPandas"]

#durationTotal = plannerSequencePandas["DURATION"].sum()
#workTotal     = plannerSequencePandas["WORK"].sum()

#print("durationMax",   durationMax,   "workMax",   workMax)
#print("durationTotal", durationTotal, "workTotal", workTotal)
#print("Num pivots:", len(pivots))
#print("Num reduced pivots:", len(reducedPivots))
#print (rankedPivots)

