
import GridUtils as GridUtil
import RasterSetInterface as rsi
import time
from math import acos, cos, sin, ceil
import pandas as pd
import yaml as yaml
import numpy as np
import osr
import paramiko as paramiko
from scp import SCPClient
from datetime import date, datetime, timedelta
import subprocess
import os
import sys
import dill as pickle
import itertools
from collections import OrderedDict

lib_path = os.path.abspath(os.path.join(__file__, '..', '..', 'AnalystTools'))
sys.path.append(lib_path)
import TargetFitness as TargetFitness # needs to 'AnalystTools'
import PlannerTools  as PlannerTools
import FindPivots    as FindPivots
import PlannerVis    as PlannerVis

# Import path planning modules
import PathPlannerTargetSelect as PPT
import PathPlannerGoto         as PPG
import PathPlannerCoverage     as PPC




def parseOptions():
    from optparse import OptionParser

    # Define options
    parser = OptionParser()
    parser.add_option("-p", "--pickle", dest="pickle", metavar="PICKLE",
        help="Path to pickle file (will create if not present).")
    parser.add_option("-c", "--config", dest="config", metavar="CONFIG",
        help="Path to configuration file")
    parser.add_option("-t", "--source", dest="source", metavar="SOURCE",
        help="Pickle source (MISSION, GOTO, COVERAGE)")

    # Get options
    (options, args) = parser.parse_args()
    
    # Verify correct options
    if options.pickle is None:
        (options, args) = None, None

    return options, args





# MAIN

# Parse options
(options, args) = parseOptions()
if options is None:
    exit()

# Load JSON
json_data = pickle.load(open(options.pickle, 'rb'))


# Load data
environment     = PlannerTools.initializeByFile(options.config, haveCurrents = True)
targetsTable    = PlannerTools.initTargetsTableByFile(environment["targets"]["table"])
safepointsTable = PlannerTools.initSafepointsTableByFile(environment["vehicle"]["safepointsFile"])

# Hacky rename since I don't like the original name, but need to maintain compatability
environment["logbook"] = environment["targets"]["weights"]

startPoint = { "Lat" : environment["vehicle"]["startCoords"][1],
               "Lon" : environment["vehicle"]["startCoords"][0],
}

# Print maps
#currentsMap = PlannerVis.makeWaterCurrentMap(environment["region"]["raster"],
#    environment["currents"]['magnitude']['raster'],
#    environment["currents"]['direction']['raster'],
#    10,
#    environment["region"]["file"],
#    "test-7.png", 7)
#exit()

# If no source specified, print raw json
if options.source is None:
    print (json_data)


if options.source == "MISSION":
    print(json_data)

if options.source == "GOTO":
    for gotoResult in json_data:
        print(gotoResult["solutionPath"])
        print("Duration", gotoResult["solutionPath"]["duration"]["total"])
        print("Distance", gotoResult["solutionPath"]["distance"]["total"])
        print("Work",     gotoResult["solutionPath"]["work"]["total"])
        print("Reward",   gotoResult["solutionPath"]["reward"]["total"])

        print("{},{},{},{}".format(gotoResult["solutionPath"]["distance"]["total"],
                                   gotoResult["solutionPath"]["duration"]["total"],
                                   gotoResult["solutionPath"]["work"]["total"],
                                   gotoResult["solutionPath"]["reward"]["total"]))
                                   
        print(gotoReults.keys())


        #run = 1
        #for i, l in enumerate(gotoResult["solutionPath"]["optlog"]):
        #    r = "B,{},{},{}".format(run, i, l)
        #    print(r)


if options.source == "COVERAGE":

   for coverageResult in json_data:
       if coverageResult["path"]["solver"] == "LAWNMOWER":
           print("Trials",   coverageResult["path"]["trials"])

           gridPath = PPC.convertTargetPath2WorldPath(coverageResult["points"], 
                                                        coverageResult["path"])

           gridPathLong, gStart, gTarget = PlannerTools.getPathLongForm(gridPath)

           cHeading, cDistance, cDuration, cWork, cCoord, cReward = \
                PlannerTools.statPath(environment, gridPathLong, gStart, gTarget)

           print("Duration", cDuration["total"])
           print("Distance", cDistance["total"])
           print("Work",     cWork["total"])
           print("Reward",   cReward["total"])


#           for g in gridPath:
#               print ("(row, col)", int(g["row"]), int(g["col"]))




