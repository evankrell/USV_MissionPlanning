#!/usr/bin/python

'''
PathPlannerTargetSelect
Author: Evan Krell

Given a list of targets in an environment, 
and their priority scores,
select a sequence of targets that maximizes target gain,
minimizes distance and energy, and ensures their is time to reach destination.

Note that this is a preliminary step for a full path planner.
It does not create intermediary waypoints for travelling between targets.
It just chooses a sequence of targets that can be visited safely. 
'''

# Dependecies, external
import pandas as pd
import numpy as np
from osgeo import gdal
from PyGMO.problem import base, tsp
from PyGMO import algorithm, island, problem, population
import math
import sys
import itertools

# Dependencies, part of mission planner
import GridUtils as GridUtil
import RasterSetInterface as rsi
import PlannerTools as PlannerTools



def buildTargetGraph(targetsTable, startpoint, stopTable, environment):

    def edgeFitness (edge):
        fitness = (
                    edge["distance"]         + \
                    pow (edge["terrain"], 2) + \
                    edge["current"]
        )
        return fitness

    # Initialize weights for all nodes, edges, and combined node+edge pairs
    edgeDetail  = [[None for i in targetsTable.index.values] for j in targetsTable.index.values]
    edgeWeight  = [[None for i in targetsTable.index.values] for j in targetsTable.index.values]
    nodeDetail  = [ None for i in targetsTable.index.values]
    nodeWeight  = [ None for i in targetsTable.index.values]
    comboWeight = [[None for i in targetsTable.index.values] for j in targetsTable.index.values]
    
    endPointDetail   = [[None for i in stopTable.index.values] for j in targetsTable.index.values]
    endPointWeight   = [[None for i in stopTable.index.values] for j in targetsTable.index.values]
    startPointDetail = [ None for i in targetsTable.index.values]
    startPointWeight = [ None for i in targetsTable.index.values]


    # Get point information for startpoint
    pointArchive_s = GridUtil.getArchiveByWorld(startpoint["Lat"], startpoint["Lon"],
                                                       environment["region"]["grid"],
                                   environment["region"]["raster"].GetGeoTransform())
    # Consider all segments
    i = 0
    j = 0
    for index_i, row_i in targetsTable.iterrows():
        
        # Get ith point info
        pointArchive_i = GridUtil.getArchiveByWorld(row_i["Lat"], row_i["Lon"],
                             environment["region"]["grid"],
                             environment["region"]["raster"].GetGeoTransform())

        ########################
        # Get node weight of i #
        ########################

        # Get polygon around target
        targetCoveragePolygon = PlannerTools.target2polygon(pointArchive_i["row"], 
                           pointArchive_i["col"], row_i["Width"], row_i["Length"])

        # Get estimate of water current complexity within region
        nodeCurrentScore, nodeCurrentSummary = \
            PlannerTools.estimateCurrentComplexityOfRegion(targetCoveragePolygon, 
                                  environment["currents"]["magnitude"]["raster"],
                                         environment["region"]["extent"]["rows"],
                                         environment["region"]["extent"]["cols"])

        # Set information on node
        nodeDetail[i] = { "nodeCurrentSummary" : nodeCurrentSummary,
                          "nodeCurrentScore"   : nodeCurrentScore,
                          "nodeArea"           : row_i["Length"] * row_i["Width"], #targetCoveragePolygon.area,
        } 

        # Combine informaion into single score for node
        nodeWeight[i] = (nodeDetail[i]["nodeCurrentScore"] *  
                         nodeDetail[i]["nodeArea"])

        #########################
        # Get weight from start #
        #########################

        # Get estimate of water current complexity between points
        currentScore, currentSummary = \
            PlannerTools.estimateCurrentComplexityBetweenPoints(pointArchive_i["row"], 
                                                                pointArchive_i["col"],
                                                                pointArchive_s["row"], 
                                                                pointArchive_s["col"],
                                       environment["currents"]["magnitude"]["raster"],
                                              environment["region"]["extent"]["rows"],
                                              environment["region"]["extent"]["cols"])

        # Set information on edge
        startPointDetail[i] = { \
                    "distance" : PlannerTools.euclideanDistance(pointArchive_i["row"], 
                                                                pointArchive_i["col"],
                                                                pointArchive_s["row"], 
                                                                pointArchive_s["col"]),

                    "terrain"  : PlannerTools.countLinePenalties(pointArchive_i["row"], 
                                                                 pointArchive_i["col"],
                                                                 pointArchive_s["row"], 
                                                                 pointArchive_s["col"],
                                                         environment["region"]["grid"],
                                               environment["region"]["extent"]["rows"],
                                               environment["region"]["extent"]["cols"],
                                    environment["plannerTargetSelect"]["obstacleFlag"]),
                    
                    "current"  : currentScore
        }

        # Combine information into single score for edge
        startPointWeight[i] = edgeFitness(startPointDetail[i])

        ##########################
        # Get edge weights from i#
        ##########################

        for index_j, row_j in targetsTable.iterrows():

            # Get ith point info
            pointArchive_j = GridUtil.getArchiveByWorld(row_j["Lat"], row_j["Lon"],
                                                     environment["region"]["grid"],
                                 environment["region"]["raster"].GetGeoTransform())

            if (i == j):
                edgeDetail[i][j] = { \
                    "distance" : 0,
                    "terrain"  : 0,
                    "current"  : 0,
                }
            else:
                # Get estimate of water current complexity between points
                currentScore, currentSummary = \
                    PlannerTools.estimateCurrentComplexityBetweenPoints(pointArchive_i["row"], 
                                                                        pointArchive_i["col"],
                                                                        pointArchive_j["row"], 
                                                                        pointArchive_j["col"],
                                               environment["currents"]["magnitude"]["raster"],
                                                      environment["region"]["extent"]["rows"],
                                                      environment["region"]["extent"]["cols"])

                # Set information on edge
                edgeDetail[i][j] = { \
                    "distance" : PlannerTools.euclideanDistance(pointArchive_i["row"],
                                                                pointArchive_i["col"],
                                                                pointArchive_j["row"],
                                                                pointArchive_j["col"]),

                    "terrain"  : PlannerTools.countLinePenalties(pointArchive_i["row"],
                                                                 pointArchive_i["col"],
                                                                 pointArchive_j["row"],
                                                                 pointArchive_j["col"],
                                                         environment["region"]["grid"],
                                               environment["region"]["extent"]["rows"],
                                               environment["region"]["extent"]["cols"],
                                    environment["plannerTargetSelect"]["obstacleFlag"]),

                    "current"  : currentScore
                }

            # Combine information into single score for edge
            edgeWeight[i][j] = edgeFitness(edgeDetail[i][j])
  
            j = j + 1


        ##############################
        # Get end edge weights from i#
        ##############################

        e = 0
        for index_e, row_e in stopTable.iterrows():

            # Get eth end point info
            pointArchive_e = GridUtil.getArchiveByWorld(row_e["Lat"], 
                                                        row_e["Lon"],
                                       environment["region"]["grid"],
                   environment["region"]["raster"].GetGeoTransform())

            # Get estimate of water current complexity between points
            currentScore, currentSummary = \
                PlannerTools.estimateCurrentComplexityBetweenPoints(pointArchive_i["row"], 
                                                                    pointArchive_i["col"],
                                                                    pointArchive_e["row"], 
                                                                    pointArchive_e["col"],
                                           environment["currents"]["magnitude"]["raster"],
                                                  environment["region"]["extent"]["rows"],
                                                   environment["region"]["extent"]["cols"])

            # Set information on edge
            endPointDetail[i][e] = { \
                    "distance" : PlannerTools.euclideanDistance(pointArchive_i["row"], 
                                                                pointArchive_i["col"],
                                                                pointArchive_e["row"],
                                                                pointArchive_e["col"]),
 
                    "terrain"  : PlannerTools.countLinePenalties(pointArchive_i["row"], 
                                                                 pointArchive_i["col"],
                                                                 pointArchive_e["row"], 
                                                                 pointArchive_e["col"],
                                                         environment["region"]["grid"],
                                               environment["region"]["extent"]["rows"],
                                               environment["region"]["extent"]["cols"],
                                    environment["plannerTargetSelect"]["obstacleFlag"]),

                    "current"  : currentScore
            }

            # Combine information into single score for edge
            endPointWeight[i][e] = edgeFitness(endPointDetail[i][e])
            e = e + 1

        # Index update
        i = i + 1
        j = 0
        e = 0

    # Loop through each edge again, to combine node weights
    # into the edge weights to support TSP solver
    i = 0
    j = 0
    for index_i, row_i in targetsTable.iterrows():
        for index_j, row_j in targetsTable.iterrows():
            if (i == j):
                comboWeight[i][j] = 0
            else:
                comboWeight[i][j] = edgeWeight[i][j] + nodeWeight[j]
            j = j + 1
        i = i + 1
        j = 0

    targetGraph = { "edgeDetail"       : edgeDetail,
                    "edgeWeight"       : edgeWeight,
                    "nodeDetail"       : nodeDetail,
                    "nodeWeight"       : nodeWeight,
                    "comboWeight"      : comboWeight,
                    "endPointDetail"   : endPointDetail,
                    "endPointWeight"   : endPointWeight,
                    "startPointDetail" : startPointDetail,
                    "startPointWeight" : startPointWeight,
    }

    return targetGraph



def getAllPossibleRoutes(targetsTable, startPoint, safepointsTable, environment, region2point):
    
    routeTemplate = { "sequence" : [], "fitness"  : 0,     "distance"  : 0,
                      "work"     : 0,  "duration" : 0,     "reward"    : 0,
                      "stopIdx"  : 0,  "feasible" : False, "seqcoords" : [],
    }

    routes = []

    numStarts  = 1                        # Always have one start
    numTargets = targetsTable.shape[0]    # Number of gotos
    numStops   = safepointsTable.shape[0] # Number of stops
    numNodes   = numStarts + numTargets + numStops

    starts = []
    targets = range(numStarts, numTargets + 1)
    stops   = range(numTargets + 1, numNodes)

    startArchive = GridUtil.getArchiveByWorld(startPoint["Lat"],
                                              startPoint["Lon"],
                                  environment["region"]["grid"],
              environment["region"]["raster"].GetGeoTransform())

    stopArchives = [None for i in stops]
    for i in range(numStops):
        stopArchives[i]  = GridUtil.getArchiveByWorld(safepointsTable.loc[i + 1]["Lat"],
                                                      safepointsTable.loc[i + 1]["Lon"],
                                                          environment["region"]["grid"],
                                      environment["region"]["raster"].GetGeoTransform())

    permTargets = []
    for r in range(numTargets):
        permTargets.extend(itertools.permutations(targets, r + 1))
    for permTarget in permTargets:
        sequencePre = [0] + [p for p in permTarget]
        for s in stops:
            sequence                 = sequencePre + [s]
            route                    = routeTemplate.copy()
            route["sequence"]        = sequence
            route["stopIdx"]         = sequence[-1] - numTargets  # End point index
            route["selectedTargets"] = sequence[1:-1]             # Only targets
            route["seqcoords"]       = PlannerTools.sequence2coords(route["sequence"], 
                                                                         startArchive,
                                                   stopArchives[route["stopIdx"] - 1], 
                                                                         region2point)
            routes.append(route)

    return routes

def evaluateRoute(route, targetGraph):

    heuristicEval = { "fitness"       : 0,
                      "distanceScore" : 0,
                      "currentScore"  : 0,
                      "terrainScore"  : 0,
    }

    # Evaluate start --> target
    i = route["selectedTargets"][0]
    
    heuristicEval["distanceScore"] = heuristicEval["distanceScore"] + \
                 targetGraph["startPointDetail"][i - 1]["distance"]
    heuristicEval ["terrainScore"]  = heuristicEval["terrainScore"] + \
                  targetGraph["startPointDetail"][i - 1]["terrain"]
    heuristicEval ["currentScore"]  = heuristicEval["currentScore"] + \
                  targetGraph["startPointDetail"][i - 1]["current"]

    # Evaluate target --> target
    if (len(route["sequence"])) > 3:
        for i in range(len(route["selectedTargets"]) - 1):
            j = i + 1
            heuristicEval["distanceScore"] = heuristicEval["distanceScore"] + \
                        targetGraph["edgeDetail"][i - 1][j - 1]["distance"] 
            heuristicEval ["terrainScore"]  = heuristicEval["terrainScore"] + \
                         targetGraph["edgeDetail"][i - 1][j - 1]["terrain"]
            heuristicEval ["currentScore"]  = heuristicEval["currentScore"] + \
                         targetGraph["edgeDetail"][i - 1][j - 1]["current"]
       
    # Evaluate target --> end
    e = route["stopIdx"]

    heuristicEval["distanceScore"] = heuristicEval["distanceScore"] + \
            targetGraph["endPointDetail"][i - 1][e - 1]["distance"]
    heuristicEval ["terrainScore"]  = heuristicEval["terrainScore"] + \
             targetGraph["endPointDetail"][i - 1][e - 1]["terrain"]
    heuristicEval ["currentScore"]  = heuristicEval["currentScore"] + \
             targetGraph["endPointDetail"][i - 1][e - 1]["current"]

    for i in route["selectedTargets"]:
        heuristicEval["distanceScore"] = heuristicEval["distanceScore"] + \
           targetGraph["nodeDetail"][i - 1]["nodeArea"]


    heuristicEval["durationScore"] = heuristicEval["distanceScore"] / 500
    return heuristicEval


def driver(targetsTable, startPoint, safepointsTable, environment):

    region2point = PlannerTools.region2pointBuild(startPoint, targetsTable, 
                                              safepointsTable, environment)

    tspData = buildTargetGraph(targetsTable, startPoint, safepointsTable, environment)

    routes = getAllPossibleRoutes(targetsTable, startPoint, safepointsTable, 
                                                  environment, region2point)
    for route in routes:
        route["evaluation"] = evaluateRoute(route, tspData)
        print(route["evaluation"])

# TEMP main
config = "TestDataArchive/config.yaml"
environment = PlannerTools.initializeByFile(config, haveCurrents = True)
targetsTable = PlannerTools.initTargetsTableByFile(environment["targets"]["table"])
safepointsTable = PlannerTools.initSafepointsTableByFile(environment["vehicle"]["safepointsFile"])
environment["logbook"] = environment["targets"]["weights"]
startPoint = { "Lat" : environment["vehicle"]["startCoords"][1],
               "Lon" : environment["vehicle"]["startCoords"][0], }

driver(targetsTable, startPoint, safepointsTable, environment)


