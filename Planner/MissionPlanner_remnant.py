'''
This is deprecated code for calling
the coverage and goto planners,
but separately rather than interleaved.

This means that all coverage paths are planned,
then all Goto paths.

But this is bad because each planner depends
on how much time has passed since all previous segments
'''
#########################
# Path planner coverage #
#########################

# Generate coverage paths of each target
coverageResults = []

for TID in selectedTargets["targetSequence"]:
    start = time.time()
    trow = targetsTable.loc[[TID]]

    # Index of coverage task in seqcoords
    covidx = 1
    for sc in seqcoords[1::2]:
        if sc["startNode"] == TID:
            break
        covidx = covidx + 2
        
    cPoints, cPath = PPC.driver(float(targetsTable.loc[[TID]]["Lat"]), 
                       float(targetsTable.loc[[TID]]["Lon"]), 
                       float(targetsTable.loc[[TID]]["Width"]),
                       float(targetsTable.loc[[TID]]["Length"]), 
                       int(targetsTable.loc[[TID]]["Ypoints"]), 
                       int(targetsTable.loc[[TID]]["Xpoints"]), 
                       int(targetsTable.loc[[TID]]["Margin"]), 
                       int(targetsTable.loc[[TID]]["Roam"]),
                       environment,
                       (seqcoords[covidx]["startRow"], seqcoords[covidx]["startCol"]),
                       (seqcoords[covidx]["stopRow"],  seqcoords[covidx]["stopCol"]))
     
    computeTime = time.time() - start
    timeLog.loc[len(timeLog)] = ["coverage: " + str(TID), 2, computeTime]
    # Convert path such that points are relative to world grid
    gridPath = PPC.convertTargetPath2WorldPath(cPoints, cPath)
    gridPathLong, gStart, gTarget = PlannerTools.getPathLongForm(gridPath)

    # Get path stats
    cHeading, cDistance, cDuration, cWork, cCoord, cReward = \
        PlannerTools.statPath(environment, gridPathLong, gStart, gTarget)

    cStartWorld  = GridUtil.grid2world(gStart["row"], gStart["col"], 
                   environment["region"]["raster"].GetGeoTransform(), 
                   environment["region"]["extent"]["cols"])
    cTargetWorld = GridUtil.grid2world(gTarget["row"], gTarget["col"], 
                   environment["region"]["raster"].GetGeoTransform(),
                   environment["region"]["extent"]["cols"])
    cStartWorld  = { "Lat" : cStartWorld[0],  "Lon" : cStartWorld[1] }
    cTargetWorld = { "Lat" : cTargetWorld[0], "Lon" : cTargetWorld[1] }
    cPathInfo = { "path"       : gridPathLong,
                  "start"      : (gStart["row"], gStart["col"]),
                  "stop"       : (gTarget["row"], gTarget["col"]),
                  "startWorld" : cStartWorld,
                  "endWorld"   : cTargetWorld,
                  "heading"    : cHeading, 
                  "distance"   : cDistance,
                  "duration"   : cDuration,
                  "work"       : cWork,
                  "coords"     : cCoord,
                  "reward"     : cReward,
                }
    cPathPandas = PlannerTools.path2pandas(cPathInfo)
    coverageResults.append({ "points"       : cPoints,
                             "pathPandas"   : cPathPandas,
                             "path"         : cPath,
                             "gridPath"     : gridPath,
                             "computeTime"  : computeTime,
                             "startGrid"    : gStart,
                             "endGrid"      : gTarget,
                             "startWorld"   : cPathInfo["startWorld"],
                             "endWorld"     : cPathInfo["endWorld"],
    })
json_data["plannerCoverage"]["coverageResults"] = coverageResults

#####################
# Path planner goto #
#####################
gotoResults = []


tpf = GridUtil.grid2world(seqcoords[0]["stopRow"], seqcoords[0]["stopCol"],
                   environment["region"]["raster"].GetGeoTransform(), 
                   environment["region"]["extent"]["cols"])
targetPointF  = { "Lat" : tpf[0],
                  "Lon" : tpf[1],
}

tpe = GridUtil.grid2world(seqcoords[-1]["startRow"], seqcoords[-1]["startCol"],
                   environment["region"]["raster"].GetGeoTransform(), 
                   environment["region"]["extent"]["cols"])
targetPointE  = { "Lat" : tpe[0],
                  "Lon" : tpe[1],
}


idx = int(selectedTargets["solution"].x[len(selectedTargets["solution"].x) - 1]) + 1
endPoint      = { "Lat" : safepointsTable.loc[[idx]]["Lat"],
                  "Lon" : safepointsTable.loc[[idx]]["Lon"],
}

# Plan path from start to first target
start = time.time()
segmentF = PPG.driver(startPoint, targetPointF, environment)
computeTime = time.time() - start
timeLog.loc[len(timeLog)] = ["Goto: (S," + str(selectedTargets["targetSequence"][0]) + ")", 3, computeTime]
gotoResults.append(segmentF)

# Plan path between each target
gotoIdx = 0
for x in range(len(selectedTargets["targetSequence"]) - 1):
    start = time.time()
    TID_i = selectedTargets["targetSequence"][x]
    TID_j = selectedTargets["targetSequence"][x + 1]

    tpi = GridUtil.grid2world(seqcoords[gotoIdx]["startRow"], seqcoords[gotoIdx]["startCol"],
                      environment["region"]["raster"].GetGeoTransform(), 
                      environment["region"]["extent"]["cols"])
    targetPointI  = { "Lat" : tpi[0],
                      "Lon" : tpi[1],
    }
    
    tpj = GridUtil.grid2world(seqcoords[gotoIdx]["stopRow"], seqcoords[gotoIdx]["stopCol"],
                      environment["region"]["raster"].GetGeoTransform(), 
                      environment["region"]["extent"]["cols"])
    targetPointJ  = { "Lat" : tpj[0],
                      "Lon" : tpj[1],
    }
    start = time.time()
    segmentIJ = PPG.driver(targetPointI, targetPointJ, environment)
    computeTime = time.time() - start
    timeLog.loc[len(timeLog)] = ["Goto: (" + str(selectedTargets["targetSequence"][x]) + 
                                 "," + str(selectedTargets["targetSequence"][x + 1]) + ")", 3, computeTime]
    gotoResults.append(segmentIJ)
    gotoIdx = gotoIdx + 2


# Plan path from final target to end point
start = time.time()
segmentE = PPG.driver(targetPointE, endPoint, environment)
computeTime = time.time() - start
timeLog.loc[len(timeLog)] = ["Goto: (" + str(selectedTargets["targetSequence"][numTargets - 1]) + ",E)", 3, computeTime]
gotoResults.append(segmentE)

json_data["plannerGoto"]["gotoResults"] = gotoResults

