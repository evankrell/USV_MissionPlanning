from PyGMO.problem import base
from PyGMO import algorithm, island, problem
import GridUtils as GridUtil
import RasterSetInterface as rsi
from math import acos, cos, sin, ceil
import pandas as pd
import yaml as yaml
import numpy as np
import osr
import paramiko as paramiko
from scp import SCPClient
from datetime import date, datetime, timedelta
import subprocess
import os
import sys
lib_path = os.path.abspath(os.path.join(__file__, '..', '..', 'AnalystTools'))
sys.path.append(lib_path)
import TargetFitness as TargetFitness


# Parse options
(options, args) = parseOption ()

# Where to save report
reportDestinationDir = "/work/hobi/webshare/evansecretfolder"

# Get system environment configuration
environment = initializeByFile ("config_withTargets.yaml", haveCurrents = True)

# Target coordinates
targetWorld = (-70.87939, 42.50756)	

target = GridUtil.getArchiveByWorld (targetWorld[1], targetWorld[0], 
	environment['region']['grid'], environment['region']['raster'].GetGeoTransform ())

# SSH information
sshInfo = { 'server' : 'hpcm.tamucc.edu', 
	'port' : 22, 
	'user' : 'ekrell', 
	'password' : options.password }
# Solve problem 
(solutionPath, pathPD) = solveProblem (environment, target)

# Visualize result
# Note that the plot is not cleared when functions end.
#   Thus, the additional plots will add on the current plot.
#   Explicity clear plot between calls if that is not desired. 
pltRegion = plotRegion (environment['region'], solutionPath)
plotCurrents (environment['currents'], environment['region'], solutionPath)
plotTargets (environment['targets'], environment['region'], solutionPath, pltRegion)

# Generate HTML report 
buildSolutionReport (environment, pathPD, 'report.html')

# Save HTML report
publishFile ('currents_1.png', reportDestinationDir, 'currents.png', sshInfo)
publishFile ('test.png', reportDestinationDir, 'path.png', sshInfo)
publishFile ('targets.png', reportDestinationDir, 'targets.png', sshInfo)
publishFile ('report.html', reportDestinationDir, 'report.html', sshInfo)

