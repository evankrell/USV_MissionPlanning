from pylab import *
import yaml
import netCDF4
import matplotlib.tri as Tri
import matplotlib.pyplot as plt
import netCDF4
import pandas as pd
import datetime as dt
from datetime import date, datetime, timedelta
from dateutil.rrule import rrule, DAILY
from osgeo import gdal
from math import acos, cos, sin
import time
import os
import sys
lib_path = os.path.abspath(os.path.join(__file__, '..', '..', 'AnalystTools'))
sys.path.append(lib_path)
import TargetFitness as TargetFitness

def components2MagDir (vecA, vecB):
	dotProd = sum( [vecA[i] * vecB[i] for i in range(len(vecA))] )
	magA = pow (sum( [vecA[i] * vecA[i] for i in range(len(vecA))] ), 0.5)
	magB = pow (sum( [vecB[i] * vecB[i] for i in range(len(vecB))] ), 0.5)

	if (magA * magB == 0):
		return ( 0.0, 1 )

	cosTheta = dotProd / (magA * magB)
	theta_rad = acos (cosTheta)

	return (magA, theta_rad)

def getMagDir (u, v):
	c = [ components2MagDir(v, (1, 0)) for v in zip (u, v)]
	return c 

def getGridExtent (data):
	# Sources:
	#	https://gis.stackexchange.com/a/104367

	# data: a gdal object
	cols = data.RasterXSize
	rows = data.RasterYSize
	transform = data.GetGeoTransform ()
	
	minx = transform[0]
	maxy = transform[3]
	maxx = minx + transform[1] * cols
	miny = maxy + transform[5] * rows
	
	extent = { 'minx' : minx, 'miny' : miny, 'maxx' : maxx, 'maxy' : maxy, 'rows' : rows, 'cols' : cols }
	return extent


def getNcByTime (nc, time, extent):
	# Sources: 
	#    https://stackoverflow.com/a/29136166

	itime = netCDF4.date2index (time, nc.variables['time'], select = 'nearest')
	
	# Get latitude and longitude values
	lat = nc.variables['lat'][:]
	lon = nc.variables['lon'][:]

	# Get water currents as u and v component variables
	u = nc.variables['u'][itime, 0, :]
	v = nc.variables['v'][itime, 0, :]
	points = pd.DataFrame (list (zip (lat, lon, u, v)), columns = ['lat', 'lon', 'u', 'v'])
	points = points.loc[points['lat'] >= extent['miny']]
	points = points.loc[points['lat'] <= extent['maxy']]
	points = points.loc[points['lon'] >= extent['minx']]
	points = points.loc[points['lon'] <= extent['maxx']]

	return points

def interpolate (x, y, z, gridx, gridy):
	# Source: https://gis.stackexchange.com/a/150881
	from scipy import interpolate

	f = interpolate.Rbf(x, y, z, function='linear')
	gridz = f (gridx, gridy)

	return gridz

def array2raster (grid, extent, rasterFile, baseRegion):
	# Source:
	# 	https://gis.stackexchange.com/a/150881

	drv = gdal.GetDriverByName ('GTiff')
	ds = drv.Create (rasterFile, extent['cols'], extent['rows'], 1 ,gdal.GDT_Float32)	
	band = ds.GetRasterBand (1)
	band.SetNoDataValue (-3e30)
	ds.SetGeoTransform (baseRegion.GetGeoTransform ())
	ds.SetProjection (baseRegion.GetProjection ())
	band.WriteArray (grid)
	return ds

def initRaster (baseRegion, extent, rasterFile, numBands):
	drv = gdal.GetDriverByName ('GTiff')
	ds = drv.Create (rasterFile, extent['cols'], extent['rows'], numBands, gdal.GDT_Float32)	
	ds.SetGeoTransform (baseRegion.GetGeoTransform ())
	ds.SetProjection (baseRegion.GetProjection ())
	return ds


def addRasterBand (grid, gdalRaster, i):
	band = gdalRaster.GetRasterBand (i)
	band.SetNoDataValue (0)
	band.WriteArray (grid)
	

def raster2plot (gRaster):
	# Plots a gdal raster with matplotlib
	# Returns the plot, rather than showing it
	
	return None

def getNcByDuration (sourceNC, extent, start, stop, interval_min):

	pointsSeries = []

	# For each time stamp...
	for time in rrule (MINUTELY, dtstart=start, until=stop, interval = interval_min):
		
		pointsSeries.append (getNcByTime (sourceNC, time, extent))

	return pointsSeries

def initialize (regionFile, currentsMagFile, currentsDirFile, 
	ncURL, startDate, days, interval_min):

	# Use start date and duration to determine stop date
	stopDate = startDate + timedelta (days = days)

	# Load the region of interest raster
	regionData = gdal.Open (regionFile)
	regionExtent = getGridExtent (regionData)

	# Init connection to DAP data source
	# Open as NetCDF
	sourceNC = netCDF4.Dataset (ncURL)

	params = {
		'region' : {
			'file' : regionFile,
			'raster' : regionData,
			'grid' : regionData.GetRasterBand (1).ReadAsArray().T,
			'extent' : regionExtent },
		'nc' : {
			'url' : ncURL,
			'data' : sourceNC },
		'timespan' : {
			'start' : startDate,
			'stop' : stopDate,
			'interval' : interval_min },
		'currents' : {
			'magnitude' : {
				'file' : currentsMagFile,
				'raster' : None },
			'direction' : {
				'file' : currentsDirFile,
				'raster' : None } } }
	return params

def initializeTargets (targetsFile):
	# A separate function since targets are optional

	if targetsFile == "None":
		targets = { 'file' : None, 'raster' : None, 'grid' : None }
	else:
		targetsData = gdal.Open (targetsFile)
		targetsGrid = targetsData.GetRasterBand (1).ReadAsArray().T

		targets = {
			'file' : targetsFile,
			'raster' : targetsData,
			'grid' : targetsGrid }

	return targets

def initializeLogbook (logbookFile):
	# A separate function since logbook is optional

	return TargetFitness.initLogbookByFile (logbookFile)



def initializeByFile (configFile):
	with open(configFile) as f:
		config = yaml.safe_load(f)	
	
	regionFile = config['config']['dataSources']['regionRaster']
	sourceURL = config['config']['dataSources']['ncSourceURL']
	currentsMagnitudeFile = config['config']['dataSources']['currentsMagnitudeFile']
	currentsDirectionFile = config['config']['dataSources']['currentsDirectionFile']
	
	
	startDate = config['config']['missionTime']['startDate']
	if (startDate == "None"):
		startDate = datetime.now ()
	numDays = config['config']['missionTime']['numDays']
	interval_min = config['config']['missionTime']['interval']

	params = initialize (regionFile, currentsMagnitudeFile, 
		currentsDirectionFile, sourceURL, 
		startDate, numDays, interval_min)

	# Add targets data, if available
	targetsFile = config['config']['dataSources']['targetsFile']
	params['targets'] = initializeTargets (targetsFile)
	
	# Add logbook data, if available
	logbookFile = config['config']['dataSources']['logbookFile']
	params['logbook'] = initializeLogbook (logbookFile)

	return params

def main (configFile, haveCurrents = False):
	data = initializeByFile (configFile)

	if (haveCurrents == False):
		getCurrentsRasterSet (data, showTiming = True)
	else:
		
		data['currents']['magnitude']['raster'] = gdal.Open (data['currents']['magnitude']['file'])
		data['currents']['direction']['raster'] = gdal.Open (data['currents']['direction']['file'])

	return data


def getGrid (points, zName, XI, YI):
	ZI = interpolate (points['lon'].tolist (), 
		points['lat'].tolist (), points[zName].tolist (), XI, YI)
	return ZI

def getCurrentsRasterSet (params, showTiming = False):

	
	# Get all the points, each list elem corresponds to a timestamp
	start_time = time.time ()

	pointsSeries = getNcByDuration (
		params['nc']['data'], 
		params['region']['extent'], 
		params['timespan']['start'], 
		params['timespan']['stop'], 
		params['timespan']['interval'])

	end_time = time.time ()
	if (showTiming == True):
		print ("lapsed time was %g seconds" % (end_time - start_time))
	
	# Calculate magnitude and directon for each point
	for points in pointsSeries:
		c = zip (*getMagDir (points['u'].tolist (), points['v'].tolist ()))
		points['magnitude'] = c[0]
		points['direction'] = c[1]

	# Init gdal rasters
	params['currents']['magnitude']['raster'] = initRaster (
		params['region']['raster'], 
		params['region']['extent'], 
		params['currents']['magnitude']['file'],
		len (pointsSeries))
	params['currents']['direction']['raster'] = initRaster (
		params['region']['raster'], 
		params['region']['extent'], 
		params['currents']['direction']['file'],
		len (pointsSeries))

	XI, YI = np.mgrid [params['region']['extent']['minx']:params['region']['extent']['maxx']:complex(0, params['region']['extent']['cols']), 
		params['region']['extent']['miny']:params['region']['extent']['maxy']:complex(0, params['region']['extent']['rows'])]
	
	start_time = time.time ()

	i = 1 # Band index. Count starts at 1. 
	for points in pointsSeries: 

		# Interpolate Z vals to grid
		ZMag = getGrid (points, 'magnitude', XI, YI)
		ZDir = getGrid (points, 'direction', XI, YI)
		
		# Save as raster layers
		addRasterBand (ZMag.T, params['currents']['magnitude']['raster'], i)
		addRasterBand (ZDir.T, params['currents']['direction']['raster'], i)
	
		# Increment band
		i = i + 1

	end_time = time.time ()
	if (showTiming == True):
		print ("lapsed time was %g seconds" % (end_time - start_time))      

#main ("config.yaml", haveCurrents = True)
