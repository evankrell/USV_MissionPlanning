from PyGMO.problem import base
from PyGMO import algorithm, island, problem
import GridUtils as GridUtil
import RasterSetInterface as rsi
from math import acos, cos, sin, ceil
import pandas as pd
import yaml as yaml
import numpy as np
import osr
import paramiko as paramiko
from scp import SCPClient
from datetime import date, datetime, timedelta
import subprocess
import os
import sys
lib_path = os.path.abspath(os.path.join(__file__, '..', '..', 'AnalystTools'))
sys.path.append(lib_path)
import TargetFitness as TargetFitness

def euclideanDistance (xi, yi, xj, yj):
	"""Compute the Euclidean distance between two 2D points, i and j.
	
	Args:
		xi (double): The x-coordinate of point i.
		yi (double): The y-coordinate of point i.
		xj (double): The x-coordinate of point j.
		yj (double): The y-coordinate of point j.
	
	Returns:
		double: The Euclidian distance between i and j. 
	"""

	return pow ( (pow (xi - xj, 2) + pow (yi - yj, 2)) , 0.5)

def line (x0, y0, x1, y1, grid, xLimit, yLimit, penaltyCase):
	""" Visits each element on a 2D grid along a line from point 0 to point 1
	, and counts the number of times that the element is the penaltyCase.

	Args:
		x0 (double): The x-coordinate of point 0.
		y0 (double): The y-coordinate of point 0.
		y1 (double): The x-coordinate of point 1.
		y2 (double): The y-coordinate of point 1. 
		grid (int[][]): The grid whose values are being checked
			for penaltyCase along a line from point 0 to point 1.
		xLimit (int): The length of the x-axis. 
		yLimit (int): The length of the y-axis. 
		penaltyCase (int): A numeric flag that is being counted. 
	"""
	from bresenham import bresenham
	hits = 0
	x0 = int (ceil (x0))
	y0 = int (ceil (y0))
	x1 = int (ceil (x1))
	y1 = int (ceil (y1))
	b = list(bresenham(x0, y0, x1, y1))
	for p in b:
		if grid[p[0]][p[1]] == penaltyCase:
			hits = hits + 1
	return hits
	
def pathCountPenalties(path, xStart, yStart, xStop, yStop, xLimit, yLimit, grid, penaltyCase, distance):
	""" Calculates the penalty for each path segment. 
	The penalty is the number of times that an element along a line segment is equal to the penaltyCase.
	
	Args:
		path ((double,double)[]): Sequence of interior waypoints as (x, y) pairs.
		xStart (double): The x-coordinate of the path's start location.
		yStart (double): The y-coordinate of the path's start location.
		xStop (double): The x-coordinate of the path's stop location.
		yStop (double): The y-coordinate of the path's stop location.
		xLimit (int): The length of the x-axis
		yLimit (int): The length 0f the y-axis
		grid (int[][]): The grid whose values are being checked
			for penaltyCase along each line segment.
		penaltyCase (int): A numeric flag that is being counted.
		distance ({ 'segmentPenalties':(double)[] }): Dict where penalty information is stored.
		
	Returns:
		dict: containing penalty information, both per-segment and a summation.
		
		{'segmentPenalties':(double)[]: List of penalty count for each segment.
		 'penaltyCase':(double): Summation of penalty counts.}
	"""
	penaltyCount = 0
	count = 0
	
	xi = path[0]
	yi = path[1]
	xj = None
	yj = None
	
	distance["segmentPenalties"].append (line (xStart, yStart, xi, yi, grid, xLimit, yLimit, penaltyCase))

	count = 2
	iterations = len (path) / 2 - 1
	for i in range (0, iterations):
		xj = path[count]
		count = count + 1
		yj = path[count]
		count = count + 1
		distance["segmentPenalties"].append (line(xi, yi, xj, yj, grid, xLimit, yLimit, penaltyCase))
		xi = xj
		yi = yj
	
	distance["segmentPenalties"].append (line (xi, yi, xStop, yStop, grid, xLimit, yLimit, penaltyCase))
	distance["penalty"] = sum (distance["segmentPenalties"])
	return distance


def pathPenalty(path, xStart, yStart, xStop, yStop, xLimit, yLimit, grid, penaltyCase, distance):
	""" Calculates the penalty of the entire path.
	The penalty is the number of times that an element along the path is equal to the penaltyCase.
	
	Args:
		path ((double,double)[]): Sequence of interior waypoints as (x, y) pairs.
		xStart (double): The x-coordinate of the path's start location.
		yStart (double): The y-coordinate of the path's start location.
		xStop (double): The x-coordinate of the path's stop location.
		yStop (double): The y-coordinate of the path's stop location.
		xLimit (int): The length of the x-axis
		yLimit (int): The length 0f the y-axis
		grid (int[][]): The grid whose values are being checked
			for penaltyCase along each line segment.
		penaltyCase (int): A numeric flag that is being counted.
		distance ({ 'segmentPenalties':(double)[] }): Dict where penalty information is stored.
		
	Returns:
		dict: containing penalty information, both per-segment and a summation. Also a weighted summation.
		
		{'segmentPenalties':(double)[]: List of penalty count for each segment.
		 'penaltyCase':(double): Summation of penalty counts.
		 'penaltyWeighted':(double): Weighted summation of penalty counts.}
	"""

	obstructions = pathCountPenalties(path, xStart, yStart, xStop, yStop, xLimit, yLimit, grid, penaltyCase, distance)
	distance["penaltyWeighted"] = pow (distance["penalty"], 2)
	return distance

def pathDistance (path, xStart, yStart, xStop, yStop, xLimit, yLimit, grid, penaltyCase):

	distance = {"total" : 0.0, "penalty" : 0.0, "penaltyWeighted" : 0.0, "segments" : [], "segmentPenalties" : []}

	xi = path[0]
	yi = path[1]
	xj = None
	yj = None

	distance["segments"].append (euclideanDistance (xStart, yStart, xi, yi))

	count = 2
	iterations = len (path) / 2 - 1
	for i in range (0, iterations):
		xj = path[count]
		count = count + 1
		yj = path[count]
		count = count + 1
		distance["segments"].append (euclideanDistance (xi, yi, xj, yj))
		xi = xj
		yi = yj

	distance["segments"].append (euclideanDistance (xi, yi, xStop, yStop))
	distance["total"] = sum (distance["segments"])

	pathPenalty (path, xStart, yStart, xStop, yStop, xLimit, yLimit, grid, penaltyCase, distance)
	return distance

def getWorkAlongLine (x0, y0, x1, y1, gridMagnitude, gridDirection, bandIdx, xLimit, yLimit, targetVelocity, pixelResolution, heading):
	# Calculates "work" relative to velocity. 
	
	from bresenham import bresenham

	def calcVelocityDiff (vecA, vecB):
		#
		# vecA: Required velocity vector in (magnitude, direction)
		# vecB: Applied velocity vector in (magnitude, direction)
		
		# Break both angles into components
		magA = vecA[0]
		dirA = vecA[1]
		magB = vecB[0]
		dirB = vecB[1]
		
		xA = magA * cos (dirA)
		yA = magA * sin (dirA)
		xB = magB * cos (dirB)
		yB = magB * sin (dirB)

		xDeltaVelocity = xA - xB
		yDeltaVelocity = yA - yB
	
		dirDeltaVelocity = angle ( (xDeltaVelocity, yDeltaVelocity), (1, 0) )
		magDeltaVelocity = pow ( (xDeltaVelocity * xDeltaVelocity + yDeltaVelocity * yDeltaVelocity), 0.5 )
		
		return (magDeltaVelocity, dirDeltaVelocity, xDeltaVelocity, yDeltaVelocity) 
		
	def calcRelativeWork (speed, distance):
		# speed is velocity magnitude
		return speed * distance

	gridM = gridMagnitude.GetRasterBand (bandIdx).ReadAsArray ().T
	gridD = gridDirection.GetRasterBand (bandIdx).ReadAsArray ().T

	work = 0
	x0 = int (ceil (x0))
	y0 = int (ceil (y0))
	x1 = int (ceil (x1))
	y1 = int (ceil (y1))
	b = list(bresenham(x0, y0, x1, y1))
	for p in b:
		# Calculate work
		appliedVelocity = calcVelocityDiff ( (targetVelocity, heading) , 
			(gridM[p[0]][p[1]], gridD[p[0]][p[1]]))
		work = work + calcRelativeWork (appliedVelocity[0], pixelResolution)
	return work

def calcDuration (distance, speed):
		return distance / speed

def pathDuration (path, distance, speed):

	duration = {"total" : 0.0, "segments" : []}

	iterations = len (path) / 2 + 1
	for i in range (0, iterations):
		duration["segments"].append (calcDuration (distance["segments"][i], speed))

	duration["total"] = sum (duration["segments"])
	return duration

def getBandIdxByElapsedTime (elapsedTime, bandInterval):
	idx = int (ceil ((elapsedTime * 1.0) / bandInterval))
	return idx
	
def pathEnergy (path, xStart, yStart, xStop, yStop, 
	xLimit, yLimit, gridMagnitude, gridDirection, 
	pixelResolution, distance, duration, headings, USV):
 
	work = {"total" : 0.0, "segments" : []}
	
	xi = path[0]
	yi = path[1]
	xj = None
	yj = None

	ii = 0

	# Duration = 0 -> band index = 1	
	d = 0
	bandIdx = 1

	work["segments"].append (getWorkAlongLine (xStart, yStart, xi, yi, 
		gridMagnitude, gridDirection, bandIdx,
		xLimit, yLimit, USV["speed"], pixelResolution, headings[ii]))
	
	ii = ii + 1
	count = 2
	iterations = len (path) / 2 - 1
	for i in range (0, iterations):
		xj = path[count]
		count = count + 1
		yj = path[count]
		count = count + 1

		d = d + duration['segments'][ii]
		bandIdx = getBandIdxByElapsedTime (d, environment['timespan']['interval'])

		work["segments"].append (getWorkAlongLine (xi, yi, xj, yj, 
			gridMagnitude, gridDirection, bandIdx,
			xLimit, yLimit, USV["speed"], pixelResolution, headings[ii]))
		
		ii = ii + 1
		xi = xj
		yi = yj

		d = d + duration['segments'][ii]
		bandIdx = getBandIdxByElapsedTime (d, environment['timespan']['interval'])

	work["segments"].append (getWorkAlongLine (xj, yj, xStop, yStop, 
		gridMagnitude, gridDirection, bandIdx,
		xLimit, yLimit, USV["speed"], pixelResolution, headings[ii]))
	
	work["total"] = sum (work["segments"])

	return work


def pathReward (path, xStart, yStart, xStop, yStop, xLimit, yLimit, 
	gridTargets, logbook):

	# Call the analyst agent to do this
	reward = TargetFitness.rewardLine (path, xStart, yStart, xStop, yStop,
		xLimit, yLimit, gridTargets, logbook)

	reward['weightedTotal'] = reward['total'] * logbook['tWeight']

	return reward
	
def angle (vecA, vecB):
	dotProd = sum( [vecA[i] * vecB[i] for i in range(len(vecA))] )
	magA = pow (sum( [vecA[i] * vecA[i] for i in range(len(vecA))] ), 0.5)
	magB = pow (sum( [vecB[i] * vecB[i] for i in range(len(vecB))] ), 0.5)
	
	if (magA * magB == 0):
		return 0.0

	cosTheta = dotProd / (magA * magB)
	theta_rad = acos (cosTheta)
	return theta_rad

def vSub (vecA, vecB):
	vecS = (vecB[0] - vecA[0], vecB[1] - vecA[1])
	return vecS

def pathHeading (path, xStart, yStart, xStop, yStop):

	headings = []

	xi = path[0]
	yi = path[1]
	xj = None
	yj = None

	v = vSub ( (xStart, yStart), (xi, yi) )
	a = angle (v, (1, 0))
	headings.append (a)

	count = 2
	iterations = len (path) / 2 - 1
	for i in range (0, iterations):
		xj = path[count]
		count = count + 1
		yj = path[count]
		count = count + 1

		v = vSub ( (xi, yi), (xj, yj) )
		a = angle (v, (1, 0))
		headings.append (a)

		xi = xj
		yi = yj

	v = vSub ( (xj, yj), (xStop, yStop) )
	a = angle (v, (1, 0))
	headings.append (a)

	return headings

class highestCurrentProblem (base):

	def __init__(self, dim = 10):

		# Set problem dimensions to be twice the number of waypoints,
		#   to account for the x and y coordinates of each
		dim = environment['optimizationCriteria']['numWaypoints'] * 2

		super (highestCurrentProblem, self).__init__(dim)

		self.xStart = environment['vehicle']['startCoordsArchive']['col']
		self.xStop = target['col']
		self.yStart = environment['vehicle']['startCoordsArchive']['row']
		self.yStop = target['row']

		# Get size of pixel (resolution in meters)
		self.pixelSize_m = calcPixelResolution_m (environment['region']['grid'], environment['region']['raster'].GetGeoTransform ())

		# Set problem bounds.
		upperBounds = [0] * dim
		for i in range (0, dim):
			if (i % 2 == 0):
				# Y values
				upperBounds[i] = environment['region']['extent']['cols'] - 1
			else:
				#X values
				upperBounds[i] = environment['region']['extent']['rows'] - 1

		self.set_bounds ([0] * dim, upperBounds)


	# The actual objective function, X is the N-dimension solution vector.
	def _objfun_impl (self, x):

		###############
		# Non temporal#
		###############

		heading = pathHeading (x, self.xStart, 
			self.yStart, 
			self.xStop, 
			self.yStop)

		distance = pathDistance (x, 
			self.xStart, 
			self.yStart, 
			self.xStop, 
			self.yStop, 
			environment["region"]["extent"]["cols"],
			environment["region"]["extent"]["rows"],
			environment['region']['grid'],
			environment['optimizationCriteria']['obstacle_flag'])
	
		duration = pathDuration (x, 
			distance, 
			environment["vehicle"]["speed"])

		reward = pathReward (x,
			self.xStart, 
			self.yStart, 
			self.xStop, 
			self.yStop, 
			environment["region"]["extent"]["cols"],
			environment["region"]["extent"]["rows"],
			environment["targets"]["grid"],
			environment['logbook'])

		###########
		# Temporal#
		###########

		work = pathEnergy (x, 
			self.xStart, 
			self.yStart, 
			self.xStop, 
			self.yStop, 
			environment["region"]["extent"]["cols"],
			environment["region"]["extent"]["rows"],
			environment["currents"]["magnitude"]["raster"],
			environment["currents"]["direction"]["raster"],
			self.pixelSize_m, 
			distance, 
			duration,
			heading, 
			environment["vehicle"])

		# Combine objectives into single fitness function
		f = distance["total"] + (10) * distance["penaltyWeighted"] - reward['weightedTotal'] + (0.1) * work["total"]

		return (f, )

def getPathCoords (path, start, stop, grid, transform):

	coordArchive = []
	coordArchive.append (GridUtil.getArchiveByGrid (start[1], start[0], grid, transform))
	iterations = len (path) / 2 
	count = 0

	for i in range (0, iterations * 2, 2):
		coordArchive.append (GridUtil.getArchiveByGrid (path[i + 1], path[i], grid, transform))
	coordArchive.append (GridUtil.getArchiveByGrid (stop[1], stop[0], grid, transform))
	return coordArchive


def statPath (environment, path, target):

	pixelSize_m = calcPixelResolution_m (environment['region']['grid'], environment['region']['raster'].GetGeoTransform ())

	xStart = environment['vehicle']['startCoordsArchive']['col']
	xStop = target['col']
	yStart = environment['vehicle']['startCoordsArchive']['row']
	yStop = target['row']

	# Get coordinates
	coord = getPathCoords (path, 
		(xStart, yStart),
		(target['col'], target['row']), 
		environment['region']['grid'],
		environment['region']['raster'].GetGeoTransform ())

	# Get headings
	heading = pathHeading (path, 
		xStart,
		yStart,
		target['col'],
		target['row'])

	# Get distance
	distance = pathDistance (path, 
		xStart,
		xStop,
		target['col'],
		target['row'],
		environment['region']['extent']['cols'],
		environment['region']['extent']['rows'],
		environment['region']['grid'],
		environment['optimizationCriteria']['obstacle_flag'])

	duration = pathDuration (path, 
			distance, 
			environment['vehicle']['speed'])

	work = pathEnergy (path, 
			xStart,
			xStop,
			target['col'],
			target['row'],
			environment["region"]["extent"]["cols"],
			environment["region"]["extent"]["rows"],
			environment["currents"]["magnitude"]["raster"],
			environment["currents"]["direction"]["raster"],
			pixelSize_m, 
			distance, 
			duration,
			heading, 
			environment["vehicle"])

	return (heading, distance, duration, work, coord)	

def writePandas (df, filename):
	df.to_csv (filename, sep= ',', index=False)

def path2pandas (pathInfo):
	
	# Init df
	pathDF = pd.DataFrame ()
	
	# Break path into x and y components
	xInner = pd.Series (pathInfo["path"][::2])
	yInner = pd.Series (pathInfo["path"][1::2])
	x1 = pd.Series ([pathInfo['start'][0]])
	x2 = pd.Series ([pathInfo['stop'][0]])
	y1 = pd.Series ([pathInfo['start'][1]])
	y2 = pd.Series ([pathInfo['stop'][1]])
	x = x1
	x = x.append (xInner)
	x = x.append (x2)
	y = y1
	y = y.append (yInner)
	y = y.append (y2)
	x.index = range(len(x.index))
	y.index = range(len(y.index))

	heading = pd.Series ([0])
	heading = heading.append (pd.Series (pathInfo['heading']))
	heading.index = range(len(heading.index))

	distance = pd.Series ([0])
	distance = distance.append (pd.Series (pathInfo['distance']['segments']))
	distanceAccum = distance.cumsum ()
	distance.index = range(len(distance.index))
	distanceAccum.index = range(len(distanceAccum.index))

	duration = pd.Series ([0])
	duration = duration.append (pd.Series (pathInfo['duration']['segments']))
	durationAccum = duration.cumsum ()
	duration.index = range(len(duration.index))
	durationAccum.index = range(len(durationAccum.index))

	work = pd.Series ([0])
	work = work.append (pd.Series (pathInfo['work']['segments']))
	workAccum = work.cumsum ()
	work.index = range(len(work.index))
	workAccum.index = range(len(workAccum.index))
	
	lat = [t["lat"] for t in pathInfo["coords"]]
	lon = [t["lon"] for t in pathInfo["coords"]]
	lat = pd.Series (lat)
	lat.index = range (len(lat.index))
	lon= pd.Series (lon)
	lon.index = range (len(lon.index))

	# Combine into data frame
	pathDF['X'] = x
	pathDF['Y'] = y
	pathDF['LONGITUDE'] = lon
	pathDF['LATITUDE'] = lat
	pathDF['HEADING'] = heading
	pathDF['DISTANCE'] = distance
	pathDF['DISTANCE_ACCUM'] = distanceAccum
	pathDF['DURATION'] = duration
	pathDF['DURATION_ACCUM'] = durationAccum
	pathDF['WORK'] = work
	pathDF['WORK_ACCUM'] = workAccum

	return pathDF


def initializeByFile (configFile, haveCurrents = False):

	with open (configFile) as f:
		config = yaml.safe_load (f)

	environment = rsi.main (configFile, haveCurrents)
	environment['vehicle'] = {
		'speed': config['config']['vehicle']['speed'],
		'startCoords': (config['config']['vehicle']['startCoordinates_lon'],
			config['config']['vehicle']['startCoordinates_lat']),
		'startCoordsArchive': GridUtil.getArchiveByWorld (config['config']['vehicle']['startCoordinates_lat'], 
			config['config']['vehicle']['startCoordinates_lon'],
			environment['region']['raster'].GetRasterBand (1).ReadAsArray ().T,
			environment['region']['raster'].GetGeoTransform() )}
	environment['optimizationCriteria'] = {
		'numWaypoints': config['config']['optimization_criteria']['numWaypoints'], 
		'obstacle_flag': config['config']['optimization_criteria']['obstacle_flag'],
		'generations': config['config']['optimization_criteria']['generations'],
		'individuals': config['config']['optimization_criteria']['individuals'] }

	return environment

def solveProblem (environment, target):
	
	# Configure problem using environment and target configuration
	dim = environment['optimizationCriteria']['numWaypoints'] * 2
	prob = highestCurrentProblem(dim = dim)
	algo = algorithm.pso (gen = environment['optimizationCriteria']['generations'])
	isl = island (algo, prob, environment['optimizationCriteria']['individuals'])
	isl.evolve (1)
	isl.join ()
 
	pathInfo = {"path" : isl.population.champion.x, 
		"constraints" : isl.population.champion.c,  
		"fitness" : isl.population.champion.f,
		"start" : (environment['vehicle']['startCoordsArchive']['col'], 
			environment['vehicle']['startCoordsArchive']['row']), 
		"stop" : (target['col'], target['row']),
		"heading" : None, 
		"distance" : None, 
		"duration" : None, 
		"work" : None, 
		"coords" : None }

	sp = statPath (environment, 
		isl.population.champion.x, 
		target)

	pathInfo["heading"] = sp[0]
	pathInfo["distance"] = sp[1]
	pathInfo["duration"] = sp[2]
	pathInfo["work"] = sp[3]
	pathInfo["coords"] = sp[4]

	pathPD = path2pandas (pathInfo)

	writePandas (pathPD, "champion.csv")

	return (pathInfo, pathPD)


def plotTargets (targets, region, pathInfo, pltRegion):
	import georaster
	from osgeo import gdal
	import matplotlib.pyplot as plt
	import numpy as np
	from mpl_toolkits.basemap import Basemap

	extent = 0, region['grid'].shape[0], 0, region['grid'].shape[1]

	t = plt.imread (targets['file'])
	tplot = plt.imshow (t, interpolation = 'nearest', extent = extent)

	plt.savefig ('targets.png')

	


def plotRegion (region, pathInfo):
	# Source:
	# 	https://stackoverflow.com/a/45708066

	import georaster
	from osgeo import gdal
	import matplotlib.pyplot as plt
	import numpy as np
	from mpl_toolkits.basemap import Basemap

	fig = plt.figure (figsize = (6, 8))
	fpath = region['file']

	my_image = georaster.SingleBandRaster(fpath, load_data=False)

	# grab limits of image's extent
	minx, maxx, miny, maxy = my_image.extent

	# set Basemap with slightly larger extents
	# set resolution at intermediate level "i"
	m = Basemap (projection='cyl',
            llcrnrlon=minx-.005, \
            llcrnrlat=miny-.005, \
            urcrnrlon=maxx+.005, \
            urcrnrlat=maxy+.005, \
            resolution='h')

	#m.drawcoastlines(color="gray")
	#m.fillcontinents(color='beige')

	# load the geotiff image, assign it a variable
	image = georaster.SingleBandRaster( fpath, \
				load_data=(minx, maxx, miny, maxy), \
				latlon=True)

	# plot the image on matplotlib active axes
	# set zorder to put the image on top of coastlines and continent areas
	# set alpha to let the hidden graphics show through
	plt.imshow(image.r, extent=(minx, maxx, miny, maxy), zorder=10, alpha=0.6)


	# Extract latitude and longitude from the path coordinates
	lat = [y['lat'] for y in pathInfo['coords']]
	lon = [x['lon'] for x in pathInfo['coords']]
	x, y = m (lon, lat)

	m.plot (x, y, 'rx--')
	m.scatter ([x[0]], [y[0]], s = 80, marker = '^', color = 'b', zorder = 1)
	plt.text (x[0] + 10, y[0] + 10, 'Start')
	m.scatter ([x[len(x)-1]], [y[len(y)-1]], s = 80, marker = 'p', color = 'r', zorder = 1)

	plt.savefig ("test.png")

	return m


def plotCurrents (currents, region, pathInfo):
	import matplotlib.pyplot as plt
	import numpy as np
	import georaster
	from osgeo import gdal
	from mpl_toolkits.basemap import Basemap
	from numpy import ma

	bands = []
	bandTable = {}
	dAccum = 0
	segment = 0
	count = 0
	for d in pathInfo['duration']['segments']:
		dAccum = dAccum + d
		band = getBandIdxByElapsedTime (dAccum, environment['timespan']['interval'])
		
		if (band not in bands):
			bands.append (band)
			bandTable[band] = [count]
		else:
			bandTable[band].append (count)
		count = count + 1

	files = []	
	for bandIdx in bands:

		fig = plt.figure (figsize = (6, 8))
		fpath = region['file']

		my_image = georaster.SingleBandRaster(fpath, load_data=False)

		# grab limits of image's extent
		minx, maxx, miny, maxy = my_image.extent

		# set Basemap with slightly larger extents
		# set resolution at intermediate level "i"
		m = Basemap (projection='cyl',
		    llcrnrlon=minx-.005, \
		    llcrnrlat=miny-.005, \
		    urcrnrlon=maxx+.005, \
		    urcrnrlat=maxy+.005, \
		    resolution='h')

		# load the geotiff image, assign it a variable
		image = georaster.SingleBandRaster( fpath, \
					load_data=(minx, maxx, miny, maxy), \
					latlon=True)

		# plot the image on matplotlib active axes
		# set zorder to put the image on top of coastlines and continent areas
		# set alpha to let the hidden graphics show through
		#plt.imshow(image.r, extent=(minx, maxx, miny, maxy), zorder=10, alpha=0.6)
		plt.imshow(image.r, extent=(0, region['extent']['cols'], 0, region['extent']['rows']), zorder=10, alpha=0.6)


		# Add water currents
		mCurrentsGrid = np.fliplr (currents['magnitude']['raster'].GetRasterBand (bandIdx).ReadAsArray ().T)
		dCurrentsGrid = np.fliplr (currents['direction']['raster'].GetRasterBand (bandIdx).ReadAsArray ().T)
		
		# Data required to plot the currents
		xSamples = []
		ySamples = []
		uSamples = []
		vSamples = []

		gridFlip = np.fliplr (region['grid'])

		# Sample interval
		sampleInterval = 10 # Pixels
		for x in range (0, region['extent']['cols'], sampleInterval):
			for y in range (0, region['extent']['rows'], sampleInterval):
				xSamples.append (x)
				ySamples.append (y)
				if (gridFlip[x][y] == 0):
					uSamples.append (mCurrentsGrid[x][y] * cos (dCurrentsGrid[x][y]))
					vSamples.append (mCurrentsGrid[x][y] * sin (dCurrentsGrid[x][y]))
				else:
					uSamples.append (0)
					vSamples.append (0)

		# Overlay the vector field
		plt.quiver(xSamples[::10], ySamples[::10], uSamples[::10], vSamples[::10])


		# Extract latitude and longitude from the path coordinates
		lat = [y['row'] for y in pathInfo['coords']]
		lon = [x['col'] for x in pathInfo['coords']]
		x, y = m (lon, lat)

		ny = [(-1) * y + region['extent']['rows'] for y in y]

		plt.plot (x, ny, 'rx--')
		outFile = "currents_" + str (bandIdx) +  ".png"
		files.append (outFile)
		plt.savefig (outFile)


	# Make gif out of current grids
	fileStr = ""
	for f in files:
		fileStr = fileStr + " " + f
	cmd = "convert -loop 0 -delay 100 " + fileStr + " currents.gif"
	print (cmd)
	#subprocess.Popen (cmd, cwd = r'/home/ekrell/thesis_stuff/USV_MissionPlanning/Playground/')


def buildSolutionReport (environment, solutionPath, reportFile):
	from yattag import Doc
	import json

	doc, tag, text = Doc ().tagtext ()
	
	doc.asis('<!DOCTYPE html>')
	with tag ('head'):
		doc.asis ('<link rel="stylesheet" type="text/css" href="style.css">')

	with tag('h2'):
		text ('USV Path Planning Result')
	with tag ('h3'):
		text ('Generated: ' + datetime.now ().strftime('%m/%d/%Y_%H:%M:%S'))
	with tag ('p'):
		with tag ('b'):
			with tag ('a', href = 'https://gitlab.com/evankrell/USV_MissionPlanning'):
				text ('Project Repository')

	
	# Place the figures
	with tag ('html'):
		with tag ('body'):
			with tag ('div', klass = 'static_figures'):
					with tag ('img', src='path.png'):
						text ('')
					with tag ('div', klass = "inbl"):
						with tag ('img', src='currents.png'):
							text ('')
					with tag ('div', klass = 'inbl'):
						with tag ('img', src='targets.png'):
							text ('')

	# Place the solution path table
	pathTable = solutionPath.to_html ()
	doc.asis (pathTable)


	# A placeholder to edit later
        with tag ('p'):
                with tag ('b'):
                        text ('Notes')
	with tag ('p'):
		text ('NONE')


	# Place the environment details

	# Region
	with tag ('p'):
		with tag ('b'):
			text ('Region')
	with tag ('p'):
		text ('Geotiff raster file: ' + str (environment['region']['file']))
	with tag ('p'):
		text ('Rows: ' + str (environment['region']['extent']['rows']))
	with tag ('p'):
		text ('Cols: ' + str (environment['region']['extent']['cols']))

	# Time Frame
	with tag ('p'):
		with tag ('b'):
			text ('Time Frame')
	with tag ('p'):
		text ('Start: ' + environment['timespan']['start'].strftime('%m/%d/%Y_%H:%M:%S'))
	with tag ('p'):
		text ('Stop: ' + environment['timespan']['stop'].strftime('%m/%d/%Y_%H:%M:%S'))
	with tag ('p'):
		text ('Resolution (minutes): ' + str (environment['timespan']['interval']))

	# ROMS Source
	with tag ('p'):
		with tag ('b'):
			text ('ROMS Data Source')
	with tag ('p'):
		text ('URL: ' + str (environment['nc']['url']))

	# Currents
	with tag ('p'):
		with tag ('b'):
			text ('Water Currents')
	with tag ('p'):
		text ('Magnitude geotiff raster file: ' + str (environment['currents']['magnitude']['file']))
	with tag ('p'):
		text ('Direction geotiff raster file: ' + str (environment['currents']['direction']['file']))

	# Targets (Display of targets info is defined by Analyst)
	with tag ('p'):
		with tag ('b'):
			text ('Targets')
	with tag ('p'):
		text ('Analyst logbook config file: ' + str (environment['targets']['file']))

	doc.asis (TargetFitness.logbook2html (environment['logbook']))	


	# Vehicle
	with tag ('p'):
		with tag ('b'):
			text ('Vehicle')
	with tag ('p'):
		text ('Start position: (Latitude: ' + str (environment['vehicle']['startCoords'][1]) + ', Longitude: ' + str (environment['vehicle']['startCoords'][0]))
	with tag ('p'):
		text ('Target speed: ' + str (environment['vehicle']['speed']))
	
	# Optimization Parameters
	with tag ('p'):
		with tag ('b'):
			text ('Optimization Parameters')
	with tag ('p'):
		text ('Number of waypoints: ' + str (environment['optimizationCriteria']['numWaypoints']))
	with tag ('p'):
		text ('Generations: ' + str (environment['optimizationCriteria']['generations']))
	with tag ('p'):
		text ('Individuals: ' + str (environment['optimizationCriteria']['individuals']))
	

	with open (reportFile,'w') as f:
		f.write(doc.getvalue ())

	return doc.getvalue ()


def publishFile (sourceFile, destDir, destFile, sshInfo):

	def createSSHClient(server, port, user, password):
	# Source:
	#    https://stackoverflow.com/a/4282261	
		client = paramiko.SSHClient()
		client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		client.connect(server, port, user, password)
		return client

	paramiko.util.log_to_file ('paramiko.log')	

	ssh = createSSHClient (sshInfo['server'], sshInfo['port'], sshInfo['user'], sshInfo['password'])

	sftp = ssh.open_sftp()
	sftp.chdir (destDir)
	sftp.put (sourceFile, destFile)

def calcPixelResolution_m (grid, transform):
	import geopy.distance

	i = 10

	# Pixel 1: [0, 0]
	p1 = GridUtil.getArchiveByGrid (0, 0, grid, transform)
	
	# Pixel 2: [0, i]
	p2 = GridUtil.getArchiveByGrid (0, i, grid, transform)

	# Distance between them in kilometers
	distance = geopy.distance.vincenty( (p1['lon'], p1['lat']), (p2['lon'], p2['lat'])).m

	# Pixel resolution
	res = distance / i

	return res

def parseOption ():
	from optparse import OptionParser
	parser = OptionParser()
	parser.add_option ("-p", "--password", dest="password",
			  help="Password for SCP to transfer report to server", metavar="PASS")
	return parser.parse_args()




#### Main 

# Parse options
(options, args) = parseOption ()

# Where to save report
reportDestinationDir = "/work/hobi/webshare/evansecretfolder"

# Get system environment configuration
environment = initializeByFile ("config_withTargets.yaml", haveCurrents = True)

# Target coordinates
targetWorld = (-70.87939, 42.50756)	

target = GridUtil.getArchiveByWorld (targetWorld[1], targetWorld[0], 
	environment['region']['grid'], environment['region']['raster'].GetGeoTransform ())

# SSH information
sshInfo = { 'server' : 'hpcm.tamucc.edu', 
	'port' : 22, 
	'user' : 'ekrell', 
	'password' : options.password }
# Solve problem 
(solutionPath, pathPD) = solveProblem (environment, target)

# Visualize result
# Note that the plot is not cleared when functions end.
#   Thus, the additional plots will add on the current plot.
#   Explicity clear plot between calls if that is not desired. 
pltRegion = plotRegion (environment['region'], solutionPath)
plotCurrents (environment['currents'], environment['region'], solutionPath)
plotTargets (environment['targets'], environment['region'], solutionPath, pltRegion)

# Generate HTML report 
buildSolutionReport (environment, pathPD, 'report.html')

# Save HTML report
publishFile ('currents_1.png', reportDestinationDir, 'currents.png', sshInfo)
publishFile ('test.png', reportDestinationDir, 'path.png', sshInfo)
publishFile ('targets.png', reportDestinationDir, 'targets.png', sshInfo)
publishFile ('report.html', reportDestinationDir, 'report.html', sshInfo)

