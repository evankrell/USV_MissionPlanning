from pylab import *
import yaml
import netCDF4
import matplotlib.tri as Tri
import matplotlib.pyplot as plt
import netCDF4
import pandas as pd
import datetime as dt
from datetime import date, datetime, timedelta
from dateutil.rrule import rrule, DAILY
from osgeo import gdal
from math import acos, cos, sin
import time
import os
import sys

from scipy.interpolate import griddata
import georaster
from mpl_toolkits.basemap import Basemap




def getNcByTime (nc, time, extent):
	# Sources: 
	#    https://stackoverflow.com/a/29136166

	itime = netCDF4.date2index (time, nc.variables['time'], select = 'nearest')

	# Get latitude and longitude values
	lat = nc.variables['lat'][:]
	lon = nc.variables['lon'][:]
        latc = nc.variables['latc'][:]
        lonc = nc.variables['lonc'][:]

        # Find velocity points in bounding box
        ind = np.argwhere((lonc >= extent["minx"]) & (lonc <= extent["maxx"]) & (latc >= extent["miny"]) & (latc <= extent["maxy"]))
        subsample=3
        np.random.shuffle(ind)
        Nvec = int(len(ind) / subsample)
        idv = ind[:Nvec]
        #idv = ind

	# Get water currents as u and v component variables
	u = nc.variables['u'][itime, 0, :]
	v = nc.variables['v'][itime, 0, :]

        slatc = latc[idv]
        slonc = lonc[idv]
        su    = u[idv]
        sv    = v[idv]

        slatc = [latc[0] for latc in slatc]
        slonc = [lonc[0] for lonc in slonc]
        su    = [u[0]    for u    in su]
        sv    = [v[0]    for v    in sv]

        points = pd.DataFrame(list(zip(slatc, slonc, su, sv)), columns = ['latc', 'lonc', 'u', 'v'])

	points = points.loc[points['latc'] >= extent['miny']]
	points = points.loc[points['latc'] <= extent['maxy']]
	points = points.loc[points['lonc'] >= extent['minx']]
	points = points.loc[points['lonc'] <= extent['maxx']]

	points = points.sample(frac=0.5, replace=False)
	return points

# Sometimes the 'forecast' is not yet available

casttype = 'nowcast'
#casttype = 'forecast'

date = '20180808';

url_uvel ='http://nomads.ncep.noaa.gov:9090/dods/'+ \
    'rtofs/rtofs_global'+date+ \
    '/rtofs_glo_3dz_'+casttype+'_daily_uvel'
url_vvel ='http://nomads.ncep.noaa.gov:9090/dods/'+ \
    'rtofs/rtofs_global'+date+ \
    '/rtofs_glo_3dz_'+casttype+'_daily_vvel'

nc_uvel = netCDF4.Dataset(url_uvel)
nc_vvel = netCDF4.Dataset(url_vvel)

lat_uvel = nc_uvel['lat'][:]
lon_uvel = nc_uvel['lon'][:]
lat_vvel = nc_vvel['lat'][:]
lon_vvel = nc_vvel['lon'][:]

data_uvel = nc_uvel.variables['u'][1,1,:,:]
data_vvel = nc_vvel.variables['v'][1,1,:,:]



m=Basemap(projection='mill',lat_ts=10, \
  llcrnrlon=lon_uvel.min(),urcrnrlon=lon_uvel.max(), \
  llcrnrlat=lat_uvel.min(),urcrnrlat=lat_uvel.max(), \
  resolution='c')

Lon, Lat = meshgrid(lon_uvel,lat_uvel)
x, y = m(Lon,Lat)

cs = m.pcolormesh(x,y,data_vvel,shading='flat', \
  cmap=plt.cm.jet)

plt.show()








