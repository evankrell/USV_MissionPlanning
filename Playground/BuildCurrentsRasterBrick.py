# Use a NetCDF file to generate a multi-band Geotiff
# This "raster brick" has one band per interval.
# Example:
#     Duration = 24 hours
#     Interval = 1 hour
#     Raster brick will have 24 bands

import RasterSetInterface as rsi

data = rsi.initializeByFile ("config_simulationRasterBuild.yaml")
rsi.getCurrentsRasterSet (data, showTiming = True)

