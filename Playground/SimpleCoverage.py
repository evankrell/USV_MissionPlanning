#!/usr/bin/python

import GridUtils as GridUtil
import RasterSetInterface as rsi
import pandas as pd
import numpy as np
from osgeo import gdal
from PyGMO.problem import base
from PyGMO import algorithm, island, problem
import math
import sys

# Globals

ACTIONS = ['N', 'U', 'D', 'L', 'R', 'A', 'B', 'C', 'D']

REGION = None
COVERAGE_PARAMS = None
COVERAGE_POINTS = None
PSO_PARAMS = None
GOAL_POINTS = None
DIM = None


def idx2rowcol(idx, ncol):
    row = int(idx / ncol)
    col = int(idx % ncol)
    return row, col

def rowcol2idx(row, col, ncol):
    idx = (row * ncol) + col
    return idx


def euclideanDistance (xi, yi, xj, yj):
    """Compute the Euclidean distance between two 2D points, i and j.

    Args:
          xi (double): The x-coordinate of point i.
          yi (double): The y-coordinate of point i.
          xj (double): The x-coordinate of point j.
          yj (double): The y-coordinate of point j.

    Returns:
          double: The Euclidian distance between i and j.
    """
    return pow ( (pow (xi - xj, 2) + pow (yi - yj, 2)) , 0.5)


def line (x0, y0, x1, y1, grid, xLimit, yLimit, penaltyCase):
    """ Visits each element on a 2D grid along a line from point 0 to point 1,
        and counts the number of times that the element is the penaltyCase.

    Args:
          x0 (double): The x-coordinate of point 0.
          y0 (double): The y-coordinate of point 0.
          y1 (double): The x-coordinate of point 1.
          y2 (double): The y-coordinate of point 1.
          grid (int[][]): The grid whose values are being checked
              for penaltyCase along a line from point 0 to point 1.
          xLimit (int): The length of the x-axis.
          yLimit (int): The length of the y-axis.
          penaltyCase (int): A numeric flag that is being counted.
    """
    from bresenham import bresenham
    hits = 0
    x0 = int (ceil (x0))
    y0 = int (ceil (y0))
    x1 = int (ceil (x1))
    y1 = int (ceil (y1))
    b = list(bresenham(x0, y0, x1, y1))
    for p in b:
        if grid[p[0]][p[1]] == penaltyCase:
            hits = hits + 1
    return hits 

def pathDistance(coveragePoints, pathIdx, grid):
    path = [coveragePoints[p] for p in pathIdx]
    path = [item for spath in path for item in spath]
    print(path)

    return 1, 1


def pathFitness2(actionSequence, coveragePoints, region):

    def getMoveIdx(position, action, ncols):
        positionCoords = idx2rowcol(position, ncols)
        newCoords = [0, 0]
        newPosition = position
        reldist = 0
        if   action == 'U':
            newCoords[0] = positionCoords[0] - 1
            newCoords[1] = positionCoords[1]
            reldist = 1
        elif action == 'D':
            newCoords[0] = positionCoords[0] + 1
            newCoords[1] = positionCoords[1]
            reldist = 1
        elif action == 'L':
            newCoords[0] = positionCoords[0]
            newCoords[1] = positionCoords[1] - 1
            reldist = 1
        elif action == 'R':
            newCoords[0] = positionCoords[0]
            newCoords[1] = positionCoords[1] + 1
            reldist = 1
        elif action == 'A':
            newCoords[0] = positionCoords[0] - 1
            newCoords[1] = positionCoords[1] - 1
            reldist = 2
        elif action == 'B':
            newCoords[0] = positionCoords[0] - 1
            newCoords[1] = positionCoords[1] + 1
            reldist = 2
        elif action == 'C':
            newCoords[0] = positionCoords[0] + 1
            newCoords[1] = positionCoords[1] - 1
            reldist = 2
        elif action == 'D':
            newCoords[0] = positionCoords[0] + 1
            newCoords[1] = positionCoords[1] + 1
            reldist = 2
        newPosition = rowcol2idx(newCoords[0], newCoords[1], ncols)
        return newPosition, reldist

    # Region boundaries
    nrows = len(coveragePoints)
    ncols = len(coveragePoints[0])

    # Initialize previous position
    prev = { "act": 'N',
             "idx": GOAL_POINTS["start"],
    }
    curr = { "act": None,
             "idx": None,
    }

    path = [prev["idx"]]

    visitedTargets   = []
    visitedObstacles = []
    visitedOutbounds = []
    visitedRepeats   = []
    skips            = []
    progressions     = []
    dist = 0

    seqlen = float (len(actionSequence))

    for action in actionSequence:
        global ACTIONS
        curr["act"] = ACTIONS[int(action)]

        # See where you would end up
        curr["idx"], deltaDist = getMoveIdx(prev["idx"], curr["act"], ncols)

        # See what the region value is
        coords = idx2rowcol(curr["idx"], ncols)

        # If no action, go onto next iteration
        if curr["act"] == 'N':
            skips.append(curr["idx"])
            continue

        if coords[0] <= 0 or coords[0] >= nrows or \
           coords[1] <= 0 or coords[1] >= ncols:
            visitedOutbounds.append(curr["idx"])
            continue

        path.append(curr["idx"])

        #print (coveragePoints[coords[0]][coords[1]])

        if coveragePoints[coords[0]][coords[1]]["target"] == True:
            if curr["idx"] in visitedTargets:
                visitedRepeats.append(curr["idx"])
            else:
                visitedTargets.append(curr["idx"])
        
        if coveragePoints[coords[0]][coords[1]]["occupied"] == True:
            visitedObstacles.append(curr["idx"])

        if curr["act"] == prev["act"]:
            progressions.append(curr["idx"])

        dist = dist + deltaDist

        prev["idx"] = curr["idx"]
        prev["act"] = curr["act"]

    
    print (visitedTargets)

    rewardCoverage   = float(len(visitedTargets))    #/ seqlen
    rewardProgress   = float(len(progressions))      #/ seqlen
    rewardSkips      = float(len(skips))             #/ seqlen
    penaltyDist      = float(dist)                   #/ seqlen
    penaltyRepeat    = float(len(visitedRepeats))    #/ seqlen
    penaltyObstacles = float(len(visitedObstacles))  #/ seqlen
    penaltyOutbounds = float(len(visitedOutbounds))  #/ seqlen

    fitness = \
              -20   *    rewardCoverage + \
              100   *    penaltyObstacles + \
              -15   *    rewardProgress + \
              10    *    penaltyRepeat
              # 50   *    penaltyDist
              

    print (fitness, rewardCoverage, penaltyObstacles, rewardProgress,
                    penaltyRepeat, penaltyDist)
    #print (path) 
    return fitness, rewardCoverage, penaltyDist, penaltyObstacles, path



def pathFitness(path, coveragePoints, region):
    
    def countTypes(path, coveragePoints):
        # Init counters
        nuniques   = 0 # Number of unique targets
        ntargets   = 0 # Number of targets
        noccupied  = 0 # Number of occupied cells
        nnulls     = 0 # Number of null cells
        nextras    = 0 # Number of non-target or repeated targets

        # Count targets, occupied cells
        targetIdxs = []
        for idx in path:
            # Skip null points
            if idx < 0:
                nnulls = nnulls + 1
                continue
            row, col = idx2rowcol(idx, len(coveragePoints[0]))
            if coveragePoints[row][col]["target"] == True:
                ntargets = ntargets + 1
                targetIdxs.append(idx)
            if coveragePoints[row][col]["occupied"] == True:
                noccupied = noccupied + 1

        # Count number of unique targets
        nuniques = len(set(targetIdxs))
        nextras = len(path) - nuniques - nnulls

        return ntargets, noccupied, \
               nuniques, nnulls, nextras
    
    def pathDistance(path, coveragePoints, region):
        path = list(path)
        distance = {"total" : 0.0, 
                    "penalty" : 0.0, 
                    "penaltyWeighted" : 0.0, 
                    "segments" : [], 
                    "segmentPenalties" : []}
        
        rowi, coli = idx2rowcol(path[0], 
                                len(coveragePoints[0]))
        rowf, colf = idx2rowcol(path[len(path) - 1], 
                                len(coveragePoints[0]))

        del path[0]
        del path[len(path) - 1]

        for idx in path:
            # Skip null points
            if idx < 0:
                continue
            rowj, colj = idx2rowcol(idx, len(coveragePoints[0]))
            distance["segments"].append(euclideanDistance \
                (coli, rowi, colj, rowj))
            rowi = rowj
            coli = colj

        distance["segments"].append(euclideanDistance \
            (coli, rowi, colf, rowf))

        distance["total"] = sum (distance["segments"])
        return distance

    ntargets, noccupied, nuniques, nnulls, nextras \
        = countTypes(path, coveragePoints)
    distance = pathDistance(path, coveragePoints, region)

    # Calculate fitness
    f = ( 
         #(-10000) * nnulls +      # Max null cells
         (10)    * nextras + 
         (50)   * distance["total"] +    # Min path distance
         (-100)   * nuniques +    # Max num unique targets
         (100)   * noccupied)    # Min occupied cells

    print (f, nnulls, nextras)

    return f, ntargets, nuniques, noccupied


class coveragePathProblem2(base):
    def __init__(self, dim = 10):
        # Set problem dimensions based on number of points
        dim = DIM

        super(coveragePathProblem2, self).__init__(dim, dim)

        # bounds based on number of possible actions
        self.set_bounds(0, 4)

    def _objfun_impl(self, x):
        
        actionSeq = list(x)
        f = pathFitness2(actionSeq, COVERAGE_POINTS, REGION)[0]
        return (f, )

class coveragePathProblem(base):
    def __init__(self, dim = 10):
        # Set problem dimensions based on number of points
        dim = DIM

        super(coveragePathProblem, self).__init__(dim, dim)

        npoints = (len(COVERAGE_POINTS) *   # Number of rows
                   len(COVERAGE_POINTS[0])) # Number of cols
        self.set_bounds(-1 * npoints, npoints - 1)


    def _objfun_impl(self, x):
        
        path = list(x)
        path.insert(0, GOAL_POINTS["start"])
        path.append(GOAL_POINTS["stop"])
        f, penaltyObstacles = pathFitness(path, COVERAGE_POINTS, REGION)[0]
        return (f, )

def printPoints(points):
    nrows = len(points)
    ncols = len(points[0])

    for row in range(nrows):
        for col in range(ncols):
            if points[row][col]["occupied"] == True:
                sys.stdout.write('x ')
            elif points[row][col]["target"] == False:
                sys.stdout.write('n ')
            else:
                sys.stdout.write('o ')
        sys.stdout.write('\n')

    

def getCoveragePoints(region, center, size, npoints, margin, roam):

    def makePointNode(col, row, 
            occupied = False, target = True):
        pointNode = { "occupied": occupied, 
                      "target": target,
                      "row": row,
                      "col": col,
        }
        return pointNode

    def printRectangle (ul, ll, ur, lr):
        print ("(%d,%d) ----- (%d,%d)" % (ul[0], ul[1], ur[0], ur[1]))
        print ("   |   (%d,%d)   |   " % (center[0], center[1]))
        print ("(%d,%d) ----- (%d,%d)" % (ll[0], ll[1], lr[0], lr[1]))

    # Make rectangle
        #   (xul, yul) ---------- (xur, yur)
        #       |                     |
        #       |                     |
        #   (xll, yll) ---------- (xlr, ylr)

    # Rectangle with roam buffer
    ul = (center[0] - 0.5 * (size[0] + roam), 
          center[1] + 0.5 * (size[1] + roam))
    ll = (center[0] - 0.5 * (size[0] + roam), 
          center[1] - 0.5 * (size[1] + roam))
    ur = (center[0] + 0.5 * (size[0] + roam), 
          center[1] + 0.5 * (size[1] + roam))
    lr = (center[0] + 0.5 * (size[0] + roam), 
          center[1] - 0.5 * (size[1] + roam))

    # Rectable without roam buffer
    wul = (center[0] - 0.5 * size[0], 
           center[1] + 0.5 * size[1])
    wll = (center[0] - 0.5 * size[0], 
           center[1] - 0.5 * size[1])
    wur = (center[0] + 0.5 * size[0], 
           center[1] + 0.5 * size[1])
    wlr = (center[0] + 0.5 * size[0], 
           center[1] - 0.5 * size[1])


    printRectangle(ul, ll, ur, lr)

    # Generate points in rectangle
    xsample = np.linspace(ul[0], ur[0], npoints[0])
    ysample = np.linspace(ul[1], ll[1], npoints[1])
    points = np.stack(np.meshgrid(xsample, ysample), -1).reshape(-1, 2).astype(int)

    pointNodes = []
    idx = 0
    for p in points:
        occupied = False
        target = False
        
        pul = (int(p[0] - 0.5 * margin), 
               int(p[1] + 0.5 * margin))
        pll = (int(p[0] - 0.5 * margin), 
               int(p[1] - 0.5 * margin))
        pur = (int(p[0] + 0.5 * margin),
               int(p[1] + 0.5 * margin))
        plr = (int(p[0] + 0.5 * margin),
               int(p[1] - 0.5 * margin))

        marginRegion = region["grid"][pul[0]:pur[0],pll[1]:pul[1]]

        # Check if occupied
        if np.sum(marginRegion) > 0:
            occupied = True

        # Check if target (via bounds)
        if p[0] > wul[0] and p[0] < wlr[0] and \
           p[1] > wll[1] and p[1] < wul[1]:
            target = True

        pointNodes.append(makePointNode(p[0], p[1], occupied, target))
        idx = idx + 1

    gpoints = [[dict() for i in range(npoints[0])] for j in range(npoints[1])]

    idx = 0
    col = 0
    row = 0
    for n in pointNodes:
        gpoints[row][col] = dict(n)
        col = col + 1
        if col % npoints[0] == 0:
            row = row + 1
            col = 0
        if col >= npoints[0]:
             col = 0
        if row >= npoints[1]:
             break
    return gpoints


def getCoveragePath():
    if REGION          == None or \
       COVERAGE_PARAMS == None or \
       PSO_PARAMS      == None or \
       GOAL_POINTS     == None:
        return None

    numPoints = (len(COVERAGE_POINTS) *   # Number of rows
                 len(COVERAGE_POINTS[0])) # Number of cols

    # Set problem dimension
    # "Heuristic": Should not need more moves than
    # 2 * number of points
    global DIM
    DIM  = int (numPoints * 3)

    prob = coveragePathProblem2()
    algo = algorithm.sga(gen = PSO_PARAMS["generations"])
    isl = island(algo, prob, PSO_PARAMS["poolsize"])
    
    hasCollisions = True
    trials = 0
    while (hasCollisions == True):
        trials = trials + 1
        isl.evolve(1)
        isl.join()
        f, ntargets, nuniques, noccupied, path \
             = pathFitness2(isl.population.champion.x, 
                           COVERAGE_POINTS, REGION)
        if noccupied == 0:
            hasCollisions = False

    actionSequence = list(isl.population.champion.x)

    coveragePath = { "path"           : path,
                     "actionSequence" : actionSequence,
                     "fitness"        : isl.population.champion.f,
                     "trials"         : trials,
    }

    return coveragePath


def plotPath (path, coveragePoints, region):
    # Source:
    #       https://stackoverflow.com/a/45708066

    import georaster
    from osgeo import gdal
    import matplotlib.pyplot as plt
    import numpy as np
    from mpl_toolkits.basemap import Basemap

    fig = plt.figure (figsize = (6, 8))
    fpath = region['file']

    my_image = georaster.SingleBandRaster(fpath, load_data=False)

    # grab limits of image's extent  
    lowerleft  = (coveragePoints[0][0]["row"], 
                  coveragePoints[0][0]["col"])
    upperleft  = (coveragePoints[len(coveragePoints) - 1][0]["row"],
                  coveragePoints[len(coveragePoints) - 1][0]["col"])
    upperright = (coveragePoints[len(coveragePoints) - 1] \
                                [len(coveragePoints[0]) - 1]["row"],
                  coveragePoints[len(coveragePoints) - 1] \
                                [len(coveragePoints[0]) - 1]["col"])
    lowerright = (coveragePoints[0][len(coveragePoints[0]) - 1]["row"],
                  coveragePoints[0][len(coveragePoints[0]) - 1]["col"])


    worldlowerleft = GridUtil.grid2world(
                     lowerleft[0],
                     lowerleft[1],
                     region["raster"].GetGeoTransform())
    worldupperleft = GridUtil.grid2world(
                     upperleft[0],
                     upperleft[1],
                     region["raster"].GetGeoTransform())
    worldupperright = GridUtil.grid2world(
                     upperright[0],
                     upperright[1],
                     region["raster"].GetGeoTransform())
    worldlowerright = GridUtil.grid2world(
                     lowerright[0],
                     lowerright[1],
                     region["raster"].GetGeoTransform())
                  
    minx = worldlowerleft[1]
    miny = worldlowerleft[0]
    maxx = worldlowerright[1]
    maxy = worldupperleft[0]

    #minx, maxx, miny, maxy = my_image.extent

    # set Basemap with slightly larger extents
    # set resolution at intermediate level "i"
    m = Basemap (projection='cyl',
        llcrnrlon=minx-.005, \
        llcrnrlat=miny-.005, \
        urcrnrlon=maxx+.005, \
        urcrnrlat=maxy+.005, \
        resolution='h')

    # load the geotiff image, assign it a variable
    image = georaster.SingleBandRaster( fpath, \
        load_data=(minx, maxx, miny, maxy), \
        latlon=True)

    # plot the image on matplotlib active axes
    # set zorder to put the image on top of coastlines and continent areas
    # set alpha to let the hidden graphics show through
    plt.imshow(image.r, extent=(minx, maxx, miny, maxy), zorder=10, alpha=0.6)

    # Extract latitude and longitude from the path coordinates
    lat = []
    lon = []

    ncols = len(coveragePoints[0])
    for idx in path:
        if idx < 0:
            continue   # Skip nulls
        row, col = idx2rowcol(idx, ncols)

        print(row, col)

        worldrow, worldcol = GridUtil.grid2world(
            coveragePoints[row][col]["row"],
            coveragePoints[row][col]["col"],
            region["raster"].GetGeoTransform())

        lat.append(worldrow)
        lon.append(worldcol)
    x, y = m(lon, lat)

    m.plot (x, y, 'rx--')
    #m.scatter ([x[0]], [y[0]], s = 80, marker = '^', 
    #           color = 'b', zorder = 1)
    #plt.text (x[0] + 10, y[0] + 10, 'Start')
    #m.scatter ([x[len(x)-1]], [y[len(y)-1]], s = 80, 
    #           marker = 'p', color = 'r', zorder = 1)
    
    plt.savefig ("simplecoverage.png")
    return m







def test():
    terrainFlag = 1

    regionFile = "../DataLayers/scenario1/Map/regionLand_2.tif"
    regionRaster = gdal.Open(regionFile)
    regionExtent = rsi.getGridExtent(regionRaster)
    regionGrid = regionRaster.GetRasterBand(1).ReadAsArray().T

    global REGION
    REGION = { "file"   : regionFile,
               "raster" : regionRaster,
               "grid"   : regionGrid,
               "extent" : regionExtent,
    }

    # Center of converage area
    center = (600, 300)
    # Width, length of coverage area
    size = (100, 200)
    # Resolution of coverage area (x, y)
    npoints = (5, 10)
    # Margin to stay away from terrain (in cells)
    margin = 5
    # Border cells. These are not part of the target 
    # area, but the robot allowed to travel for dist min
    roam = 50

    global COVERAGE_PARAMS
    COVERAGE_PARAMS = { "center"  : center,
               "size"    : size,
               "npoints" : npoints,
               "margin"  : margin,
               "roam"    : roam,
    }

    global PSO_PARAMS
    PSO_PARAMS = { "generations" : 1000,
                   "poolsize"    : 100,
    }


    # Get points in coverage area that avoid obstacles
    global COVERAGE_POINTS
    COVERAGE_POINTS = getCoveragePoints(REGION, center, size, npoints, margin, roam)
    printPoints(COVERAGE_POINTS)

    global GOAL_POINTS
    # Robot start and stop positions, in terms of col, row of coveragePoints (not world!)
    #GOAL_POINTS = { "start" : { "x" : 0, 
    #                            "y" : len(COVERAGE_POINTS) },
    #                "stop"  : { "x" : len(COVERAGE_POINTS[0]), 
    #                            "y" : len(COVERAGE_POINTS) - 50 },
    #}
    GOAL_POINTS = { "start" : 20,
                    "stop"  : 30,
    }

    # Get path to travel over coverage area
    coveragePath = getCoveragePath()
    print(coveragePath)

    plotPath(coveragePath["path"], COVERAGE_POINTS, REGION)
    

    # PLAYGROUND
    #coveragePath = range(0, len(coveragePoints))
    #distance, penalty = pathDistance(coveragePoints, coveragePath, region)
    #idx = 0
    #for i in range(len(COVERAGE_POINTS)):
    #    for j in range(len(COVERAGE_POINTS[0])): 
    #        row, col = idx2rowcol(idx, len(COVERAGE_POINTS[0]))
    #        print (i, j, row, col)
    #        idx = idx + 1

test()


