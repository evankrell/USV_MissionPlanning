import numpy as np
import random
from scipy.spatial import distance
from deap import creator, base, tools, algorithms
from bresenham import bresenham

BOX = dict()


#####################
# Setup Environment #
#####################

# Environment, where 0->free and 1->occupied.
env = np.array([ [0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0],
                 [0, 0, 0, 0, 0, 0],
                 [1, 1, 0, 0, 0, 0],
                 [1, 1, 1, 0, 0, 0],
                 [1, 1, 0, 0, 0, 0],
               ])

start = (0, 0)
goal  = (5, 5)

manualGoodSolution = [1, 2, 3, 4, 5, 11, 10, 9, 8 , 7, 6, 12, 13, 14, 15, 16, 17, 21, 20, 19, 18, 21, 25, 26, 27, 23, 24, 28]

###################
# Setup Utilities #
###################

# Generate coord2node table. Converts a 'row, col' to a numeric node ID
# (Each grid cell is a node)
count = 0
nodes = np.array([[0 for x in range(len(env[0]))] for y in range(len(env))])
for r in range(len(env)):
    for c in range(len(env[0])):
        nodes[r][c] = count
        count = count + 1

# Generate node2coord table. Converts a numeric node ID to 'row,col'.
# (Each grid cell is a node)
count = 0
node2coords = np.array([(0, 0) for x in range(len(env) * len(env[0]))])
for r in range(len(env)):
    for c in range(len(env[0])):
        node2coords[count][0] = r
        node2coords[count][1] = c
        count = count + 1

targetMap = []
count = 0
for r in range(len(env)):
    for c in range(len(env[0])):
        if env[r, c] == 0:
            targetMap.append(count)
        count = count + 1
numTargets = len(targetMap)

# Put the environment into a global container
global BOX
BOX["start"] = start
BOX["goal"] = goal
BOX["targetMap"]      = targetMap
BOX["node2coords"]    = node2coords
BOX["coords2nodes"]   = nodes
BOX["verbose"]        = False

###############
# Print Setup #
###############

print (BOX)

####################
# Fitness Function #
####################
def convertIDX(chromosome):
    convertedChromosome = []
    for c in chromosome:
        convertedChromosome.append(BOX["targetMap"][c])
    return convertedChromosome

def simplePath(path):
    return [a for a,b in zip(path, path[1:]+[not path[-1]]) if a != b]

def getInternalNodes(pointA, pointB):
    coordsA        = node2coords[pointA]
    coordsB        = node2coords[pointB]
    internalCoords = list(bresenham(coordsA[1], coordsA[0], coordsB[1], coordsB[0]))
    internalCoords.pop(0)
    internalCoords.pop()
    internalNodes  = [BOX["coords2nodes"][ic[1], ic[0]] for ic in internalCoords]
    return internalNodes

def expandPathAdj(path):
    newPath = []
    pointA = path[0]
    newPath.append(pointA)
    for p in path[1:]:
        pointB = p
        internalNodes = getInternalNodes(pointA, pointB)
        for i in internalNodes:
            newPath.append(i)
        pointA = pointB
        newPath.append(pointB)
    return newPath

def getPointDist(pointA, pointB):
    coordsA = node2coords[pointA]
    coordsB = node2coords[pointB]
    distAB = distance.euclidean(coordsA, coordsB) ** 2
    return distAB

def getPathDist(path):
    distance = 0.0
    pointA = path[0]
    for p in path[1:]:
        pointB = p
        distance = distance + getPointDist(pointA, pointB)
        pointA = pointB
    return distance

def getPathObstacles(path):
    numObstacles = 0
    for p in path:
        coordsP = node2coords[p]
        if env[coordsP[0], coordsP[1]] == 1:
            numObstacles = numObstacles + 1
    return numObstacles

def getPathRepeats(path):
    uniques = []
    repeats = 0
    for p in path:
        if p in uniques:
            repeats = repeats + 1
        else:
            uniques.append(p)
    return repeats

def getPathTurns(path):

    def getDirection(pointA, pointB):
        direction = 'NONE'
        coordsA = node2coords[pointA]
        coordsB = node2coords[pointB]
        if coordsA[0] + 1 == coordsB[0] and coordsA[1] == coordsB[1]:
            direction = 'DOWN'
        if coordsA[0] - 1 == coordsB[0] and coordsA[1] == coordsB[1]:
            direction = 'UP'
        if coordsA[0] == coordsB[0] and coordsA[1] + 1 == coordsB[1]:
            direction = 'RIGHT'
        if coordsA[0] == coordsB[0] and coordsA[1] - 1 == coordsB[1]:
            direction = 'LEFT'
        if coordsA[0] + 1 == coordsB[0] and coordsA[1] + 1 == coordsB[1]:
            direction = 'DOWN-RIGHT'
        if coordsA[0] + 1 == coordsB[0] and coordsA[1] - 1 == coordsB[1]:
            direction = 'DOWN-LEFT'
        if coordsA[0] - 1 == coordsB[0] and coordsA[1] + 1 == coordsB[1]:
            direction = 'UP-RIGHT'
        if coordsA[0] - 1 == coordsB[0] and coordsA[1] - 1 == coordsB[1]:
            direction = 'UP-LEFT'
        return direction

    numTurns = 0
    pointA = path[0]
    pointB = path[1]
    currentDirection = getDirection(pointA, pointB)
    for pointB in path[1:]:
        newDirection = getDirection(pointA, pointB)
        if (currentDirection != 'NONE' and currentDirection != newDirection):
            numTurns = numTurns + 1
            currentDirection = newDirection
        pointA = pointB
    return numTurns

def evalNodeBased(individual):
    global BOX

    # Convert chromosome to actual nodes
    convertedChromosome = convertIDX(individual)

    # Convert individual to path
    path = [nodes[BOX["start"][0], BOX["start"][1]]]    # Add start node
    for i in convertedChromosome:     # Optimization individual is interior nodes
        if i > 0:
            path.append(i)
    path.append(nodes[BOX["goal"][0], BOX["goal"][1]]) # Add goal node
    path = simplePath(path)

    # Expand path
    path = expandPathAdj(path)

    # Get path information
    pathCount    = len(path)
    pathDistance = getPathDist(path)
    targetCount  = len(set(path).intersection(set(targetMap)))

    # Count obstacles 
    numObstacles = getPathObstacles(path)

    # Count repeats
    numRepeats = getPathRepeats(path)

    # Count turns
    numTurns = getPathTurns(path)

    # Calculate path fitness
    targetsNeed = 18 - targetCount
    fitness = (pathDistance 
        + targetsNeed  * (10) 
        + numObstacles * (100) 
        + numTurns     * (0)       # Turn numTurns OFF 
        + numRepeats   * (0))      # Turn numRepeats OFF
    fitness = (-1) * fitness

    if BOX["verbose"] == True:
        print(path, "obstacleCount:", numObstacles, "targetCount:", targetCount, "pathDistance:", pathDistance, "numTurns", numTurns, "fitness:", fitness)

    return [fitness]


###################
# Setup GA Solver #
###################

gaDimensions = 20

creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Individual", list, fitness=creator.FitnessMax)

toolbox = base.Toolbox()

toolbox.register("attr_act", random.randint, 0, len(targetMap) - 1 )
toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_act, n = gaDimensions)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

toolbox.register("evaluate", evalNodeBased)
toolbox.register("mate", tools.cxTwoPoint)
toolbox.register("mutate", tools.mutFlipBit, indpb=0.05)
toolbox.register("select", tools.selTournament, tournsize=3)

population = toolbox.population(n=300)

hof = tools.HallOfFame(1)
stats = tools.Statistics(lambda ind: ind.fitness.values)
stats.register("avg", np.mean)
stats.register("min", np.min)
stats.register("max", np.max)

#####################
# Execute GA Solver #
#####################
NGEN=100

pop, logbook = algorithms.eaSimple(population, toolbox, cxpb=0.4, mutpb=0.2, ngen=NGEN,
                   stats=stats, halloffame=hof, verbose=True)

print("Best individual is: %s\nwith fitness: %s" % (hof[0], hof[0].fitness))

BOX["verbose"] = True
print("Manual good solution")
evalNodeBased(manualGoodSolution)
print("")
print("GA 'best' solution")
evalNodeBased(hof[0])
print("")



combo = [0, 1, 2, 3, 4, 5, 10, 16, 22, 21, 14, 13, 12, 13, 14, 15, 16, 17, 10, 9, 8, 7, 6,   7, 14, 15, 19, 22, 26, 25, 22, 19, 18, 13, 12, 13, 14, 9, 10, 16, 20, 23, 22, 23, 20, 16, 11, 5, 11, 17, 21, 24, 28]
evalNodeBased(combo)

