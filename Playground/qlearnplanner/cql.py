import numpy as np
import random

BOX = dict()

def decrementReward(r):
    if r > 0:
        rnew = -2
    else:
        rnew = r - r*r
    return rnew

def checkBounds(position, nrows, ncols):
    if position[0] < 0 or position[0] >= nrows or \
       position[1] < 0 or position[1] >= ncols:
        return False
    else:
        return True

def move(env, position, action):

    # Current position
    crow = position[0]
    ccol = position[1]

    # Next position
    nrow = crow
    ncol = ccol

    # Move
    if   action == 'U':    # Up
        nrow = crow - 1
        ncol = ccol
    elif action == 'D':    # Down
        nrow = crow + 1
        ncol = ccol
    elif action == 'L':    # Left
        nrow = crow
        ncol = ccol - 1
    elif action == 'R':    # Right
        nrow = crow
        ncol = ccol + 1

    return (nrow, ncol)


def reward(env, position, action, rewardsSpatial, rewardsSpecial, goal):

    reward = 0
    rewardType = "NONE"

    # Reward halt on goal
    if action == "H":
        if position[0] == goal[0] and \
           position[1] == goal[1]:
            reward = rewardsSpecial["GOAL"]
            rewardType = "GOAL"
        else:
            reward = rewardsSpecial["EARLY"]
            rewardType = "EARLY"

    # Reward moves
    if action == "U" or \
       action == "D" or \
       action == "L" or \
       action == "R":

        if checkBounds(position, len(env), len(env[0])) == True:
            reward = rewardsSpatial[position[0]][position[1]]
            rewardType = "SPACE"
        else:
            reward = rewardsSpecial["BOUNDS"]
            rewardType = "BOUNDS"

    return reward, rewardType


def evaluateActionSequence(env, start, goal, rewardsSpatial, rewardsSpecial, actions, quiet):

    # Setup rewards (need to copy since changes over iterations)
    rewardsSpatial = np.copy(rewardsSpatialInit)

    # Init robot and init reward
    position = start
    rewardAccum = rewardsSpatial[position[0]][position[1]]
    # Adjust rewards
    if checkBounds(position, len(env), len(env[0])) == True and env[position[0]][position[1]] == 0:
        rewardsSpatial[position[0]][position[1]] = decrementReward(rewardsSpatial[position[0]][position[1]])


    # Print status
    if quiet == False:
        print (position, rewardAccum)

    # Do action sequence
    for a in actions:

        # Move
        position = move(env, position, a)

        # Calculate reward
        r, rtype = reward(env, position, a, rewardsSpatial, rewardsSpecial, goal)
        rewardAccum = rewardAccum + r

        # Adjust rewards
        if rtype == "SPACE":
            if checkBounds(position, len(env), len(env[0])) == True and env[position[0]][position[1]] == 0:
                rewardsSpatial[position[0]][position[1]] = decrementReward(rewardsSpatial[position[0]][position[1]])

        # Print status
        if quiet == False:
            print (position, rewardAccum, rtype)

        if a == "H":
            break

    return rewardAccum



##########################
# Experiment Environment #
##########################

# Environment, where 0->free and 1->occupied.
env = np.array([ [0, 0, 0],
                 [0, 0, 0],
                 [1, 1, 0],
                 [1, 0, 0],
               ])

start = (0, 0)
goal  = (3, 2)

rewardsSpatialInit = np.array([ [   10,    10, 10],
                                [   10,    10, 10],
                                [-100, -100, 10],
                                [-100,    10, 10],
                              ])
rewardsSpecial = { "GOAL"   : 50,
                   "EARLY"  : -50,
                   "BOUNDS" : -100,
                 }

print("Environment")
print(env)
print("")

print("Start:", start, "Goal:", goal)
print("")

print("Rewards (spatial):")
print(rewardsSpatialInit)
print("")

print("Rewards (special):")
print(rewardsSpecial)
print("")


###############
# Experiments #
###############

print("Known optimal:")
actions = ["D", "R", "U", "R", "D", "D", "D", "L", "R", "H", "U"]
evaluateActionSequence(env, start, goal, rewardsSpatialInit, rewardsSpecial, actions, False)
print("")

print ("With unneeded backtracking:")
actions = ["D", "R", "U", "R", "D", "D", "D", "U", "D", "L", "R", "H", "U"]
evaluateActionSequence(env, start, goal, rewardsSpatialInit, rewardsSpecial, actions, False)
print("")

print ("With double unneeded backtracking:")
actions = ["D", "R", "U", "R", "D", "D", "D", "U", "D", "U", "D", "L", "R", "H", "U"]
evaluateActionSequence(env, start, goal, rewardsSpatialInit, rewardsSpecial, actions, False)
print("")




global BOX
BOX["start"] = start
BOX["goal"] = goal
BOX["rewardsSpatial"] = rewardsSpatialInit
BOX["rewardsSpecial"] = rewardsSpecial

converter = ['U', 'D', 'L', 'R', 'H']

def evalOneMax(individual):
    actions = [converter[i] for i in individual]
    global BOX
    return [evaluateActionSequence(env, BOX["start"], BOX["goal"],
                                       BOX["rewardsSpatial"], BOX["rewardsSpecial"],
                                       actions, True)]

def evalOutside(individual):
    actions = [converter[i] for i in individual]
    global BOX
    return [evaluateActionSequence(env, BOX["start"], BOX["goal"],
                                       BOX["rewardsSpatial"], BOX["rewardsSpecial"],
                                       actions, False)]




############################
# Solver - GA (node-based) #
############################
from scipy.spatial import distance
import random
from deap import creator, base, tools, algorithms


# Generate coord2node table. Converts a 'row, col' to a numeric node ID
# (Each grid cell is a node)
count = 0
nodes = np.array([[0 for x in range(len(env[0]))] for y in range(len(env))])
for r in range(len(env)):
    for c in range(len(env[0])):
        nodes[r][c] = count
        count = count + 1

# Generate node2coord table. Converts a numeric node ID to 'row,col'.
# (Each grid cell is a node)
count = 0
node2coords = np.array([(0, 0) for x in range(len(env) * len(env[0]))])
for r in range(len(env)):
    for c in range(len(env[0])):
        node2coords[count][0] = r
        node2coords[count][1] = c
        count = count + 1

print node2coords


targetMap = []
count = 0
for r in range(len(env)):
    for c in range(len(env[0])):
        if env[r, c] == 0:
            targetMap.append(count)
        count = count + 1
print (targetMap)

numTargets = len(targetMap)

global BOX
BOX["start"] = start
BOX["goal"] = goal
BOX["rewardsSpatial"] = rewardsSpatialInit
BOX["rewardsSpecial"] = rewardsSpecial
BOX["targetMap"]      = targetMap
BOX["node2coords"]    = node2coords
BOX["coords2nodes"]   = nodes

def evalNodeBased(individual):

    def convertIDX(chromosome):
        convertedChromosome = []
        for c in chromosome:
            convertedChromosome.append(BOX["targetMap"][c])
        return convertedChromosome



    def simplePath(path):
        return [a for a,b in zip(path, path[1:]+[not path[-1]]) if a != b]

    def getPointDist(pointA, pointB, type):
        coordsA = node2coords[pointA]
        coordsB = node2coords[pointB]
        if type == "euclidean":
            distAB = distance.euclidean(coordsA, coordsB)
        if type == "cityblock":
            distAB = distance.cityblock(coordsA, coordsB)
        else: 
            distAB = 0
        return distAB
   
    def getPathDist(path):
        distance = 0
        pointA = path[0]
        for p in path[1:]:
            pointB = p
            distance = distance + getPointDist(pointA, pointB, "cityblock")
            pointA = pointB
        return distance
        
           
    global BOX

    # Convert chromosome to actual nodes
    convertedChromosome = convertIDX(individual)

    # Convert individual to path
    path = [nodes[BOX["start"][0], BOX["start"][1]]]    # Add start node
    for i in convertedChromosome:     # Optimization individual is interior nodes
        if i > 0:
            path.append(i)   
    path.append(nodes[BOX["goal"][0], BOX["goal"][1]]) # Add goal node
    path = simplePath(path)

    # Get path information
    pathCount    = len(path)
    pathDistance = getPathDist(path)
    targetCount  = len(set(path).intersection(set(targetMap)))


    # Calculate path fitness
    fitness = pathDistance - targetCount * (3)

    fitness = (-1) * fitness

    print(path, "pathCount:", pathCount, "targetCount:", targetCount, "pathDistance:", pathDistance, "fitness:", fitness)


    return [fitness]

evalNodeBased([1, 2, 2, 3, 0])

evalNodeBased([0, 3, 4, 1, 2, 5, 8])


creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Individual", list, fitness=creator.FitnessMax)

toolbox = base.Toolbox()

toolbox.register("attr_act", random.randint, 0, len(targetMap) - 1 )
toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_act, n=13)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)


toolbox.register("evaluate", evalNodeBased)
toolbox.register("mate", tools.cxTwoPoint)
toolbox.register("mutate", tools.mutFlipBit, indpb=0.05)
toolbox.register("select", tools.selTournament, tournsize=3)

population = toolbox.population(n=300)

NGEN=100
for gen in range(NGEN):
        offspring = algorithms.varAnd(population, toolbox, cxpb=0.5, mutpb=0.1)
        fits = toolbox.map(toolbox.evaluate, offspring)
        for fit, ind in zip(fits, offspring):
            ind.fitness.values = fit
            population = toolbox.select(offspring, k=len(population))
            top10 = tools.selBest(population, k=10)

print ("Top 10:")
for t in top10:
    evalNodeBased(t)





exit()




###################
# Solver - Random #
###################

#fit = 0
#individual = None
#while fit < 93:
#    individual = np.random.randint(5, size=10)
#    fit = evalOneMax(individual)[0]
#    print (fit)
#
#evalOutside(individual)

###############
# Solver - GA #
###############


#import random
#from deap import creator, base, tools, algorithms
#
#creator.create("FitnessMax", base.Fitness, weights=(1.0,))
#creator.create("Individual", list, fitness=creator.FitnessMax)
#
#toolbox = base.Toolbox()
#
#toolbox.register("attr_act", random.randint, 0, 4)
#toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_act, n=10)
#toolbox.register("population", tools.initRepeat, list, toolbox.individual)
#
#
#toolbox.register("evaluate", evalOneMax)
#toolbox.register("mate", tools.cxTwoPoint)
#toolbox.register("mutate", tools.mutFlipBit, indpb=0.05)
#toolbox.register("select", tools.selTournament, tournsize=3)
#
#population = toolbox.population(n=300)
#
#NGEN=100
#for gen in range(NGEN):
#        offspring = algorithms.varAnd(population, toolbox, cxpb=0.5, mutpb=0.1)
#        fits = toolbox.map(toolbox.evaluate, offspring)
#        for fit, ind in zip(fits, offspring):
#            ind.fitness.values = fit
#            population = toolbox.select(offspring, k=len(population))
#            top10 = tools.selBest(population, k=10)
#
#for t in top10:
#    print(t)
#    evalOutside(t)
#




##############
# Solver - Q #
# (Version 2 #
##############
import itertools

# Define action space
actionIDs = [0, 1, 2, 3, 4]

def getLocalHist(history, position, moves):
    localHist = [i for i in range(moves + 1)]
    row = position[0]
    col = position[1]

    # Check self
    localHist[0] = history[row][col]

    # Check up
    if row - 1 >= 0:
        localHist[1] = history[row - 1][col]
    else:
        localHist[1] = '0'

    # Check down
    if row + 1 < len(history):
        localHist[2] = history[row + 1][col]
    else:
        localHist[2] = '0'

    # Check left
    if col - 1 >= 0:
        localHist[3] = history[row][col - 1]
    else:
        localHist[3] = '0'

    # Check right
    if col + 1 < len(history[0]):
        localHist[4] = history[row][col + 1]
    else:
        localHist[4] = '0'

    return localHist

def getState(env, rewardsSpatial, position, nodes, moves, history):
    numCells = moves * moves
    startIDX = nodes[position[0]][position[1]] * numCells * 2
    endIDX = startIDX + numCells * 2

    localHistory = getLocalHist(history, position, moves)
    count = 0
    for row in states[startIDX:endIDX]:
        if (row[1:]==localHistory).all() == True:
            stateIDX = startIDX + count
            break
        count = count + 1

    return (startIDX, endIDX, stateIDX)


# Generate history table
historyInit = np.array([['0' for x in range(len(env[0]))] for y in range(len(env))])

# Generate coord2node table. Converts a 'row, col' to a numeric node ID
# (Each grid cell is a node)
count = 0
nodes = np.array([[0 for x in range(len(env[0]))] for y in range(len(env))])
for r in range(len(env)):
    for c in range(len(env[0])):
        nodes[r][c] = count
        count = count + 1


# Generate state table
# columns = | node ID | self | up | down | left | right |
# The node ID has an integer. The others are booleans where 0 is unvisited and 1 is visited
states = []
moves = 4
count = 0
for r in range(len(env)):
    for c in range(len(env[0])):
        for seq in itertools.product("01", repeat=moves): # Permutations of neighborhood visited states

            # Create row for case where current node has not been visited
            staterow = [s for s in seq]
            staterow.insert(0, 0)
            staterow.insert(0, count)
            states.append(staterow)

            # Create row for case where current node has been visited
            staterow = [s for s in seq]
            staterow.insert(0, 1)
            staterow.insert(0, count)
            states.append(staterow)

        count = count + 1
states = np.array(states)

# Get number of unique possible states (defined on 4-neighborhood)
numStates = len(states)

# Generate Q table
observationSpaceSize = numStates
actionSpaceSize = len(actionIDs)
Q = np.zeros([observationSpaceSize, actionSpaceSize])

# Display initial setup
print(historyInit)
print(nodes)
print(states)
print(numStates)

rewardsSpatial = rewardsSpatialInit
stateIDXstart = getState(env, rewardsSpatial, start, nodes, moves, historyInit)
stateIDXgoal = getState(env, rewardsSpatial, goal, nodes, moves, historyInit)

print(states[stateIDXstart[0]:stateIDXstart[1]])
print(states[stateIDXgoal[0]:stateIDXgoal[1]])

print("State ID: ", stateIDXstart[2], "State:", states[stateIDXstart[2]])
print("State ID: ", stateIDXgoal[2], "State:", states[stateIDXgoal[2]])

print(Q[stateIDXstart[2]])
print(Q[stateIDXgoal[2]])

# Learning(searching) count
limit = 100
# Alpha value in Q-Learning.
alpha = 0.1
# Gamma value in Q-Learning.
gamma = 0.6
# Epsilon value in Q-Learning.
epsilon = 0.1

for i in range(1, limit):
    print ("Learning episode: ", i)

    # Initialize rewards with initial states
    rewardsSpatial = rewardsSpatialInit.copy()
    # Initiaze local history
    history        = historyInit.copy()
    # Set current position to start
    position       = start
    # Get current system state
    stateID        = getState(env, rewardsSpatial, position,
                              nodes, moves, history)
    # Init score to 0
    score        = 0
    # Init action sequence history to nothing
    actionSeq    = []
    # Init number of visisted target nodes to 0
    visitedNodes = 0

    # Check if position is a target and had not yet been visited
    if env[position[0]][position[1]] == 0 and history[position[0]][position[1]] == '0':
        visitedNodes = visitedNodes + 1
    # Mark node as visited
    history[position[0]][position[1]] = '1'
    # Check if position in non-obstacle
    if env[position[0]][position[1]] == 0:
        # Set negative reward
        rewardsSpatial[position[0]][position[1]] = -1


    # Explore
    done = False
    while not done:
        r = 0

        # Select action
        if random.uniform(0, 1) < epsilon:    # Explore actions
            actionIDX = random.choice(actionIDs)
            action    = converter[actionIDX]
        else:
            actionIDX = np.argmax(Q[stateID[2]])
            action = converter[actionIDX]
        actionSeq.append(action)

        # Move
        positionNew = move(env, position, action)
        if checkBounds(positionNew, len(env), len(env[0])) == True:
            # Update state
            stateIDnew = getState(env, rewardsSpatial, position, nodes, moves, history)

            # Calculate reward
            r, rtype = reward(env, positionNew, action, rewardsSpatial, rewardsSpecial, goal)
        else:
           # Maintain position
           positionNew = position
           # Maintain state
           stateIDnew = stateID
           # Calculate reward
           r = rewardsSpecial["BOUNDS"]
           rtype = "BOUNDS"

        # Check if position is a target and had not yet been visited
        if env[positionNew[0]][positionNew[1]] == 0 and history[positionNew[0]][positionNew[1]] == '0':
            visitedNodes = visitedNodes + 1
        # Mark node as visited
        history[positionNew[0]][positionNew[1]] = '1'    # Set position as visited

        # Exit if halting at goal, and having found all nodes
        if position == goal and action == "H" and visitedNodes == 9:
            print ("End episode with score", score)
            done = True
            r = rewardsSpecial["GOAL"]

        #print (position, action, visitedNodes)

        # Update score with reward
        score = score + r

        # Update Q table
        oldQval = Q[stateID[2], actionIDX]
        nextMax = np.max(Q[stateIDnew[2]])
        newQval = (1 - alpha) * oldQval + alpha * (r + gamma * nextMax)
        Q[stateID[2], actionIDX] = newQval

        # Check if position in non-obstacle
        if env[positionNew[0]][positionNew[1]] == 0:
            # Set negative reward
            rewardsSpatial[positionNew[0]][positionNew[1]] = -1

        # New is now old for next iteration
        position = positionNew
        stateID = stateIDnew

    #threshold = 50
    #if score > threshold:
    #    print(score, actionSeq)

np.savetxt("Q-1.txt", Q)


print ("Evaluate agent...")
# Evaluate agent

# Initialize rewards with initial states
rewardsSpatial = rewardsSpatialInit.copy()
# Initiaze local history
history = historyInit.copy()
# Set current position to start
position       = start
# Get current system state
stateID        = getState(env, rewardsSpatial, position,
                          nodes, moves, history)
# Init score to 0
score = 0
# Init action sequence history to nothing
actionSeq = []

# Set position as visited
history[position[0]][position[1]] = '1'
# Check if position in non-obstacle
if env[position[0]][position[1]] == 0:
    # Set negative reward
    rewardsSpatial[position[0]][position[1]] = -1

done = False
while not done:
    # Select action
    actionIDX = np.argmax(Q[stateID[2]])
    action = converter[actionIDX]
    actionSeq.append(action)

    # Move
    positionNew = move(env, position, action)
    if checkBounds(positionNew, len(env), len(env[0])) == True:
        # Update state
        stateIDnew = getState(env, rewardsSpatial, position, nodes, moves, history)

        # Calculate reward
        r, rtype = reward(env, positionNew, action, rewardsSpatial, rewardsSpecial, goal)
    else:
       # Maintain position
       positionNew = position
       # Maintain state
       stateIDnew = stateID
       # Calculate reward
       r = rewardsSpecial["BOUNDS"]
       rtype = "BOUNDS"

    if (position == goal and action == "H"):
        done = True
        r = rewardsSpecial["GOAL"]

    # Update score with reward
    score = score + r

    # Set position as visited
    history[positionNew[0]][positionNew[1]] = '1'
    # Check if position in non-obstacle
    if env[positionNew[0]][positionNew[1]] == 0:
        # Set negative reward
        rewardsSpatial[positionNew[0]][positionNew[1]] = -1

    # New is now old for next iteration
    position = positionNew
    stateID = stateIDnew


print(score, actionSeq)
print(history)
print(rewardsSpatial)
















##############
# Solver - Q #
# (Version 1 #
##############
##
##import matplotlib.pyplot as plt
##import numpy as np
##import pandas as pd
##import seaborn as sns
##
##
### `#` is wall in maze.
### `S` is a start point.
### `G` is a goal.
### `@` is the agent.
##start_point_label, end_point_label, wall_label, agent_label = ("S", "G", "#", "@")
##
##qenv = env.astype(object)
###qenv = np.array([['-' for x in range(len(env[0]))] for y in range(len(env))])
##for r in range(len(qenv)):
##    for c in range(len(qenv[0])):
##        if env[r][c] == 1:
##            qenv[r][c] = wall_label
##        else:
##           qenv[r][c] = rewardsSpatialInit[r][c]
##
##qenv[start[0]][start[1]] = start_point_label
##qenv[ goal[0]][ goal[1]] = end_point_label
##
##
### Init states table. Converts a 'row, col' to a numeric state ID
##count = 0
##states = np.array([[0 for x in range(len(env[0]))] for y in range(len(env))])
##for r in range(len(qenv)):
##    for c in range(len(qenv[0])):
##        states[r][c] = count
##        count = count + 1
##
##
### Visualize
###fig = plt.figure(figsize=(16, 14))
###sns.heatmap(env, linewidths=.5)
###plt.show();plt.close()
##
##
### Init q table
##observationSpaceSize = len(env) * len(env[0])
##actionSpaceSize      = len(converter)
##q_table = np.zeros([observationSpaceSize, actionSpaceSize])
##print (q_table)
##
### Learning(searching) count
##limit = 10000
### Alpha value in Q-Learning.
##alpha = 0.1
### Gamma value in Q-Learning.
##gamma = 0.6
### Epsilon value in Q-Learning.
##epsilon = 0.1
##
##
##actionIDs = [0, 1, 2, 3, 4]
##
##
##for i in range(1, 100):
##    # Reinforcement Learning
##    # Init robot and init reward
##    rewardsSpatial = rewardsSpatialInit.copy()
##    position = start
##    stateID = states[position[0]][position[1]]
##
##    # Evaluate initial position
##
##
##    # Explore
##    done = False
##    while not done:
##
##        # Select action
##        if random.uniform(0, 1) < epsilon:    # Explore actions
##            actionIDX = random.choice(actionIDs)
##            action    = converter[actionIDX]
##        else:
##            actionIDX = np.argmax(q_table[stateID])
##            action = converter[actionIDX]
##        print (actionIDX, action)
##
##        # Move
##        positionNew = move(env, position, action)
##        if checkBounds(positionNew, len(env), len(env[0])) == True:
##            # Update state
##            stateIDnew = states[positionNew[0]][positionNew[1]]
##            # Calculate reward
##            r, rtype = reward(env, positionNew, action, rewardsSpatial, rewardsSpecial, goal)
##        else:
##           # Maintain position
##           positionNew = position
##           # Maintain state
##           stateIDnew = stateID
##           # Calculate reward
##           r = -100
##           rtype = "BOUNDS"
##
##
##        if (position == goal and action == "H"):
##            done = True
##            r = rewardsSpecial["GOAL"]
##
##        # Update Q table
##        oldQval = q_table[stateID, actionIDX]
##        nextMax = np.max(q_table[stateIDnew])
##        newQval = (1 - alpha) * oldQval + alpha * (r + gamma * nextMax)
##        q_table[stateID, actionIDX] = newQval
##        print (newQval)
##
##
##        # New is now old for next iteration
##        position = positionNew
##        stateID = stateIDnew
##
##
##print (q_table)
###exit()
##
##"""Evaluate agent's performance after Q-learning"""
##print("Agent evaluation")
##
##total_epochs, total_penalties = 0, 0
##episodes = 1
##
##for _ in range(episodes):
##    epochs, penalties, r = 0, 0, 0
##
##    rewardsSpatial = rewardsSpatialInit.copy()
##    position = start
##    stateID = states[position[0]][position[1]]
##
##    done = False
##
##    while not done:
##        print (position)
##        actionIDX = np.argmax(q_table[stateID])
##        action = converter[actionIDX]
##
##        # Move
##        positionNew = move(env, position, action)
##        if checkBounds(positionNew, len(env), len(env[0])) == True:
##	    # Update state
##            stateIDnew = states[positionNew[0]][positionNew[1]]
##            # Calculate reward
##            r, rtype = reward(env, positionNew, action, rewardsSpatial, rewardsSpecial, goal)
##        else:
##            # Maintain position
##            positionNew = position
##            # Maintain state
##            stateIDnew = stateID
##            # Calculate reward
##            r = -100
##            rtype = "BOUNDS"
##
##        if (positionNew == goal and action == "H"):
##            done = True
##            r = rewardsSpecial["GOAL"]
##
##        # New is now old for next iteration
##        position = positionNew
##        stateID = stateIDnew
##
##
##        penalties += r
##        epochs += 1
##
##    total_penalties += penalties
##    total_epochs += epochs
##
###print(f"Results after {episodes} episodes:")
###print(f"Average timesteps per episode: {total_epochs / episodes}")
###print(f"Average penalties per episode: {total_penalties / episodes}")
##




